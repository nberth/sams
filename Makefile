# ---

# In case we are in a development directory, with `git' hopefully:
HAS_GIT = $(shell command -v git 2>&1 >/dev/null && test -d ".git" && \
	          echo yes || echo no)
ifeq ($(HAS_GIT),yes)
  VERSION_STR = $(shell git describe --tags --always)
else
  VERSION_STR = "unknown"
endif

# ---

DIST_EXT = tar.gz

CORE_DIST = sams-core-$(VERSION_STR)
CORE_FILES = LICENSE README $(addprefix core/, build.properties	\
					build.xml etc src	\
					 .dir-locals.el)
NEXPL_DIST = sams-ntier-examples-$(VERSION_STR)
NEXPL_FILES = LICENSE README $(addprefix examples/, common		\
					apache-tomcat sqltiers		\
					 apache-tomcat-sqltiers)

# ---

.PHONY: dist
dist: force
	mkdir -p /tmp/$(CORE_DIST);
	cp -r $(CORE_FILES) /tmp/$(CORE_DIST);
	tar caf $(CORE_DIST).$(DIST_EXT) -C /tmp $(CORE_DIST);
	rm -rf /tmp/$(CORE_DIST)
	mkdir -p /tmp/$(NEXPL_DIST);
	cp -r $(NEXPL_FILES) /tmp/$(NEXPL_DIST);
	tar caf $(NEXPL_DIST).$(DIST_EXT) -C /tmp $(NEXPL_DIST);
	rm -rf /tmp/$(NEXPL_DIST)

# ---

.PHONY: clean
clean: force
	rm -f *.$(DIST_EXT)

# ---

.PHONY: force
force:

# ---
