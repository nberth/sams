#!/bin/bash
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# This file is part of sams-core.
#
# Copyright Grenoble University, (2013)
#
# Author: Nicolas Berthier <nicolas.berthier@inria.fr>
#
# This software is a computer program whose purpose is to autonomously manage
# computing systems using reactive control techniques.
#
# This software is governed by the CeCILL license under French law and abiding
# by the rules of distribution of free software.  You can use, modify and/ or
# redistribute the software under the terms of the CeCILL license as circulated
# by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
#
# As a counterpart to the access to the source code and rights to copy, modify
# and redistribute granted by the license, users are provided only with a
# limited warranty and the software's author, the holder of the economic
# rights, and the successive licensors have only limited liability.
#
# In this respect, the user's attention is drawn to the risks associated with
# loading, using, modifying and/or developing or reproducing the software by
# the user in light of its specific status of free software, that may mean that
# it is complicated to manipulate, and that also therefore means that it is
# reserved for developers and experienced professionals having in-depth
# computer knowledge. Users are therefore encouraged to load and test the
# software's suitability as regards their requirements in conditions enabling
# the security of their systems and/or data to be ensured and, more generally,
# to use and operate it in the same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

depfile="./deployment-db";	# relative to current directory?

user="$1"; shift;
host="$1"; shift;
appid="$1"; shift;

error () { echo "$1" &>/dev/stderr; exit 1; }
info  () { echo "$1" &>/dev/stderr;         }

# Disable (interactive) host key checking for those...
# sshopts="-o StrictHostKeyChecking=yes";
sshopts="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null";

# Protects dieses `#'...
remote ()          {      ssh $sshopts -T -q "${user}@${host}" -- "${*//#/\\#}"; }
remote_tty ()      {      ssh $sshopts -t -q "${user}@${host}" -- "${*//#/\\#}"; }
exec_remote_tty () { exec ssh $sshopts -t -q "${user}@${host}" -- "${*//#/\\#}"; }

# XXX: there is probably a problem with spaces in args here:
recopy () {
    local d="$1"; shift;
    scp $sshopts -q -r "$@" "${user}@${host}:$d";
}

# XXX: ibid
retrieve () {
    local dest="$1"; shift;
    local d="$1"; shift;
    local a=( );
    # There is surely a better way of doing this...
    for f in "$@"; do
	a=( "${a[@]}" "${user}@${host}:$d/$f" );
    done;
    scp $sshopts -q -r "${a[@]}" "$dest";
}

# Lock commands.
#
# TODO: trap shell errors...

if command -v lockfile-create >& /dev/null; then
    # `lockfile-progs' seems available.
    lockit () { lockfile-create "$1"; }
    unlockit () { lockfile-remove "$1"; }
elif command -v lockfile >& /dev/null; then
    # `procmail' seems available.
    lockit () { lockfile -r -1 "$1.lock"; }
    unlockit () { rm -f "$1.lock"; }
else
    # maybe with `mkdir'... http://wiki.bash-hackers.org/howto/mutex
    error "No file locking mechanism seems available yet!";
fi;

# Always create it so that we can always use the following functions
# without other cares.
touch "${depfile}";

register_deployment () {
    lockit "${depfile}";
    echo "${user}" "${host}" "$@" >> "${depfile}";
    unlockit "${depfile}";
}
unregister_deployment () {
    lockit "${depfile}";
    local t="$(mktemp)";
    grep -v -F "${user} ${host} $1 " "${depfile}" > "$t";
    mv "$t" "${depfile}";
    unlockit "${depfile}";
}
deployment () {
    lockit "${depfile}";
    grep "^${user} ${host} $1 " "${depfile}" | cut -d ' ' -f 4-;
    unlockit "${depfile}";
}
all_deployments () {
    lockit "${depfile}";
    cut -d ' ' -f 1-3 "${depfile}";
    unlockit "${depfile}";
}

# check application id unless specified:
[ "x${checkappid}" != "xfalse" ] && [ -z "${appid}" ] && {
    error "No deployment specified!";
}
