<?xml version="1.0"?>
<!DOCTYPE config SYSTEM "a3config.dtd">
<config>
  <property name="Transaction" value="fr.dyade.aaa.util.NullTransaction"/>
  <domain name="D@IDENT@" network="fr.dyade.aaa.agent.SimpleNetwork"/>

  <server id="0" name="sams-center" hostname="@CENTER_HOST@">
    <network domain="D@IDENT@" port="@CENTER_PORT@"/>
  </server>

  <server id="@IDENT@" name="@NODE@" hostname="@HOST@">
    <network domain="D@IDENT@" port="@REMOTE_PORT@"/>
  </server>
</config>
