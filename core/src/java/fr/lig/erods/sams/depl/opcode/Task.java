/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.depl.opcode;

import fr.dyade.aaa.agent.Agent;
import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Notification;
import fr.lig.erods.sams.common.Env;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.util.Ansi;
import fr.lig.erods.sams.depl.Main;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Remote actuator agent.
 */
public abstract class Task extends Agent {

  protected final Env env;

  // ---

  protected Task (final Env env) {
    this.env = env;
  }

  // ---

  protected final void info (final String s, final Ansi.C... codes) {
    if (logmon.isLoggable (BasicLevel.INFO))
      logmon.log (BasicLevel.INFO, s);

    Ansi.info (s, codes);
  }

  // ---

  private static final Not.Handlers<Task> nh = new Not.Handlers<Task> ();
  static {

    nh.put ("real-stop", new Not.Handler<Task> () {
    	protected void p (final AgentId from, final Task to, final Not n)
	  throws Exception { to.real_stop (n); }});

    // ---

    nh.put ("ping", new Not.Handler<Task> () {
    	protected void p (final AgentId from, final Task to, final Not n)
	{ n.maybeAck (); }});	// pong directly.

    // ---
  }

  // ---

  protected void real_stop (final Not cause) throws Exception {
    logmon.log (BasicLevel.DEBUG, "Real-stop notification received.");
    Main.stop (cause);
  }

  // ---

  // Force overriding.
  protected abstract String getLogTopic ();

  // ---

  protected final String getProp (final String key) {
    return env.get (key);
  }

  protected final String setProp (final String key, final String value) {
    env.put (key, value);
    return value;
  }

  // ---

  public void react (AgentId from, Notification n) throws Exception {
    if (nh.maybeHandle (from, this, n, true) == Not.Status.HANDLED) return;
    super.react (from, n);
  }

  // ---

}
