/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.depl.opcode.sensors;

import java.io.Serializable;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.AgentServer;
import fr.dyade.aaa.common.Debug;
import fr.lig.erods.sams.common.util.TransientException;
import org.apache.commons.lang3.text.StrTokenizer;
import org.apache.commons.lang3.tuple.Pair;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

public class UnixLoadMonitor extends TimedMonitor {

  // ---

  private static final Logger logmon =
    Debug.getLogger (UnixLoadMonitor.class.getName ());

  // static private final int cores = Runtime.getRuntime ().availableProcessors ();

  // ---

  private UnixLoadMonitor (final String name, final AgentId... dests)
    throws IOException {
    super (name, dests);
  }

  private static final StrTokenizer tkzr = new StrTokenizer ();

  private float l1/*, l2, l3 */;

  private String readLoadavg () throws FileNotFoundException, IOException {
    BufferedReader br = null;
    try {
      br = new BufferedReader (new FileReader ("/proc/loadavg"));
      return br.readLine ();
    } finally {
      if (br != null) br.close ();
    }
  }

  protected final Serializable measure () {
    try {
      final String line = readLoadavg ();
      final String info [] = tkzr.reset (line).getTokenArray ();
      this.l1 = Float.parseFloat (info [0]);
      // this.l2 = Float.parseFloat (info [1]);
      // this.l3 = Float.parseFloat (info [2]);

      final short serverId = AgentServer.getServerId ();
      final Pair<Short, Float> val = Pair.of (serverId, new Float (l1));

      return val;
    } catch (Exception e) {
      logmon.log  (BasicLevel.ERROR,
		   "Unable to update Unix load `"+ this.getName () +"'.",
		   e);      // heum…
      return null;
    }
  }

  // ---

  private static AgentId monitor;

  public static void start (final String name,
			    final long ms_period, final AgentId... dests)
    throws TransientException {
    assert (monitor == null);

    if (logmon.isLoggable (BasicLevel.DEBUG))
      logmon.log  (BasicLevel.DEBUG,
		   "Starting Unix load monitor for node `"+ name +"'.");

    try {
      final UnixLoadMonitor a = new UnixLoadMonitor (name, dests);
      a.deploy ();
      monitor = a.getId ();
      TimedMonitor.start (monitor, ms_period);
    } catch (IOException e) {
      logmon.log  (BasicLevel.ERROR,
		   "Unable to start Unix load monitor for node `"+ name +"'.",
		   e);
    }
  }

  // ---

  public static void stop ()
    throws TransientException {
    assert (monitor != null);

    if (logmon.isLoggable (BasicLevel.DEBUG))
      logmon.log  (BasicLevel.DEBUG, "Stopping Unix load monitor.");

    TimedMonitor.stop (monitor);
    monitor = null;
  }

  // ---

  protected String getLogTopic() { return UnixLoadMonitor.class.getName (); }

  // ---

}
