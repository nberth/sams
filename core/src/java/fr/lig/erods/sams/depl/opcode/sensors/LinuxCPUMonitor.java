/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.depl.opcode.sensors;

import java.io.IOException;
import java.io.Serializable;

import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.AgentServer;
import fr.dyade.aaa.common.Debug;
import fr.lig.erods.sams.common.util.TransientException;
import fr.lig.erods.sams.common.util.linux.CpuTicks;
import org.apache.commons.lang3.tuple.Pair;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

public class LinuxCPUMonitor extends TimedMonitor {

  // ---

  private final double alpha;
  private CpuTicks prev;
  private float cpu = 0.0f;

  // ---

  private static final Logger logmon =
    Debug.getLogger (LinuxCPUMonitor.class.getName ());

  // ---

  private LinuxCPUMonitor (final String name, final double alpha,
			   final AgentId... dests) throws IOException {
    super (name, dests);
    this.alpha = alpha;
    this.prev = CpuTicks.readStat (); // read it now!
  }

  // ---

  private double computeCpu (final double prev, final double alpha,
			     final double curr) {
    return alpha * prev + (1.0 - alpha) * curr;
  }

  // ---

  protected final Serializable measure () {
    try {

      final CpuTicks now = CpuTicks.readStat ();

      final double cpu = CpuTicks.relativeUsage (this.prev, now);
      this.cpu = new Float (computeCpu (this.cpu, this.alpha, cpu));

      final short serverId = AgentServer.getServerId ();
      final Pair<Short, Float> val = Pair.of (serverId, new Float (this.cpu));

      this.prev = now;

      return val;

    } catch (IOException e) {
      logmon.log  (BasicLevel.ERROR,
		   "Unable to update Linux CPU load `"+ this.getName () +"'.",
		   e);
      return null;
    }
  }

  // ---

  private static AgentId monitor;

  // ---

  public static void start (final String name, final double alpha,
			    final long ms_period, final AgentId... dests)
    throws TransientException {
    assert (monitor == null);

    if (logmon.isLoggable (BasicLevel.DEBUG))
      logmon.log  (BasicLevel.DEBUG,
		   "Starting Linux CPU monitor for node `"+ name +"'.");

    try {
      final LinuxCPUMonitor a = new LinuxCPUMonitor (name, alpha, dests);
      a.deploy ();
      monitor = a.getId ();
      TimedMonitor.start (monitor, ms_period);
    } catch (IOException e) {
      logmon.log  (BasicLevel.ERROR,
		   "Unable to start Linux CPU monitor for node `"+ name +"'.",
		   e);
    }
  }

  // ---

  public static void stop ()
    throws TransientException {
    assert (monitor != null);

    if (logmon.isLoggable (BasicLevel.DEBUG))
      logmon.log  (BasicLevel.DEBUG, "Stopping Linux CPU monitor.");

    TimedMonitor.stop (monitor);
    monitor = null;
  }

  // ---

  protected String getLogTopic() { return LinuxCPUMonitor.class.getName (); }

  // ---

}
