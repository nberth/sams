/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.depl.opcode.sensors;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.TimerTask;
import java.util.Arrays;

import fr.dyade.aaa.agent.Agent;
import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.AgentServer;
import fr.dyade.aaa.agent.Channel;
import fr.dyade.aaa.agent.Notification;
import fr.lig.erods.sams.common.opcode.Measure;
import fr.lig.erods.sams.common.util.PersistentHashMap;
import fr.lig.erods.sams.common.util.PersistentWakeUpTask;
import fr.lig.erods.sams.common.util.TransientException;

public abstract class TimedMonitor extends Agent {

  // ---

  protected final Set<AgentId> dests;

  // ---

  protected TimedMonitor (final String name, final Set<AgentId> dests) {
    super (name);
    this.dests = dests;
    // this.dests.addAll (dests);
  }

  protected TimedMonitor (final String name, final AgentId... dests) {
    this (name, new HashSet<AgentId> (Arrays.asList (dests)));
  }

  // ---

  // ---

  protected abstract Serializable measure ();

  // ---

  /**
   * Notification used to sense periodically.
   *
   * <p>Note the only reason why this class is public is so that it can be
   * instantiated by {@link fr.dyade.aaa.agent.WakeUpTask} (through {@link
   * PersistentWakeUpTask}).
   */
  public static class MeasureNot extends Notification {}

  // ---

  private static class SubscribeNot extends Notification {
    final Set<AgentId> dests;
    SubscribeNot (final AgentId... dests) {
      this.dests = new HashSet<AgentId> (Arrays.asList (dests));
    }
  }

  // ---

  /**
   * Basic helper for destination subscription.
   */
  public static void subscribe (final AgentId a, final AgentId... dests) {
    Channel.sendTo (a, new SubscribeNot (dests));
  }

  // ---

  public void react (final AgentId from, final Notification n)
    throws Exception {
    if (n instanceof MeasureNot) {
      final Serializable value = this.measure ();
      if (value != null) {
	final Measure<?> m = Measure.of (this.getName (), value);
	for (final AgentId a: this.dests)
	  sendTo (a, m);
      }
      return;
    }
    if (n instanceof SubscribeNot) {
      this.dests.addAll (((SubscribeNot) n).dests);
      return;
    }
    super.react (from, n);
  }

  // ---

  // private static final class WutMap
  //   extends PersistentHashMap<AgentId, PersistentWakeUpTask>
  //   implements Persistent.Fuzzy {
  //   protected void newRef () { wm = this; }
  // }

  // private static transient WutMap wm = null;

  // static {
  //   Local.runOnInit (new Runnable () { public void run () {
  //     wm = new WutMap ();
  //   }});
  // }

  private static final class WutMap
    extends PersistentHashMap<AgentId, PersistentWakeUpTask> {
    protected void newRef () { wm.write (this); }
  }

  private static
  final PersistentHashMap.Ref<AgentId, PersistentWakeUpTask> wm =
    new PersistentHashMap.Ref<AgentId, PersistentWakeUpTask>
    (new PersistentHashMap.C<WutMap> () {
      public WutMap b () { return new WutMap (); }
    });

  // ---

  public static void start (final AgentId a, final long ms_period)
    throws IOException, TransientException {
    assert (! wm.containsKey (a));

    // Schedule early startup measurement with a transient timer task...
    AgentServer.getTimer ().schedule (new TimerTask () {
	public void run () { Channel.sendTo (a, new MeasureNot ()); }
      }, ms_period >> 2);

    wm.put (a, new PersistentWakeUpTask (a, MeasureNot.class, ms_period));
  }

  // ---

  public static void stop (final AgentId a)
    throws TransientException {
    final PersistentWakeUpTask wut = wm.get (a);
    assert (wut != null);
    wut.cancel ();
    wm.remove (a);
  }

  // ---

}
