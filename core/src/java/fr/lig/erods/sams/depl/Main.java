/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.depl;

import java.lang.reflect.Constructor;

import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.AgentServer;
import fr.dyade.aaa.agent.Channel;
import fr.dyade.aaa.common.Debug;
import fr.lig.erods.sams.common.Env;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.Sams;
import fr.lig.erods.sams.depl.opcode.Task;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Class encapsulating static methods to be used to start and stop a remote
 * agent server.
 */
public abstract class Main {

  public static AgentId parent;
  public static final Logger logmon = Debug.getLogger (Main.class.getName ());

  // ---

  /** Abstract final class  */
  private Main () {}

  // ---

  private static AgentId deployLocalTask (String taskClassName)
    throws Exception {

    final Task task;
    final Class<? extends Task> taskClass;
    final Constructor<? extends Task> ctr;

    taskClass = Class.forName (taskClassName).asSubclass (Task.class);
    ctr = taskClass.getConstructor (Env.class);

    task = (ctr != null)
      ? ctr.newInstance (Sams.env)
      : taskClass.newInstance ();
    task.deploy ();

    return task.getId ();
  }

  // ---

  /**
   * Stops this remote agent server.
   *
   * <p>To be called from an agent context, in response to a notification asking
   * to stop the remote agent server before undeployment.
   */
  public static void stop (final Not cause) {
    logmon.log (BasicLevel.DEBUG, "Acknowledging notification in `Main.stop()'.");
    // cause.setProp ("deployment.server.id", AgentServer.getServerId ());
    cause.maybeAck ();

    // XXX 1s is totally arbitrary!
    logmon.log (BasicLevel.DEBUG, "Scheduling server stop in 1s.");
    AgentServer.stop (false, 1000, false);
  }

  // ---

  /**
   * Starts an agent server on a remote site.
   *
   * <p>At least 4 arguments are expected on the command line, in order:
   *
   * <ol>
   *
   * <li>Agent server identifier (a short integer);
   *
   * <li>Agent server name;
   *
   * <li>Parent agent (<i>i.e.</i> sams' center agent identifier);
   *
   * <li>Class name of the task agent to deploy on this agent server; this must
   * be a subclass of {@link fr.lig.erods.sams.depl.opcode.Task}.
   *
   * </ol>
   *
   * <p>Acknowledges the effective start-up by sending a <b>deployment_done</b>
   * artifact to the given parent agent, with the entry
   * <em>deployment.agent.id</em> being the agent identifier of the newly
   * created task agent.
   */
  public static void main (String args []) {
    try {
      assert (args.length >= 4);

      AgentServer.init (args);
      Sams.init (args [1]);

      parent = AgentId.fromString (args [2]);
      AgentServer.start ();

      final AgentId tsk = deployLocalTask (args [3]);

      final Not ge =
	new Not ("deployment_done", Not.e ("deployment.agent.id", tsk));
      logmon.log (BasicLevel.DEBUG, "Sending `deployment_done' notification.");
      Channel.sendTo (parent, ge);

    } catch (Exception e) {
      e.printStackTrace (System.err);
      System.exit (1);
    }
  }

  // ---
}
