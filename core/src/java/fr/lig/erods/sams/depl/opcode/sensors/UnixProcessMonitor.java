/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.depl.opcode.sensors;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.TimerTask;

import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.AgentServer;
import fr.dyade.aaa.agent.Channel;
import fr.dyade.aaa.common.Debug;
import fr.lig.erods.sams.common.Not;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * It sends generic "task_failure" notifications, whenever it detects that the
 * the given PID file does not relates to an existing process.  Note this one
 * should communicate directly to the managing tier, not the model.
 *
 * Does not survive failures yet…
 */
public class UnixProcessMonitor extends TimerTask {

  private static final Logger logmon =
    Debug.getLogger (UnixProcessMonitor.class.getName ());

  // ---

  private final String pidFile;
  private final AgentId dest;
  private String procDir;

  // ---

  private UnixProcessMonitor (final AgentId dest, final String pidFile) {
    this.dest = dest;
    this.pidFile = pidFile;
    this.procDir = null;
  }

  // ---

  private int readPid () throws FileNotFoundException, IOException  {
    BufferedReader br = null;
    try {
      br = new BufferedReader (new FileReader (this.pidFile));
      return new Integer (br.readLine ());
    } finally {
      if (br != null) br.close ();
    }
  }

  // ---

  public void run () {

    if (this.procDir == null) {	// TODO manage #tries?
      try {
	this.procDir = "/proc/"+ readPid ();
      } catch (Exception e) {
	logmon.log  (BasicLevel.ERROR, "Unable to read PID file `"+
		     this.pidFile +"'.", e);
	return;
      }
    }

    final File d = new File (this.procDir);
    if (d.exists () && d.isDirectory ()) return; // Nothing to send for now…

    // FIXME We should certainly check the associated command…

    final Not i = new Not ("task_failure");
    Channel.sendTo (this.dest, i);
    stop ();			// This became irrelevant…
  }

  // ---

  private static UnixProcessMonitor tsk = null;

  public static void start (final AgentId dest, final String pidFile,
			    final long ms_period) {
    assert (tsk == null);

    logmon.log  (BasicLevel.DEBUG, "Starting Unix process monitor.");

    try {
      tsk = new UnixProcessMonitor (dest, pidFile);
      AgentServer.getTimer ()
	.scheduleAtFixedRate (tsk, ms_period >> 2, ms_period);
    } catch (Exception e) {
      logmon.log  (BasicLevel.ERROR, "Unable to read PID file `"+pidFile +"'.");
    }
  }

  // ---

  public static void stop () {
    if (tsk == null) return;
    logmon.log  (BasicLevel.DEBUG, "Stopping Unix process monitor.");
    tsk.cancel ();
    tsk = null;
  }


  // ---

}
