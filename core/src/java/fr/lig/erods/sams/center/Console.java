/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.center;

import java.io.File;
import java.io.IOException;

import fr.dyade.aaa.agent.AgentServer;
import fr.dyade.aaa.common.Debug;
import fr.lig.erods.sams.common.Sams;
import fr.lig.erods.sams.common.util.Ansi.C;
import fr.lig.erods.sams.common.util.Ansi;
import fr.lig.erods.sams.common.util.TransientException;
import jline.Completor;
import jline.ConsoleReader;
import jline.SimpleCompletor;
import org.apache.commons.lang3.text.StrMatcher;
import org.apache.commons.lang3.text.StrTokenizer;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Basic console for interactive usage.
 */
public class Console {

  // ---

  private static final Logger slogmon =
    Debug.getLogger (Console.class.getName ());

  // ---

  private static final Cmd.Map cmds = new Cmd.Map ();
  static {
    // Setup default commands.
    cmds.put  ("quit", new Cmd.Basic () { protected void run () {
      AgentServer.stop ();
      System.exit (0);
    }});
  }

  // ---

  protected static final StrTokenizer tokenizer;
  static {
    StrTokenizer tkzr = new StrTokenizer ();
    tkzr = tkzr.setDelimiterMatcher (StrMatcher.charSetMatcher (" \t\n\r\f"));
    tkzr = tkzr.setQuoteMatcher (StrMatcher.quoteMatcher ());
    tokenizer = tkzr;
  }

  // ---

  private static void run (final Cmd.Map mdCmds)
    throws IOException {

    final ConsoleReader reader = new ConsoleReader ();
    reader.setBellEnabled (false);
    reader.setUseHistory (true);
    reader.getHistory ().setHistoryFile
      (new File (System.getProperty ("user.home") + "/.sams-history"));

    cmds.putAll (mdCmds);
    // Map<String, Cmd> currentMap = initCmds (cmds);
    final Completor completor = new SimpleCompletor
      (Console.cmds.keySet ().toArray (new String [] {}));
    reader.addCompletor (completor);

    String line;
    final String prompt = new Ansi.S ("> ", C.BOLD, C.YELLOW).toString ();
    while ((line = reader.readLine (prompt)) != null) {

      final String [] args = tokenizer.reset (line).getTokenArray ();
      if (args.length == 0) continue;

      if (Console.cmds.containsKey (args [0])) {
	int status = Console.cmds.get (args [0]).run (args);
	if (status != 0) {
	  System.err.println ("Return status: "+ status);
	}
      } else {
	System.err.println ("Unknown command: "+ args [0]);
      }
    }
  }

  // ---

  private static void tryRunConsole () throws IOException {
    try {
      Console.run (ModelDriver.getAllCmds ());
      return;
    } catch (TransientException e) {
      slogmon.log (BasicLevel.DEBUG, "Waiting 100ms for restoration…", e);
    }
    try {
      Thread.sleep (100);	// 100ms.
    } catch (InterruptedException exc) {}
    tryRunConsole ();		// XXX tail-call in Java?
  }

  // ---

  private static final String CENTER_NAME = "sams-center";

  // ---

  /**
   * Launches a console.
   *
   * <p>The only accepted argument must be either:
   *
   * <ul>
   *
   * <li><b>start</b>: start the center agent server; or
   *
   * <li><b>restore</b>: restore a previously interrupted center agent server.
   *
   * </ul>
   */
  public static void main (final String args[]) {
    try {
      AgentServer.init (new String [] {"0", CENTER_NAME});

      if (args.length == 0 || "start".equals (args [0])) {
	Sams.init (CENTER_NAME);
	ModelDriver.init ();
      } else if ("restore".equals (args [0])) {
	Sams.restore (CENTER_NAME);
	// Take care center agent will not be restored immediately…
      } else {
	System.err.println
	  ("Error: Missing argument (either `start', `restore', or nothing).");
	System.exit (1);
      }

      System.err.println ("Starting server!");
      AgentServer.start ();
      tryRunConsole ();
    } catch (Exception e) {
      slogmon.log (BasicLevel.ERROR, "Caught exception in `main ()'!", e);
    }
  }

  // ---

}
