/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.center;

import java.io.Serializable;

import fr.dyade.aaa.agent.Agent;
import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Channel;
import fr.dyade.aaa.agent.Notification;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.opcode.Impulse;
import fr.lig.erods.sams.common.util.Ansi.C;
import fr.lig.erods.sams.common.util.Ansi;
import fr.lig.erods.sams.common.util.PersistentWakeUpTask;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Abstract type of agents running the decision code. Such a class is in charge
 * of encapsulating a <em>tick</em> object (typically the result of the
 * compilation of a synchronous program), that outputs commands based on an
 * input vector <em>I</em> and its internal state when its <em>step(I)</em>
 * method is called. This object encodes the <em>management decision logic</em>
 * related to the operations performed on the <em>managed system</em>. The
 * decision code, for its part, determines how to build the input vector, when
 * to call the <em>tick.step()</em> method, and finally how to interpret the
 * resulting output vector.
 *
 * <p>Any concrete decision code agent understands {@link
 * fr.lig.erods.sams.common.Not} notifications that carry the following
 * commands:
 *
 * <ul>
 *
 * <li><b>tick.start</b>: starts the periodic executions the tick. This
 * notification MUST convey the following information:
 *
 *   <ul>
 *
 *   <li><em>tick.max-delay</em>: the <em>idle time</em> (in milliseconds, as an
 *   instance of {@link String}) after which a step of the tick is triggered
 *   even if no new impulse has been received. The <em>idle time</em> is defined
 *   as the duration since the last reaction;
 *
 *   </ul>
 *
 * <li><b>tick.stop</b>: stops the periodic executions of the tick.
 *
 * </ul>
 *
 * <p>As is, it is also able to receive {@link
 * fr.lig.erods.sams.common.opcode.Impulse} notifications; the responsibility to
 * fill the {@link #ih} map of impulse inputs handlers is delegated to
 * subclasses.
 *
 * @param <T> the type of accepted tick objects
 * @param <I> the type of input vectors to feed a tick of type <tt>T</tt> (see
 *            {@link #iv})
 * @param <O> the type of output vectors produced by a tick of type <tt>T</tt>
 */
public abstract class DecisionCode<T extends Tick<I, O> & Serializable,
				     I extends Serializable, O>
  extends Agent {

  /**
   * The encapsulated <em>tick</em> object, encoding the management decision
   * logic.
   */
  protected T tick;

  /**
   * The only instance of the input vector. In principle, an input vector
   * contains two possibly empty slices: (i) the impulses part contains Boolean
   * variables (or of another type interpreted as such in the
   * <em>tick.step()</em> method) that notify the reception of the impulse; and
   * (ii) the measured inputs that record the freshest values received.
   */
  protected I iv;

  /**
   * A mapping from impulses to associated handlers. Any notification of a new
   * impulse recorded in this map triggers the {@link #run_step()} method of the
   * decision code.
   *
   * <p> Its suggested usage is as in the following example:
   *
   * <codeblock><pre>
   * ih.put ("impulse name", new Impulse.Handler () {
   *   protected void assign (boolean b) { iv.name = b; }
   * });
   * </pre></codeblock>
   */
  protected final Impulse.MapReceiver ih = new Impulse.MapReceiver () {
      protected void onNewImpulse () {
	// Force immediate execution of the step;
	run_step ();
      }
    };

  // ---

  private long period;
  private PersistentWakeUpTask uptsk;
  private transient Ansi.S prefix;

  // ---

  /**
   * Only available constructor.
   */
  public DecisionCode (final T tick, final I iv) {
    super ("decision-code"); // fixed!?
    this.tick = tick;
    this.tick.reset ();
    this.iv = iv;
  }

  // ---

  /**
   * Recovers the internal counter when restoring after a failure.
   */
  protected void agentInitialize (final boolean firstTime) throws Exception {
    super.agentInitialize (firstTime);
    this.prefix = new Ansi.S (this.getName (), C.GREEN, C.BOLD);
    if (! firstTime && this.uptsk != null)
      uptsk.onRestore ();
  }

  // ---

  protected final void info (final String s, final Ansi.C... codes) {
    if (logmon.isLoggable (BasicLevel.INFO))
      logmon.log (BasicLevel.INFO, this.getName () +": "+ s);

    Ansi.info (this.prefix, s, codes);
  }

  // ---

  private static final Not.Handlers<DecisionCode<?, ?, ?>> nh =
    new Not.Handlers<DecisionCode<?, ?, ?>> ();
  static {
    nh.put ("tick.start", new Not.Handler<DecisionCode<?, ?, ?>> () {
    	protected void p (final AgentId from, final DecisionCode<?, ?, ?> to,
			  final Not n) { to.start (n); }});
    nh.put ("tick.stop", new Not.Handler<DecisionCode<?, ?, ?>> () {
    	protected void p (final AgentId from, final DecisionCode<?, ?, ?> to,
			  final Not n) { to.stop (n); }});
  }

  // ---

  /**
   * Checks possible assertions on values in the input vector, to detect some
   * errors…
   *
   * <p>Does nothing by default.
   */
  protected void checkInputConstraints () {}

  /**
   * Interprets the output vector resulting from executions of {@link
   * Tick#step}.
   *
   * <p>Does nothing by default.
   */
  protected void interpretOutputs (O o) {}

  // ---

  /**
   * Executes the <em>tick.step(I)</em> method once. The impulse part of the
   * input vector <em>I</em> is filled according to the contents of the {@link
   * #ih} map prior to the execution of the step, and reset afterward. The
   * {@link #interpretOutputs(O)} method is called eventually with the resulting
   * output vector.
   */
  private void run_step () {
    logmon.log (BasicLevel.DEBUG, "Executing one step.");

    // Raise all impulse inputs.
    ih.raisePendings ();

    // Execute tick.step ()
    final O o = this.tick.step (this.iv);

    // Restore initial state of impulse inputs, and clear pending ones (note
    // that none have been added during the execution of the tick…).
    ih.clearPendings ();

    // Scedule next step time, in case no new impulse arrives meanwhile…
    this.sched_next_step ();

    this.interpretOutputs (o);
  }

  // ---

  private static class OpCodeNot extends Notification {
    public String name;
    public AgentId opcode;
    private OpCodeNot (final String name, final AgentId opcode) {
      this.name = name;
      this.opcode = opcode;
    }
  }

  // ---

  /**
   * Binds a named operating code agent to the given decision code. Note the
   * former agent must have already been deployed (XXX check this), and the
   * decision code identified by <tt>dcid</tt> must not have been started yet.
   *
   * @param dcid the decision code agent to bind the operating code to
   * @param name the name of the operating code agent, in the point of view of
   *             the decision code
   * @param aid the operating code agent
   */
  public static void bindOpCode (final AgentId dcid,
				 final String name, final AgentId aid) {
    Channel.sendTo (dcid, new DecisionCode.OpCodeNot (name, aid));
  }

  // ---

  /**
   * Registers a named operating code agent.
   */
  protected abstract void registerOpCode (final String name,
					  final AgentId opcode);

  // ---

  /**
   * Class used internally to trigger the execution of {@link #run_step()} when
   * the maximum idle time delay is reached.
   *
   * <p>Note the only reason why it is public is that {@link
   * fr.dyade.aaa.agent.WakeUpTask} needs to be able to build instances of this
   * class.
   */
  public static class ForceStepNot extends Notification {}

  // ---

  /**
   * Schedules next step in <code>period</code> ms.
   */
  private void sched_next_step () {
    if (uptsk != null)
      // note a timed notification may already have been sent, in which case we
      // will react again very soon; yet it does not really matter…
      uptsk.cancel ();
    uptsk = new PersistentWakeUpTask
      (this.getId (), ForceStepNot.class, this.period);
  }

  // ---

  protected void start (final Not cause) {
    logmon.log (BasicLevel.INFO, "Enabling periodic executions of the model.");
    this.period = new Long ((String) cause.getProp ("tick.max-delay"));
    this.sched_next_step ();
    cause.maybeAck ();
  }

  // ---

  protected void stop (final Not cause) {
    logmon.log (BasicLevel.INFO, "Disabling periodic executions of the model.");
    uptsk.cancel ();
    uptsk = null;
    cause.maybeAck ();
  }

  // ---

  public void react (final AgentId from, final Notification n)
    throws Exception {
    if (ih.maybeHandle (n)               == Impulse.Status.HANDLED) return;
    if (nh.maybeHandle (from, this, n, true) == Not.Status.HANDLED) return;
    if (n instanceof ForceStepNot) { this.run_step ();              return; }
    if (n instanceof OpCodeNot)
      this.registerOpCode (((OpCodeNot) n).name, ((OpCodeNot) n).opcode);
    else
      super.react (from, n);
  }

  // ---

  protected String getLogTopic() { return DecisionCode.class.getName (); }

  // ---
}
