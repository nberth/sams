/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.center;

import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.util.Map;
import java.util.HashMap;

import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.AgentServer;
import fr.dyade.aaa.agent.ServerConfigHelper;
import fr.dyade.aaa.common.Debug;
import fr.lig.erods.sams.common.Env;
import fr.lig.erods.sams.common.Node;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.Sams;
import fr.lig.erods.sams.common.util.CommandBuilder;
import fr.lig.erods.sams.common.util.ISet;
import fr.lig.erods.sams.common.util.Persistent;
import fr.lig.erods.sams.common.util.ProcessHandle;
import fr.lig.erods.sams.common.util.TransientException;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Instances of <tt>Remote</tt> represent and encapsulate remote tasks
 * management. Notably, it encapsulates an environment (an instance of {@link
 * fr.lig.erods.sams.common.Env}) from which values defining shell commands are
 * determined.
 *
 * <p>The deployment of the remote task is done by calling either {@link
 * #startDeployment()} or {@link #startDeployment(Not)}.  Internally, the
 * deployment sequence consists in executing the shell commands defined by the
 * following keys in turn:
 *
 * <ul>
 *
 * <li><b>remote.deploy</b> creates the remote site directory on the destination
 * node, and transfers files listed in <b>remote.files</b> directly in it
 *
 * <li><b>remote.detar</b> decompresses archives listed in the
 * <b>remote.tars</b> property directly in the remote site directory
 *
 * <li><b>remote.prepare.cmd</b> (executed in the remote directory) configures
 * the remote agent server (defined in <tt>sams.properties</tt>)
 *
 * <li><b>remote.start.cmd</b> (executed in the remote directory) starts the
 * remote agent server
 *
 * </ul>
 *
 * If the {@link #startDeployment(Not)} was used, then the given notification is
 * sent to acknowledge the end of the startup sequence.
 *
 * <p>On the other hand, the undeployment of a remote site starts when either
 * {@link #startUndeployment()} or {@link #startUndeployment(Not)} are
 * called. In this case, the command defined by <b>remote.undeploy</b> is
 * executed, that should remove the remote site directory. Note that the remote
 * agent server <em>must</em> have been stopped prior to its undeployment. When
 * this operation terminates, any resource allocated on the local agent server
 * for the communication with the remote one is released; this completion can be
 * notified by requesting a notification to be sent (see {@link
 * #startUndeployment(Not)}).
 *
 * <p>Instances of this class should not travel across agents; instead, use the
 * value of {@link #id} to identify each instance.
 *
 */
public class Remote extends CommandBuilder {

  // ---

  /** Public identifier for each remote object (&gt; 0). */
  public final int id;

  /** The node the remote task is deployed onto. */
  public final Node node;

  /** Remote agent identifier. */
  public AgentId agent = null;

  /** Tells whether the local agent server has been notified about the remote
      one. */
  private boolean registered = false;

  // ---

  /** This integer is guaranteed to never be used as identifier for any instance
      of <tt>Remote</tt>. */
  public static final int NONE = 0;

  private static final Logger slogmon =
    Debug.getLogger (Remote.class.getName ());

  // ---

  /**
   * The only constructor of <tt>Remote</tt> instances.
   *
   * @param taskEnv the environment defining properties of this object.
   *
   */
  public Remote (final Env taskEnv, final Node dest, final AgentId parent)
    throws TransientException, Node.OutOfPortException {
    super (initEnv (taskEnv, dest));

    this.id = allocId ();
    this.node = dest;

    this.env.put ("remote.ident", Integer.toString (this.id));
    if (! this.env.containsKey ("remote.dir.template"))
      this.env.put ("remote.dir.template", "/tmp/${remote.name}");
    this.env.put ("remote.parent.id", parent.toString ());
    this.env.put ("remote.arch", this.env.get (this.node.getHost () +".arch",
					       this.env.get ("remote.arch")));
    this.env.put ("name", this.env.get ("remote.name"));
    this.env.put ("arch", this.env.get ("remote.arch")); // in case…
  }

  // ---

  private static Env initEnv (final Env taskEnv, final Node n)
    throws TransientException, Node.OutOfPortException {
    final String local = (String) Sams.load ("local.host");
    final Env env = new Env ();
    env.putAll (Sams.env);	// Add common environment first
    env.putAll (taskEnv);	// so we can override existing entries.
    env.put ("user", n.user);
    env.put ("host", n.host);
    env.put ("center.host", local);
    env.put ("center.port", Integer.toString (Node.allocPort (local)));
    env.put ("remote.host", n.host);
    env.put ("remote.port", Integer.toString (n.allocPort ()));
    return env;
  }

  // ---

  /**
   * Returns the name of the remote (the value of <b>remote.name</b>).
   */
  public String toString () { return this.getName (); }

  // ---

  /** The name of the class of each new network component created to communicate
      with remote agent servers (value = {@value}). */
  private static final String NETWORK_SERVICE =
    "fr.dyade.aaa.agent.SimpleNetwork";

  // ---

  /**
   * Configures the agent server so that it is able to communicate with the
   * remote one represented by this instance.
   *
   * <p>Note this method MUST be executed during an agent reaction.
   */
  private void register () {

    this.check ();

    if (this.registered) {
      logmon.log (BasicLevel.ERROR,
		  "Remote agent server for "+ this +" already registered.");
      return;
    }

    final int     localPort = Integer.parseInt (getProp ("center.port"));
    final int    remotePort = Integer.parseInt (getProp ("remote.port"));
    final String remoteHost = getProp ("remote.host");
    final String remoteName = this.getName ();

    logmon.log (BasicLevel.DEBUG, "Registering distant agent server for `"+
		remoteName +"'.");

    // synchronized (Remote.class) {
    try {
      final ServerConfigHelper c = new ServerConfigHelper (false);
      c.addDomain ("D" + this.id, NETWORK_SERVICE,
		   AgentServer.getServerId (), localPort);
      c.addServer (this.id, remoteHost, "D" + this.id,
		   remotePort, remoteName);
      c.commit ();
      this.registered = true;
    } catch (java.lang.Exception e) {
      slogmon.log (BasicLevel.ERROR, e);
      // Huh?
    }
    // }
  }

  // ---

  /**
   * Unregister the remote agent server from the configuration of the local
   * one.
   *
   * <p>The {@link register()} method MUST have already been called.
   *
   * <p>Note this method MUST be executed during an agent reaction.
   */
  private void unregister () {

    this.check ();

    if (! this.registered) {
      logmon.log (BasicLevel.ERROR,
		  "Remote agent server for "+ this +" not yet registered.");
      return;
    }

    if (logmon.isLoggable (BasicLevel.DEBUG))
      logmon.log (BasicLevel.DEBUG, "Unregistering distant agent server for `"+
		  this +"'.");

    // synchronized (Remote.class) {
    try {
      final ServerConfigHelper c = new ServerConfigHelper (false);
      // Does not seem to be instantaneous… at all!
      c.removeServer (this.id);
      c.removeDomain ("D" + this.id);
      c.commit ();
    } catch (java.lang.Exception e) {
      slogmon.log (BasicLevel.ERROR, e);
      this.registered = false;
    }
    // }
  }

  // ---

  // see
  // `http://stackoverflow.com/questions/434718/sockets-discover-port-availability-using-java'
  private boolean portIsAvailable (final int port) {
    ServerSocket ss = null;
    DatagramSocket ds = null;
    try {
      ss = new ServerSocket (port);
      ss.setReuseAddress (true);
      ds = new DatagramSocket (port);
      ds.setReuseAddress (true);
      return true;
    } catch (IOException e) {
    } finally {
      if (ds != null) ds.close();
      if (ss != null) {
	try { ss.close(); }
	catch (IOException e) { /* should not be thrown */ }
      }
    }
    return false;
  }

  // ---

  // NEVER call it during an agent's reaction!
  private void waitForPortAvailability ()
    throws TransientException {

    // Augment the chances to take these changes into account…:
    while (! portIsAvailable (Integer.parseInt (getProp ("center.port"))))
      Thread.yield ();

    // Make sure it can be used again if we (the center) execute on the same
    // node:
    if (this.node.host.equals ((String) Sams.load ("local.host")))
      while (! portIsAvailable (Integer.parseInt (getProp ("remote.port"))))
	Thread.yield ();
  }

  // ---

  /** Tells whether the instance has already been destroyed. */
  private boolean destroyed = false;

  /**
   * Releases resources allocated internally. After calling this method, the
   * instance should never be used anymore; ideally, it should not be referenced
   * neither.
   *
   * @throws TransientException when shared persistent data have not yet been
   *                            recovered.
   */
  public synchronized void destroy () throws TransientException {
    if (this.destroyed) return;

    // Release allocated ports, and the identifier.
    final String local = (String) Sams.load ("local.host");
    Node.releasePort (local, Integer.parseInt (getProp ("center.port")));
    node.releasePort        (Integer.parseInt (getProp ("remote.port")));
    freeId (this.id);

    this.destroyed = true;
  }

  // ---

  // /**
  //  * Automatically calls {@link #destroy()}.
  //  */
  // protected void finalize ()
  //   throws Throwable {
  //   super.finalize ();
  //   this.destroy ();
  // }

  /**
   * Helper to check the internal state of the object.
   */
  private void check () {
    if (this.destroyed)
      throw new IllegalStateException ("Remote instance already destroyed!");
  }

  // ---------------------------------------------------------------------------

  private ProcessHandle deploy ()
    throws UnknownCommandException, MalformedCommandException,
    java.io.IOException {
    return this.byKey ("remote.deploy");
  }

  private ProcessHandle redetar ()
    throws UnknownCommandException, MalformedCommandException,
    java.io.IOException {
    return this.byKey ("remote.detar");
  }

  private ProcessHandle undeploy ()
    throws UnknownCommandException, MalformedCommandException,
    java.io.IOException {
    return this.byKey ("remote.undeploy");
  }

  private enum ExecOption { PTY, DETACHED };

  private ProcessHandle remoteExec (final String key, ExecOption... options)
    throws UnknownCommandException, MalformedCommandException,
    java.io.IOException {
    final String c = getProp (key);
    String cmd = getProp ("remote.execute.prefix");
    for (ExecOption o: options) switch (o) {
      case PTY:	     cmd = getProp ("remote.execute.prefix");	       break;
      case DETACHED: cmd = getProp ("remote.execute.detached.prefix"); break;
      }
    return (c == null) ? unknown (key) : exec (appendArgs (splitCmd (cmd), c));
  }

  // ---
  // Site-related commands:

  /**
   * Execute the <b>remote.prepare.cmd</b> command on the remote site, that
   * should setup the remote site prior to the startup of the agent server.
   *
   * @return a process handle
   */
  private ProcessHandle prepare ()
    throws UnknownCommandException, MalformedCommandException,
    java.io.IOException {
    return remoteExec ("remote.prepare.cmd");
  }

  /**
   * Execute the <b>remote.start.cmd</b> command on the remote site, that should
   * start the remote agent server.
   *
   * @param opt whether the remote command's input-output streams should be
   *            detachable or not
   * @return    a process handle
   */
  private ProcessHandle start (ExecOption... opt)
    throws UnknownCommandException, MalformedCommandException,
    java.io.IOException {
    return remoteExec ("remote.start.cmd", opt);
  }

  // ---------------------------------------------------------------------------

  /**
   * Starts the deployment of the remote task. If non-null, the notification
   * <tt>ackMe</tt> will be acknowledged if and when the command launching the
   * remote agent server on the remote site <em>starts</em> its execution.
   *
   * <p>Note this method MUST be executed during an agent reaction.
   *
   * @param ackMe notification to acknowledge at the end of startup.
   */
  public void startDeployment (final Not ackMe) {
    this.register ();
    new Deployer (this, ackMe).start ();
  }

  /**
   * Starts the deployment of the remote task.
   *
   * <p>Note this method MUST be executed during an agent reaction.
   */
  public void startDeployment () {
    this.startDeployment (null);
  }

  // ---

  /**
   * Driver thread for deployment.
   */
  private class Deployer extends Thread {

    private final Remote d;
    private final Not ackMe;

    // ---

    Deployer (final Remote d, final Not ackMe) {
      this.d = d;
      this.ackMe = ackMe;
      // Set max priority for IO thread.
      this.setPriority (Thread.MAX_PRIORITY);
    }

    // ---

    public void run () {
      try {
	boolean started = false;
	if (d.deploy ().waitFor () == 0) started = true;
	if (! started || d.redetar ().waitFor () != 0) {

	  slogmon.log (BasicLevel.ERROR, "Unable to deploy " +
		       d.getName () + "!");
	  // TODO: this.ackMe.maybeNok ()!

	  if (started) startUndeployment ();

	} else if (d.prepare ().waitFor () != 0) {

	  slogmon.log (BasicLevel.ERROR, "Unable to setup " +
		       d.getName () + "!");
	  // TODO: this.ackMe.maybeNok ()!

	  startUndeployment ();

	} else {
	  d.start (ExecOption.DETACHED);

	  if (slogmon.isLoggable (BasicLevel.INFO))
	    slogmon.log (BasicLevel.INFO, d.getName () + " started.");

	  if (this.ackMe != null)
	    this.ackMe.maybeAck ();
	}
      } catch (java.lang.Exception exc) {
	slogmon.log (BasicLevel.ERROR, "Caught exception when deploying "+
		     d.getName (), exc);
	// XXX What should be done here?
      }
    }
  }

  // ---------------------------------------------------------------------------

  /**
   * Starts the undeployment of the remote site. Essentially removes the remote
   * directory and cleans up local resources. If non-null, the notification
   * <tt>ackMe</tt> will acknowledged if and when the command removing the
   * remote site directory ends its execution.
   *
   * <p>Note this method MUST be executed during an agent reaction.
   *
   * @param ackMe notification to acknowledge at the end of undeployment.
   */
  public void startUndeployment (final Not ackMe) {
    this.unregister ();
    new Undeployer (this, ackMe).start ();
  }

  /**
   * Starts the undeployment of the remote site. Essentially removes the remote
   * directory and cleans up local resources.
   *
   * <p>Note this method MUST be executed during an agent reaction.
   */
  public void startUndeployment () {
    this.startUndeployment (null);
  }

  // ---

  /**
   * Driver thread for undeployment.
   */
  private static class Undeployer extends Thread {

    private final Remote d;
    private final Not ackMe;

    // ---

    Undeployer (final Remote d, final Not ackMe) {
      this.d = d;
      this.ackMe = ackMe;
      // Set max priority for IO thread.
      this.setPriority (Thread.MAX_PRIORITY);
    }

    // ---

    public void run () {
      try {

	if (slogmon.isLoggable (BasicLevel.DEBUG))
	  slogmon.log (BasicLevel.DEBUG, "Undeploying "+ d.getName () + ".");

	d.undeploy ().waitFor ();

	if (slogmon.isLoggable (BasicLevel.DEBUG))
	  slogmon.log (BasicLevel.DEBUG, d.getName () + " undeployed.");

	d.waitForPortAvailability ();

	if (this.ackMe != null)
	  this.ackMe.maybeAck ();

      } catch (java.lang.Exception e) {
	slogmon.log (BasicLevel.ERROR, d.getName () + " caught exception:", e);
      }
    }
  }

  // ---------------------------------------------------------------------------

  private static final class Ids extends Persistent<ISet> {
    // 0 is not available here since it identifies the center node.
    private Ids () { super (ISet.interval (1, Short.MAX_VALUE)); }
    protected void newRef () { availableIds.write (this); }
  }

  private static final Persistent.Ref<ISet> availableIds =
    new Persistent.Ref<ISet> (new Persistent.C<Ids> () {
  	public Ids b () { return new Ids (); }
      });

  // ---

  private static int allocId () throws TransientException {
    synchronized (availableIds) {
      ISet s = availableIds.get ();
      final int i = s.chooseSure ();
      s = s.rem (i);
      availableIds.set (s);
      return i;
    }
  }

  // ---

  private static void freeId (int i)
    throws TransientException {
    synchronized (availableIds) {
      availableIds.set (availableIds.get ().add (i));
    }
  }

  // ---------------------------------------------------------------------------

  /**
   * Port allocator, recording a mapping from remotes to a set of allocated
   * ports that can be freed at once.
   */
  public static class PortManager implements Serializable {

    private final Map<Integer, ISet.Ref> ports =
      new HashMap<Integer, ISet.Ref> ();

    /**
     * Allocates a new port for the given remote.
     *
     * @param r the remote for which a port is to be allocated
     * @return the allocated port
     *
     * @throws Node.OutOfPortException if no port is available for the given
     *                                  remote.
     * @throws TransientException when shared persistent data have not yet been
     *                            recovered.
     */
    public int alloc (final Remote r)
      throws Node.OutOfPortException, TransientException {
      final int port = r.node.allocPort ();
      final ISet.Ref s = this.ports.get (r.id);
      if (s != null) s.add (port);
      else           this.ports.put (r.id, ISet.Ref.singleton (port));
      return port;
    }

    /**
     * Releases all ports previously allocated for the given remote.
     *
     * @param r the remote for which the ports are to be released
     *
     * @throws TransientException when shared persistent data have not yet been
     *                            recovered.
     */
    public void releaseAll (final Remote r)
      throws TransientException {
      final ISet.Ref s;
      if ((s = this.ports.get (r.id)) == null)
	return;
      while (! s.isEmpty ()) {
	final int i = s.pollSure ();
	r.node.releasePort (i);
      }
      this.ports.remove (r.id);
    }
  }

  // ---------------------------------------------------------------------------

}
