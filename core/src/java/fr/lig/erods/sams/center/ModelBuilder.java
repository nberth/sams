/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.center;

import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;

import fr.dyade.aaa.agent.Agent;

/**
 * Common interface for model builders.
 */
public interface ModelBuilder {

  /** The type of results expected from the execution of a model builder. */
  public static class Result {

    /** Decision code agent */
    public final DecisionCode<?, ?, ?> decisionCode;

    /** Set of operating code agents */
    public final Set<Agent> operatingCode;

    /** Constructor with pre-built set of operating code agents */
    public Result (final DecisionCode<?, ?, ?> dc, final Set<Agent> opCode) {
      this.decisionCode = dc;
      this.operatingCode = opCode;
    }

    /** Constructor with variable number of arguments */
    public Result (final DecisionCode<?, ?, ?> dc, final Agent... ocAgents) {
      this (dc, new HashSet<Agent> (Arrays.asList (ocAgents)));
    }
  }

  /**
   * Instantiates the model.
   *
   * @param args the extra command line arguments (none transmitted for now)
   * @return the model built
   * @throws Exception in various error cases
   */
  public Result build (final String... args) throws Exception;
}
