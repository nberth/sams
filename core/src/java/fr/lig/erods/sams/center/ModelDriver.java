/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.center;

import java.io.IOException;
import java.lang.reflect.Constructor;

import fr.dyade.aaa.agent.Agent;
import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Channel;
import fr.dyade.aaa.common.Debug;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.Sams;
import fr.lig.erods.sams.common.util.TransientException;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Asbtact type of agents discussing with the decision code. It is primarily
 * intended for sending it impulse commands, and possibly receiving high-level
 * monitoring informations directly from it.
 */
public abstract class ModelDriver extends Agent {

  // ---

  /**
   * Decision code agent identifier.
   *
   * @see DecisionCode
   */
  protected final AgentId dc;

  // ---

  private static final Logger slogmon =
    Debug.getLogger (ModelDriver.class.getName ());

  // ---

  /** Only available constructor. */
  public ModelDriver (final String name, final AgentId dc) {
    super (name);
    this.dc = dc;
  }

  // ---

  /**
   * Returns a map of commands that should act up on this model driver.
   *
   * <p>Take care that the commands contained in the returned map MUST not
   * reference the instance of the model driver itself, but rather its
   * identifier. A typical usage for this method is thus to record this
   * identifier in a final variable that can be used in annonymous instances of
   * {@link Cmd}, as exemplified in the following code snippet:
   *
   * <blockquote><pre>
   * final AgentId dest = this.getId ();
   * cmdMap.put  ("command", new Cmd.Basic () { protected void run () {
   *	Channel.sendTo (dest, new Not ("command"));
   * }});
   * </pre></blockquote>
   */
  protected abstract Cmd.Map allCmds ();

  // ---

  private static void deployModelDriver (final AgentId dc)
    throws Exception, TransientException
  {
    final ModelDriver agent;
    final Class<? extends ModelDriver> agentClass;
    final Constructor<? extends ModelDriver> ctr;

    agentClass =
      Class.forName (Sams.env.get ("model-driver.class"))
      .asSubclass (ModelDriver.class);
    ctr = agentClass.getConstructor (AgentId.class);
    if (ctr == null) {
      slogmon.log (BasicLevel.ERROR, "Unable to instantiate model driver!");
      throw new Error ("Unable to instantiate model driver!");
    }

    agent = (ctr != null) ? ctr.newInstance (dc) : agentClass.newInstance ();
    agent.deploy ();

    Sams.save ("model-driver.cmds", agent.allCmds ());
    Sams.save ("model-driver.agent", agent.getId ());
  }

  // ---

  private static void deployModel (final ModelBuilder.Result model)
    throws IOException {
    final Not init = new Not ("init");
    model.decisionCode.deploy ();
    for (final Agent a: model.operatingCode) {
      a.deploy ();
      Channel.sendTo (a.getId (), init);
    }
  }

  // ---

  private static ModelBuilder buildModelBuilder () throws Exception {
    final Class<? extends ModelBuilder> mbClass;

    mbClass =
      Class.forName (Sams.env.get ("model-builder.class"))
      .asSubclass (ModelBuilder.class);
    if (mbClass == null) {
      slogmon.log (BasicLevel.ERROR, "Unable to instantiate model builder!");
      throw new Error ("Unable to instantiate model builder!");
    }

    return mbClass.newInstance ();
  }

  // ---

  /**
   * Returns a map of commands that should act up on the previously initialized
   * model driver.
   *
   * @throws TransientException when shared persistent data have not yet been
   *                            recovered.
   */
  static Cmd.Map getAllCmds () throws TransientException {
    return (Cmd.Map) Sams.load ("model-driver.cmds");
  }

  // ---

  /**
   * Initializes and deploy the model agents and its associated driver.  The
   * class names of each of them should be specified in the local environment
   * (see {@link fr.lig.erods.sams.common.Sams#env}) as values of keys
   * <b>model-builder.class</b> and <b>model-driver.class</b>.
   *
   */
  static void init () throws TransientException, Exception {
    final ModelBuilder mb = buildModelBuilder ();
    final ModelBuilder.Result model = mb.build ();
    deployModel (model);
    deployModelDriver (model.decisionCode.getId ());
  }

  // ---

}
