/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common.util;

import java.io.Serializable;
import java.util.Map;

import fr.dyade.aaa.common.Debug;
import fr.lig.erods.sams.common.Env;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrMatcher;
import org.apache.commons.lang3.text.StrTokenizer;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

public class CommandBuilder implements Serializable {

  protected final Env env;

  // ---

  protected static Logger logmon =
    Debug.getLogger (CommandBuilder.class.getName ());

  // ---

  public CommandBuilder (final String name) {
    this.env = new Env ();
    this.env.put ("name", name);
  }

  public CommandBuilder (final Env env) {
    this.env = env;
  }

  public CommandBuilder (final Map<String, String> env) {
    this.env = new Env (env);
  }

  // ---

  public class MalformedCommandException extends Exception {
    public MalformedCommandException (final String m) { super (m); }
    public String toString () {
      return "In `"+ getName () +"': "+ super.toString ();
    }
  }

  // ---

  public class UnknownCommandException extends Exception {
    public UnknownCommandException (final String m) { super (m); }
    public String toString () {
      return "In `"+ getName () +"': unknown command `"+
	super.toString () +"'.";
    }
  }

  // ---

  public final String [] splitCmd (final String fullCmd) {
    assert (fullCmd != null);
    return tokenizer.reset (fullCmd).getTokenArray ();
  }

  public final String [] appendArgs (String [] cmd, final String... args) {
    for (String arg: args)
      cmd = ArrayUtils.addAll (cmd, tokenizer.reset (arg).getTokenArray ());
    return cmd;
  }

  public final synchronized ProcessHandle exec (final String... cmd)
    throws java.io.IOException, MalformedCommandException {
    if (cmd == null || cmd.length < 1 || cmd [0] == "")
      throw new MalformedCommandException (StringUtils.join (cmd, " "));

    if (logmon.isLoggable (BasicLevel.DEBUG))
      logmon.log (BasicLevel.DEBUG, "`"+StringUtils.join (cmd, " ") +"'.");

    final Process p =
      new ProcessBuilder (cmd).redirectErrorStream (true).start ();
    final ProcessHandle h =
      new ProcessHandle (this.getName (), p);
    h.start ();
    return h;
  }

  // ---

  public synchronized ProcessHandle byKey (final String key)
    throws UnknownCommandException, MalformedCommandException,
    java.io.IOException {

    final String fullCmd = getProp (key);
    if (fullCmd == null || "".equals (fullCmd)) {
      final String msg = "Unable to find command `"+ key +"'!";
      logmon.log (BasicLevel.ERROR, msg);
      throw new UnknownCommandException (msg);
    }
    return exec (splitCmd (fullCmd));
  }

  // ---

  protected final String getProp (final String key) {
    return env.get (key, "");
  }

  // ---

  public String getName () { return getProp ("name"); }

  // ---

  // // Is this an error?
  // public class CommandBuilderError extends Error {
  //   public CommandBuilderError () { super (); }
  //   public CommandBuilderError (String e) { super (e); }
  //   public CommandBuilderError (Throwable cause) { super (cause); }
  //   public String toString () {
  //     return "Error in command builder "+ getName () +": "+ super.toString ();
  //   }
  // }

  // protected final ProcessHandle error () throws CommandBuilderError {
  //   throw new CommandBuilderError ();
  // }

  protected final ProcessHandle unknown (final String k)
    throws UnknownCommandException {
    throw new UnknownCommandException (k);
  }

  // ---

  private static final StrTokenizer tokenizer;
  static {
    StrTokenizer tkzr = new StrTokenizer ();
    tkzr = tkzr.setDelimiterMatcher (StrMatcher.charSetMatcher (" \t\n\r\f"));
    tkzr = tkzr.setQuoteMatcher (StrMatcher.quoteMatcher ());
    tokenizer = tkzr;
  }

}
