/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.LinkedList;

import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Channel;
import fr.dyade.aaa.agent.Notification;
import fr.dyade.aaa.common.Debug;
import fr.lig.erods.sams.common.Not;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

public class ProcessHandle extends Thread {

  private final String name;
  private final Process p;
  private boolean done = false;
  private static final Logger logmon =
    Debug.getLogger (ProcessHandle.class.getName ());

  // ---

  public ProcessHandle (String name, Process p) {
    this.name = name;
    this.p = p;
  }

  // ---

  public synchronized int waitFor () throws java.lang.InterruptedException {
    if (!this.done) p.waitFor ();
    return p.exitValue ();
  }

  public synchronized void kill () {
    p.destroy ();
  }

  // ---------------------------------------------------------------------------

  private static abstract class Hook /*, Serializable*/ {
    public abstract void run (final int status);
  }

  // ---

  private final Collection<Hook> endHook = new LinkedList<Hook> ();

  private synchronized void atEnd (final Hook hook) {
    if (done) hook.run (this.p.exitValue ());
    else this.endHook.add (hook);
  }

  // ---

  public ProcessHandle thenSend (final AgentId id, final Not n) {
    this.atEnd (new Hook () { public void run (final int status) {
      if (status == 0) Channel.sendTo (id, n);
    }});
    return this;
  }

  // ---

  public ProcessHandle thenSendAck (final Not ge) {
    this.atEnd (new Hook () { public void run (final int status) {
      if (status == 0) ge.maybeAck ();
    }});
    return this;
  }

  // ---

  public ProcessHandle orSend (final AgentId id, final Not n) {
    this.atEnd (new Hook () { public void run (final int status) {
      if (status != 0) Channel.sendTo (id, n);
    }});
    return this;
  }

  // ---

  // public ProcessHandle orSendNok (final Not ge) {
  //   this.atEnd (new Hook () { public void run (final int status) {
  //     if (status != 0) ge.maybeNok ();
  //   }});
  //   return this;
  // }

  // ---------------------------------------------------------------------------

  private void logOutput () throws Exception {
    final BufferedReader br =
      new BufferedReader (new InputStreamReader (p.getInputStream ()));
    final String prefix = "".equals (name) ? "" : "[" + name + "] ";
    String line;

    while ((line = br.readLine ()) != null) {
      System.err.print (prefix);
      System.err.println (line);
    }
  }

  public void run () {
    try {
      this.logOutput ();
      try {
	p.waitFor ();
	synchronized (this) {
	  int s = p.exitValue ();
	  this.done = true;
	  for (Hook h: this.endHook) h.run (s);
	  this.endHook.clear ();
	}
	if (p.exitValue () != 0 && logmon.isLoggable (BasicLevel.INFO))
	  logmon.log (BasicLevel.INFO, "Process " + name +
		      " terminated with status " + p.exitValue ());
      } catch (IllegalThreadStateException t) {
	p.destroy ();
      }
    } catch (Exception e) {
      logmon.log (BasicLevel.ERROR, "Error while executing " + name + ".", e);
    }
  }
}
