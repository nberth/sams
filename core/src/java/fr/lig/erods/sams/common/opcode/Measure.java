/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common.opcode;

import java.io.Serializable;
import java.util.HashMap;

import fr.dyade.aaa.agent.AgentServer;
import fr.dyade.aaa.agent.Notification;
import fr.dyade.aaa.common.Debug;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Named measure of a given type.
 *
 * @param <T> type of measured data
 */
public class Measure<T extends Serializable> extends Notification {

  // ---

  /** Identifier of the agent server where the measure has been instantiated */
  final short origin;
  final String name;
  final T value;

  // ---

  private static final Logger logmon =
    Debug.getLogger (Measure.class.getName ());

  // ---

  public Measure (final String name, final T value) {
    this.name = name;
    this.value = value;
    this.origin = AgentServer.getServerId ();
  }

  public static <T extends Serializable>
    Measure<T> of (final String name, final T value) {
    return new Measure<T> (name, value);
  }

  // ---

  public enum Status { HANDLED, NOT_HANDLED };

  // ---

  public static <T extends Serializable>
    Status maybeHandle (final Class<T> c, final Notification n,
			final Receiver<T> r)
    throws Exception {
    if (n instanceof Measure && c.isInstance (((Measure<?>) n).value)) {
      final Measure<?> m = (Measure<?>) n;
      if (r.refreshMeasure (m.name, c.cast (m.value)))
	return Status.HANDLED;
    }
    return Status.NOT_HANDLED;
  }

  // ---

  public interface Receiver<T> {
    public boolean refreshMeasure (String name, T value)
      throws Exception;
  }

  // ---

  public static abstract class Handler<T extends Serializable>
    implements Serializable {
    protected abstract void assign (T value);
  }

  public static class MapReceiver<T extends Serializable>
    extends HashMap<String, Handler<T>>
    implements Receiver<T>, Serializable {

    private final Class<T> clazz;

    public MapReceiver (final Class<T> clazz) {
      this.clazz = clazz;
    }

    public boolean refreshMeasure (final String name, final T value) {
      final Handler<T> h = super.get (name);
      if (h == null) {
	if (logmon.isLoggable (BasicLevel.WARN))
	  logmon.log (BasicLevel.WARN, "Unknown measure named `"+ name +"'.");
	return false;
      }
      h.assign (value);
      return true;
    }

    public Status maybeHandle (final Notification n) {
      if (n instanceof Measure && clazz.isInstance (((Measure<?>) n).value)) {
	final Measure<?> m = (Measure<?>) n;
	if (this.refreshMeasure (m.name, clazz.cast (m.value)))
	  return Status.HANDLED;
      }
      return Status.NOT_HANDLED;
    }
  }

  // ---
}
