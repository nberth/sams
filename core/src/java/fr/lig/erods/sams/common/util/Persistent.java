/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common.util;

import java.io.IOException;
import java.io.Serializable;

import fr.dyade.aaa.agent.Agent;
import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Channel;
import fr.dyade.aaa.agent.Notification;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Of course, mutations of this kind of data are not automatically undone in
 * case of failure of an agent reaction; its only purpose is to exploit
 * persistence properties of agents in order to define storages that survive
 * hardware failures. To do so, one can call the <code>sync</code> method from
 * an agent reaction during recovery (restart of the agent server), and await
 * for the response before performing any new mutation of the data.
 */
public class Persistent<T extends Serializable> extends Agent {

  // ---

  private transient boolean valid;
  protected transient T tdata;
  protected T pdata;

  // ---

  public Persistent (final T data) {
    super (true);		// fixed!
    this.pdata = data;
    this.tdata = data;
    this.valid = true;
    try {
      this.deploy ();
    } catch (IOException e) {
      logmon.log (BasicLevel.FATAL, "Error when trying to create agent for"+
		  " persistent data!", e);
      System.exit (1);
    }
  }

  // ---

  public interface Fuzzy extends Serializable {
    public void onRestore () throws Exception;
  }

  // ---

  /**
   * Do not catch me during an agent reaction, or throw me again.
   */
  public static class NotYetValidatedException extends TransientException {
    NotYetValidatedException () {
      super ("Persistent data not recovered yet; try again later…");
    }
  }

  // ---

  protected void check () throws NotYetValidatedException {
    if (! this.valid) throw new NotYetValidatedException ();
  }

  // ---

  protected void agentInitialize (final boolean firstTime)
    throws Exception {
    super.agentInitialize (firstTime);

    logmon.log (BasicLevel.DEBUG,
		"Persistent.agentInitialize ("+ firstTime+")");

    if (! firstTime) {
      this.valid = false;	// invalidate until sync.
      this.tdata = this.pdata;
      this.newRef ();
      this.onRestore ();
      this.schedValidation ();
    } else {
      this.valid = true;
    }
  }

  // ---

  public void onRestore () throws Exception {
    if (this.tdata instanceof Fuzzy)
      ((Fuzzy) this.tdata).onRestore ();
  }

  // ---

  private static class ValidateOp extends Notification {}

  private void schedValidation () {
    logmon.log (BasicLevel.DEBUG, "Scheduling validation for "+ this);

    // XXX hum… can this notification outrun some that are still in the outgoing
    // queue of agents that modified the persistent data before failure?  We
    // need to be sure the output queues are flushed in some way before sending
    // this…
    Channel.sendTo (this.getId (), new ValidateOp ());
  }

  // ---

  /**
   * Override this method to get the references to new instances during
   * recovery.
   */
  protected void newRef () { }

  // ---

  /**
   * Direct access to the data in memory, for "non-muting" opertation.
   */
  public T get () throws NotYetValidatedException {
    check ();
    return this.tdata;
  }

  // ---

  private static class SetOp<T> extends Notification {
    final T t;
    SetOp (final T t) { this.t = t; }
  }

  public void set (final T t) throws NotYetValidatedException {
    check ();
    Channel.sendTo (this.getId (), new SetOp<T> (t));
    // Assign transient version.
    this.tdata = t;
  }

  // ---

  private static class SyncOp extends Notification {
    final Notification done;
    SyncOp (final Notification done) { this.done = done; }
  }

  /**
   * Sends the given notification when all currently pending messages for of
   * this agent have been delivered. This means that when the <code>done</code>
   * notification is received by the agent calling this method (if none is
   * running, then <code>done</code> is not sent), then all pending operations
   * have been effectively performed. This is espcially useful when recovering
   * from a failure.
   */
  public void sync (final Notification done) {
    Channel.sendTo (this.getId (), new SyncOp (done));
  }

  // ---

  // In principle, we are the only creator of Set instances we receive here!
  @SuppressWarnings ({ "unchecked" })
  public void react (final AgentId from, final Notification n)
    throws Exception {
    if (n instanceof SetOp<?>) {
      final SetOp<T> s = (SetOp<T>) n;
      this.pdata = s.t;
      return;
    } else if (n instanceof SyncOp) {
      if (((SyncOp) n).done != null && ! from.isNullId ())
	Channel.sendTo (from, ((SyncOp) n).done);
      return;
    } else if (n instanceof ValidateOp) {
      logmon.log (BasicLevel.DEBUG, "Validating "+ this);
      this.valid = true;
      return;
    }
    else super.react (from, n);
  }

  // ---

  /**
   * Do not catch me during an agent reaction, or throw me again.
   */
  public static class NullRefException extends TransientException {
    NullRefException () {
      super ("Persistent data reference not recovered yet; try again later…");
    }
  }

  // ---

  /**
   * Helper for creating persistent data of type <code>U</code> with
   * constructors in <code>Ref&lt;T&gt;</code>.
   */
  public static abstract class C<T> extends TransientRef.C<T> {}

  // ---

  /**
   * A generic class <code>Ref&lt;T&gt;</code> encapsulates manipulations of
   * transient references to persistent data of type <code>T</code>.
   */
  public static class Ref<T extends Serializable>
    extends TransientRef<Persistent<T>> {

    /**
     * Note the parameter-less constructor of <code>U</code> must have public
     * access.
     */
    public <U extends Persistent<T>> Ref (final Class<U> c) {
      super (c);
    }

    public <U extends Persistent<T>> Ref (final C<U> c) {
      super (c);
    }

    public T get ()
      throws NotYetValidatedException, NullRefException {
      return this.read ().get ();
    }

    public void set (final T s)
      throws NotYetValidatedException, NullRefException {
      this.read ().set (s);
    }

    public void sync (final Notification n)
      throws NotYetValidatedException, NullRefException {
      this.read ().sync (n);
    }

    public void sync ()
      throws NotYetValidatedException, NullRefException {
      this.sync (null);
    }
  }

  // ---

  protected String getLogTopic () { return Persistent.class.getName (); }

  // ---

}
