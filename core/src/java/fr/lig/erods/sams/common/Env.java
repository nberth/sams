/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import fr.dyade.aaa.common.Debug;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.commons.lang3.text.StrTokenizer;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * The type of serializable environments.
 *
 * <p><a name="variable-expansion"/><b><em>Variable Expansion</em></b>
 *
 * <p>An object of this kind has the ability to perform variable expansion by
 * recursively substituting substrings of the form <tt>"${key}"</tt> in values
 * retrieved by using either {@link #get(String)}, {@link #get(String, String)},
 * with their corresponding value in the environment. Values resulting from
 * variable expansion are called <b>expanded values</b> in the following.
 *
 * <p>By default, variable expansion is made in two phases: the set of keys
 * recorded in the environment is used for a first pass of variable
 * substitutions, then a second pass is performed for substituting any remaining
 * system property. As a consequence, system properties can be overriden in the
 * environment when new values are inserted, and incidentally, values of system
 * properties themselves are <em>not</em> subject to variable expansion with the
 * keys defined in the the environment. Also, system properties themselves are
 * not part of this environment.
 *
 * <p>To circumvent the previous limitations, at the cost of the memory
 * footprint and serialization time of the environment, it is also possible to
 * call the method {@link #loadSysProps()}, that inserts all system properties
 * in the environment, and disables the second substitution pass.
 *
 * <p>Note that variable names are subject to variable expansion: whenever the
 * substring <tt>"${"</tt> is encountered in a value, the <em>matching</em>
 * <tt>"}"</tt> is searched, and the <em>expanded value</em> of the substring
 * in-between is used as key for substitution. For instance, if the keys
 * <tt>"foo"</tt> and <tt>"bar"</tt> are associated to <tt>"bar"</tt> and
 * <tt>"42"</tt> respectively, then the expanded value of <tt>"${${foo}}"</tt>
 * is <tt>"42"</tt>.
 *
 * <p><a name="fields-groups"/><b><em>Fields Groups</em></b>
 *
 * <p>A fields group named <em>g</em> is described in an environment by a comma
 * separated list of fields as value associated to the field group descriptor
 * (basically, a key) <em>g</em>+<tt>".fields"</tt>. As any other value, such
 * field group descriptors are subject to variable expansion. The whole value of
 * the field group is then the mapping of all keys
 * <em>g</em>+<tt>"."</tt><em>f</em> to their respective expanded values, for
 * any field <em>f</em> of the group.
 *
 */
public class Env implements Serializable {

  // It could be better to offer the possibility to build immutable environment
  // trees, yet this may alter the efficience of persistence-related and
  // serialization operations.

  // ---

  private HashMap<String, String> map;
  private transient StrSubstitutor subst;
  private boolean enableSysPass = true;

  // ---

  protected static Logger logmon = Debug.getLogger (Env.class.getName ());

  // ---

  /**
   * Builds an empty environemnt.
   */
  public Env () {
    this.map = new HashMap<String, String> ();
  }

  /**
   * Builds an environment from given predefined bindings.
   *
   * @param e predefined bindings
   */
  public Env (Map<String, String> e) {
    this.map = new HashMap<String, String> (e);
  }

  /**
   * Builds an environment by looking up and loading the given file or resource,
   * searching it in the given directory. An empty environment is built if the
   * file cannot be loaded.
   *
   * @see #load (String, String)
   */
  public Env (final String file, final String dir) {
    this ();
    this.load (file, dir);
  }

  /**
   * Builds an environment by looking up and loading the given file or
   * resource. An empty environment is built if the file cannot be loaded.
   *
   * @see #load (String)
   */
  public Env (final String pf) {
    this ();
    this.load (pf);
  }

  // ---

  private void setupSubst () {
    if (this.subst == null) {
      this.subst = new StrSubstitutor (this.map);
      this.subst.setEnableSubstitutionInVariables (true);
    }
  }

  // ---

  private String replace (String v) {
    v = subst.replace (v);
    v = this.enableSysPass ? StrSubstitutor.replaceSystemProperties (v) : v;
    return v;
  }

  // ---

  /**
   * Tests whether the given key belongs to this environment.
   */
  public boolean containsKey (final String k) {
    return this.map.containsKey (k);
  }

  // ---

  /**
   * Returns the expanded value associated to the given key if it is contained
   * in this environment, or a given fallback value otherwise.
   *
   * @param key the key to lookup
   * @param fallback the fallback value
   * @return the expanded value associated to <tt>key</tt>, or <tt>fallback</tt>
   *         otherwise
   */
  public String get (final String key, final String fallback) {
    setupSubst ();
    final String v = this.map.get (key);
    return (v == null) ? fallback : this.replace (v);
  }

  // ---

  /**
   * Returns the expanded value associated to the given key if it is contained
   * in this environment, or <tt>""</tt> otherwise.
   *
   * @param key the key to lookup
   * @return the expanded value associated to <tt>key</tt>, or <tt>""</tt>
   *         otherwise
   */
  public String get (final String key) { return this.get (key, ""); }

  // ---

  /**
   * Associates the given key to a new value.
   *
   * @return the value previously associated if any, or <tt>null</tt> otherwise.
   */
  public String put (final String key, final String value) {
    final String v = this.map.put (key, value);
    this.subst = null;
    return v;
  }

  // ---

  /**
   * Inserts all <em>bindings</em> form the given map.
   *
   * @return <tt>this</tt>
   */
  public Env putAll (final Map<String, String> m) {
    this.subst = null;
    this.map.putAll (m);
    return this;
  }

  // ---

  /**
   * Inserts all bindings from keys to <em>non-expanded values</em> from the
   * given environment.
   *
   * @return <tt>this</tt>
   */
  public Env putAll (final Env e) {
    return this.putAll (e.map);
  }

  // ---

  /**
   * Removes the binding of the given key from this environment.
   *
   * @param k the key to remove
   * @return the old value associated to <tt>k</tt> if any, <tt>null</tt>
   *         otherwise.
   */
  public String remove (final String k) {
    this.subst = null;
    return this.map.remove (k);
  }

  // ---

  /**
   * Clears the content of this environment.
   */
  public Env clear () {
    this.subst = null;
    this.map.clear ();
    return this;
  }

  // ---

  // /**
  //  * Returns a collection of the <em>expanded values</em> contained in this
  //  * environment. Unlike the {@link HashMap#values} variant, any change to the
  //  * returned collection is not reflected in the environment, and vice-versa.
  //  *
  //  * @return a collection of all expanded values.
  //  */
  // public Collection<String> values () {
  //   setupSubst ();
  //   final Collection<String> res = new HashSet<String> ();
  //   for (String s: super.values ()) res.add (this.replace (s));
  //   return res;
  // }

  // ---------------------------------------------------------------------------

  // Direct interactions with `Not'.

  /**
   * Looks up the value bound to a given key in the given instance of {@link
   * Not}, and associates the same key with the string representation of the
   * value in this environment. This method does nothing if the key does not
   * belong to the notification.
   *
   * @param key the key in the point of view of both this environment and the
   *        notification
   * @param not the notification to retrieve the value from
   * @return <tt>this</tt>
   */
  public Env maybeCopyFrom (final String key, final Not not) {
    return this.maybeCopyFrom (key, key, not, null);
  }

  /**
   * Looks up the value bound to a given key in the given instance of {@link
   * Not}, and associates another given key with the string representation of
   * the value in this environment. This method does nothing if the key does not
   * belong to the notification.
   *
   * @param key the key <em>in the point of view of the notification</em>
   * @param newKey the key <em>in the point of view of this environment</em>
   * @param not the notification to retrieve the value from
   * @return <tt>this</tt>
   */
  public Env maybeCopyFrom (final String key, final String newKey,
			    final Not not) {
    return this.maybeCopyFrom (key, newKey, not, null);
  }

  /**
   * Looks up the value bound to a given key in the given instance of {@link
   * Not}, and associates the same key with the string representation of the
   * value in this environment, or the string representation of a given fallback
   * value if the key does not belong to the bindings of the notification. This
   * method does nothing if the key does not belong to the notification and
   * fallback is <tt>null</tt>.
   *
   * @param key the key in the point of view of both this environment and the
   *        notification
   * @param not the notification to retrieve the value from
   * @param fallback the value to insert if <tt>key</tt> is not bound in
   *        <tt>not</tt>
   * @return <tt>this</tt>
   */
  public Env maybeCopyFrom (final String key, final Not not,
			    final Serializable fallback) {
    return this.maybeCopyFrom (key, key, not, fallback);
  }

  /**
   * Looks up the value bound to a given key in the given instance of {@link
   * Not}, and associates another given key with the string representation of
   * the value in this environment, or the string representation of a given
   * fallback value if the key does not belong to the bindings of the
   * notification. This method does nothing if the key does not belong to the
   * notification and fallback is <tt>null</tt>.
   *
   * @param key the key <em>in the point of view of the notification</em>
   * @param newKey the key <em>in the point of view of this environment</em>
   * @param not the notification to retrieve the value from
   * @param fallback the value to insert if <tt>key</tt> is not bound in
   *        <tt>not</tt>
   * @return <tt>this</tt>
   */
  public Env maybeCopyFrom (final String key, final String newKey,
			    final Not not, final Serializable fallback) {
    Serializable s = not.getProp (key);
    if (s == null) s = fallback;
    if (s == null) return this;
    this.put (newKey, s.toString ());
    return this;
  }

  /**
   * Looks up the value bound to the given key in the given instance of {@link
   * Not}, and associates the key with the string representation of the value in
   * this environment.
   *
   * @param key the key to lookup
   * @param not the notification to retrieve the value from
   * @return <tt>this</tt>
   * @throws Error in case <tt>key</tt> does not belong to the bindings of the
   *         notification
   */
  public Env surelyCopyFrom (final String key, final Not not) {
    Serializable s = not.getProp (key);
    if (s == null) throw new Error ("Missing key `"+ key +"' in `"+ not +"' !");
    this.put (key, s.toString ());
    return this;
  }

  // ---

  /**
   * Binds a key and its expanded value in an instance of {@link Not}. The
   * notification is left unchaned if the key does not belong to this
   * environment.
   *
   * @param key the key in the point of view of both this environment and the
   *        notification
   * @param not the notification to insert the binding into
   */
  public void maybeCopyTo (final String key, final Not not) {
    this.maybeCopyTo (key, key, not, null);
  }

  /**
   * Binds a key and its expanded value in an instance of {@link Not}. The
   * notification is left unchaned if the key does not belong to this
   * environment.
   *
   * @param key the key <em>in the point of view of this environment</em>
   * @param newKey the key <em>in the point of view of the notification</em>
   * @param not the notification to insert the binding into
   */
  public void maybeCopyTo (final String key, final String newKey,
			   final Not not) {
    this.maybeCopyTo (key, newKey, not, null);
  }

  /**
   * Binds a key and its expanded value in an instance of {@link Not}. The
   * notification is left unchaned if the key does not belong to this
   * environment and the given fallback value is <tt>null</tt>.
   *
   * @param key the key in the point of view of both this environment and the
   *        notification
   * @param not the notification to insert the binding into
   * @param fallback the value to insert in case <tt>key</tt> is not bound in
   *        this environment
   */
  public void maybeCopyTo (final String key, final Not not,
			   final String fallback) {
    this.maybeCopyTo (key, key, not, fallback);
  }

  /**
   * Binds a key and its expanded value in an instance of {@link Not}. The
   * notification is left unchaned if the key does not belong to this
   * environment and the given fallback value is <tt>null</tt>.
   *
   * @param key the key <em>in the point of view of this environment</em>
   * @param newKey the key <em>in the point of view of the notification</em>
   * @param not the notification to insert the binding into
   * @param fallback the value to insert in case <tt>key</tt> is not bound in
   *        this environment
   */
  public void maybeCopyTo (final String key, final String newKey,
			   final Not not, final String fallback) {
    String s = this.get (key, null);
    if (s == null) s = fallback;
    if (s == null) return;
    not.setProp (newKey, s);
  }

  // ---------------------------------------------------------------------------

  // Fields groups and interactions with `Not'

  /**
   * Inserts in this environment and from an instance of {@link Not}, a group of
   * fields given by its name.
   *
   * <p>The set of imported fields is determined by using the value associated
   * to <tt>group</tt>+<tt>".fields"</tt> (see <a href="#fields-groups">Fields
   * Groups</a>). Missing keys in the notification are not removed from this
   * environment, and thus keep their current value, if any.
   *
   * @param group the name of the fields group to import
   * @param not the notification to import the fields group from
   * @return <tt>this</tt>
   */
  public Env importFieldsGroup (final String group, final Not not) {
    return this.importFieldsGroup (group, group, not);
  }

  /**
   * Inserts in this environment and from an instance of {@link Not}, a group of
   * fields given by its name, while renaming it.
   *
   * <p>The set of imported fields is determined by using the value associated
   * to <tt>localGroup</tt>+<tt>".fields"</tt> (see <a
   * href="#fields-groups">Fields Groups</a>). For each one of these fields
   * <em>f</em>, the corresponding value is looked up by getting the value
   * associated to the key <tt>importedGroup</tt>+<tt>"."</tt>+<em>f</em> in the
   * notification, and associated to the key
   * <tt>localGroup</tt>+<tt>"."</tt>+<em>f</em> in this environment. Missing
   * keys in the notification are not removed from this environment, and thus
   * keep their current value, if any.
   *
   * @param localGroup the name of the fields group from the point of view of
   *                   this environment
   * @param importedGroup the name of the fields group from the point of view of
   *                      the notification
   * @param not the notification to import the fields group from
   * @return <tt>this</tt>
   */
  public Env importFieldsGroup (final String localGroup,
				final String importedGroup,
				final Not not) {
    final StrTokenizer tkzr =
      StrTokenizer.getCSVInstance (this.get (localGroup +".fields", ""));
    while (tkzr.hasNext ()) {
      final String k = tkzr.next ();
      this.maybeCopyFrom (importedGroup +'.'+ k, localGroup +'.'+ k, not);
    }
    return this;
  }

  // ---

  /**
   * Copy from this environment and to the given instance of {@link Not}, a
   * group of fields given by its name.
   *
   * <p>The set of exported fields is determined by using the value associated
   * to <tt>group</tt>+<tt>".fields"</tt> (see <a href="#fields-groups">Fields
   * Groups</a>). Missing keys in this environment are not removed from the
   * notification, and thus keep their current value, if any.
   *
   * @param group the name of the fields group to export
   * @param not the notification to export the fields group to
   */
  public void exportFieldsGroup (final String group, final Not not) {
    this.exportFieldsGroup (group, group, not);
  }

  /**
   * Copy from this environment and to the given instance of {@link Not}, a
   * group of fields given by its name, while renaming it.
   *
   * <p>The set of exported fields is determined by using the value associated
   * to <tt>localGroup</tt>+<tt>".fields"</tt> (see <a
   * href="#fields-groups">Fields Groups</a>). For each one of these fields
   * <em>f</em>, the corresponding <em>expanded</em> value is looked up by
   * getting the value associated to the key
   * <tt>localGroup</tt>+<tt>"."</tt>+<em>f</em> in this environment, and
   * associated to the key <tt>exportedGroup</tt>+<tt>"."</tt>+<em>f</em> into
   * the notification. Missing keys in this environment are not removed from the
   * notification, and thus keep their current value, if any.
   *
   * @param localGroup the name of the fields group from the point of view of
   *                   this environment
   * @param exportedGroup the name of the fields group from the point of view of
   *                      the notification
   * @param not the notification to export the fields group to
   */
  public void exportFieldsGroup (final String localGroup,
				 final String exportedGroup,
				 final Not not) {
    final StrTokenizer tkzr =
      StrTokenizer.getCSVInstance (this.get (localGroup +".fields", ""));
    while (tkzr.hasNext ()) {
      final String k = tkzr.next ();
      this.maybeCopyTo (localGroup +'.'+ k, exportedGroup +'.'+ k, not);
    }
  }

  // ---------------------------------------------------------------------------

  /**
   * Inserts all system properties in this environment, and disables the second
   * substitution pass.
   */
  @SuppressWarnings ({ "unchecked", "rawtypes" })
  public Env loadSysProps () {
    this.enableSysPass = false;
    final Map<String, String> sys = new TreeMap (System.getProperties ());
    this.putAll (sys);
    return this;
  }

  // ---

  // public Env loadSysProps (final String... props) {
  //   for (String p: props) {
  //     String v = System.getProperty (p);
  //     if (v != null) this.put (p, v);
  //   }
  // }

  // ---

  /**
   * Loads the given input stream as a properties file.
   *
   * @param is the input stream to load properties from
   * @return <tt>this</tt>
   * @throws IOException in case of input/output error
   */
  @SuppressWarnings ({ "unchecked", "rawtypes" })
  public Env load (final InputStream is) throws IOException {
    final Properties props = new Properties ();
    props.load (is);
    final Map<String, String> m = new TreeMap (props);
    this.putAll (m);
    return this;
  }

  // ---

  /**
   * Try to load properties from given file or resource. Given <em>f</em> the
   * argument, the lookup order is the following:
   *
   * <ul>
   *
   * <li><em>f</em> as a file, in the current directory if <em>f</em> is a
   * relative filename;
   *
   * <li><tt>/</tt><em>f</em> as a resource in the classpath if <em>f</em> is
   * not already an absolute path name; otherwise, <em>f</em> is used directly
   * as resource name.
   *
   * </ul>
   *
   * @param fname the name of the file or resource to load
   * @return <tt>true</tt> iff the load was successful
   */
  public boolean tryLoad (String fname) {

    File f;
    InputStream is;

    // Lookup on the file system.
    if (logmon.isLoggable (BasicLevel.DEBUG))
      logmon.log (BasicLevel.DEBUG, "Looking up `"+ fname +"' as file…");
    f = new File (fname);
    if (f.exists () && f.canRead ()) {
      logmon.log (BasicLevel.INFO, "Loading file `"+ fname +"'.");
      try {
	this.load (new FileInputStream (fname));
	return true;
      } catch (IOException e) {}
    }

    if (! f.isAbsolute ())
      // Prefix the resource name with a '/' so that it is considered as
      // resource from the root of the classpath.
      fname = '/'+ fname;

    if (logmon.isLoggable (BasicLevel.DEBUG))
      logmon.log (BasicLevel.DEBUG, "Looking up `"+ fname +"' as resource…");
    if ((is = Env.class.getResourceAsStream (fname)) != null) {
      logmon.log (BasicLevel.INFO, "Loading resource `"+ fname +"'.");
      try {
    	this.load (is);
    	return true;
      } catch (IOException e) {}
    }

    return false;
  }

  // ---

  /**
   * Loads properties from a file or resource identified by the given name.
   */
  public Env load (final String file) {
    return this.load (file, null, null, null);
  }

  /**
   * Loads properties from a file or resource identified by the given name, and
   * look it up in the given directory.
   *
   * <p>Set <tt>dir</tt> to <tt>""</tt> to forbid non-absolute lookup of
   * <tt>file</tt>.
   */
  public Env load (final String file, final String dir) {
    return this.load (file, dir, null, null);
  }

  // public Env loadFromProps (final String fileprop, final String dirprop) {
  //   return this.load (null, null, fileprop, dirprop);
  // }

  /**
   * Loads properties by successively trying to lookup files or resources in the
   * classpath. Given the arguments <em>f</em>, <em>d</em>, <em>fprop</em> and
   * <em>dprop</em>, the successive tries are the following (where
   * <em>sys(prop)</em> denotes the value of the system property <em>prop</em>,
   * or an undefined value if <em>prop</em> is <tt>null</tt> or does not belong
   * to the set of system properties):
   *
   * <ul>
   *
   * <li><em>sys(dprop)</em><tt>/</tt><em>sys(fprop)</em>, if
   * <em>sys(dprop)</em> and <em>sys(fprop)</em> are defined, and
   * <em>sys(fprop)</em> represents a relative path;
   *
   * <li><em>d</em><tt>/</tt><em>sys(fprop)</em> if <em>sys(dprop)</em> is
   * undefined, <em>d</em> is non-null, and if <em>sys(fprop)</em> is defined
   * and represents a relative path;
   *
   * <li><em>sys(fprop)</em> if defined;
   *
   * <li><em>sys(dprop)</em><tt>/</tt><em>f</em>, if <em>sys(dprop)</em> is
   * defined and <em>f</em> is non-null and represents a relative path;
   *
   * <li><em>d</em><tt>/</tt><em>f</em>, if <em>sys(dprop)</em> is undefined,
   * and if <em>d</em> is non-null and <em>f</em> is non-null and represents a
   * relative path;
   *
   * <li><em>f</em> if it is non-null, <em>sys(dprop)</em> is undefined, and
   * <em>d</em> equals <tt>null</tt>.
   *
   * </ul>
   *
   * <p>Any one of the arguments can be <tt>null</tt>; the method has no
   * noticeable effect if none is non-null.
   *
   * <p>Using static initializers and {@link #load(String, String)} is much more
   * handy for overriding default lookup paths…
   *
   * @param f file or resource basename (yet, it can contain slashes '/')
   * @param d file or resource directory name
   * @param fprop system property name, whose value is fetched to determine
   *              file or resource basenames
   * @param dprop system property name, whose value is fetched to determine file
   *              or resource directory name
   * @return <tt>this</tt>
   */
  public Env load (final String f, final String d,
		   final String fprop, final String dprop) {

    final String
      file = fprop != null ? System.getProperty (fprop)    : null,
      dir  = dprop != null ? System.getProperty (dprop, d) : d;

    // try (specified directory or value(`dprop')) / value(`fprop')
    if (dir != null && file != null && !"".equals (file) &&
	file.charAt (0) != '/' && this.tryLoad (dir +'/'+ file))
      return this;

    // try current directory / value(`fprop')
    if (file != null && this.tryLoad (file))
      return this;

    // try (specified directory or value(`dprop')) / f
    if (dir != null && f != null && !"".equals (f) &&
	f.charAt (0) != '/' && this.tryLoad (dir +'/'+ f))
      return this;

    // try current directory / f
    if (dir == null && f != null && this.tryLoad (f))
      return this;

    if (logmon.isLoggable (BasicLevel.DEBUG)) {
      if (dir != null)
	logmon.log (BasicLevel.DEBUG, "Unable to load `"+ dir +'/'+ f +"'.");
      else
	logmon.log (BasicLevel.DEBUG, "Unable to load `"+ f +"'.");
    }

    return this;
  }

  // ---------------------------------------------------------------------------

  // Basic, inefficient algorithms for handling prefixed keys…

  /**
   * Returns a new environment associating all <em>non-expanded values</em> of
   * this environment whose key strings does not start by any of the given
   * prefixes suffixed by a dot (</tt>"."</tt>).
   */
  public Env unmatchingPrefixes (final Iterable<?>... prefixes) {
    return this.unmatchingPrefixes (Arrays.asList (prefixes));
  }

  /**
   * Returns a new environment associating all <em>non-expanded values</em> of
   * this environment whose key strings does not start by any of the given
   * prefixes suffixed by a dot (</tt>"."</tt>).
   */
  public Env unmatchingPrefixes (final Iterable<String> prefixes) {
    final Env res = new Env ();

    for (final Map.Entry<String, String> e: this.map.entrySet ()) {
      final String k = e.getKey (), v = e.getValue ();
      boolean found = false;
      for (final String p: prefixes) {
	final int pl = p.length ();
	if (k.length () > pl && k.charAt (pl) == '.' && k.startsWith (p)) {
	  found = true;
	  break;
	}
      }
      if (! found)
      	res.put (k, v);
    }

    return res;
  }

  // ---

  /**
   * Selects in this environment all bindings for which one of the given strings
   * (suffixed by a dot) is a prefix of the key, and builds new environments by
   * selecting all sets of matching bindings in turn, and merging it with the
   * given initial one.
   *
   * <p>The contents of each environment <em>e(p)</em> associated to prefix
   * <em>p</em> in the resulting map is first initialized with the raw bindings
   * in <tt>init</tt> (that is, their values are not expanded). Then, for each
   * binding <em>k → v</em> in <tt>this</tt> for which the key <em>k</em> is of
   * the form <em>p</em>+<tt>"."</tt>+<em>k'</em>, the new key <em>k'</em> is
   * associated to the <em>non-expanded value v</em> in <em>e(p)</em>.
   *
   * <p>Note each given prefix can be a prefix of any of the other given
   * prefixes, in which case corresponding values in this environment will
   * appear in several environments in the result.
   *
   * @param init the environment from which all the resulting ones are
   *             initialized
   * @param prefixes the set of prefixes to select in this environment
   * @return a mapping from prefixes to corresponding environments
   */
  public Map<String, Env>
    selectByPrefixesStripUnmatch (final Env init,
				  final Iterable<String> prefixes) {

    final Map<String, Env> res = new TreeMap<String, Env> ();

    for (final String p: prefixes)
      res.put (p, new Env (init.map));

    for (final Map.Entry<String, String> e: this.map.entrySet ()) {
      final String k = e.getKey (), v = e.getValue ();
      for (final String p: prefixes) {
	final int pl = p.length ();
	if (k.length () > pl && k.charAt (pl) == '.' && k.startsWith (p)) {
	  res.get (p).put (k.substring (pl + 1), v);
	  // break;
	}
      }
    }

    return res;
  }

  /**
   * Selects in this environment all bindings for which one of the given strings
   * (suffixed by a dot) is a prefix of the key, and builds new environments by
   * selecting all sets of matching bindings in turn, and merging it with the
   * given initial one.
   *
   * <p>See {@link #selectByPrefixesStripUnmatch(Env,Iterable)} for a complete
   * description.
   */
  public Map<String, Env> selectByPrefixesStripUnmatch
    (final Env init, final String... prefixes) {
    return this.selectByPrefixesStripUnmatch (init, Arrays.asList (prefixes));
  }

  // ---

  /**
   * Selects in this environment all bindings for which one of the given strings
   * (suffixed by a dot) is a prefix of the key, and builds new environments by
   * selecting all sets of matching bindings in turn.
   *
   * Selects in this environment all bindings whose keys start by the given
   * prefixes (suffixed by a dot), and builds new environments with all matching
   * bindings.
   *
   * <p>See {@link #selectByPrefixesStripUnmatch(Env,Iterable)} for a complete
   * description, considering the initial environment as empty.
   */
  public Map<String, Env> selectByPrefixesStripUnmatch (final String... prefs) {
    return this.selectByPrefixesStripUnmatch (new Env (),
					      Arrays.asList (prefs));
  }

  // ---

  /**
   * Selects in this environment all bindings for which one of the given strings
   * (suffixed by a dot) is a prefix of the key, and builds new environments by
   * selecting all sets of matching bindings in turn, and merging it with all
   * bindings for which none of the given strings is a prefix.
   *
   * <p>The contents of each environment <em>e(p)</em> associated to prefix
   * <em>p</em> in the resulting map is first initialized with the raw
   * unmatching bindings in <tt>init</tt> (that is, their values are not
   * expanded). Then, for each binding <em>k → v</em> in <tt>this</tt> for which
   * the key <em>k</em> is of the form <em>p</em>+<tt>"."</tt>+<em>k'</em>, the
   * new key <em>k'</em> is associated to the <em>non-expanded value v</em> in
   * <em>e(p)</em>.
   *
   * <p>Remark that if both <em>k → v</em> and <em>p</em>+<tt>"."</tt>+<em>k →
   * v'</em> belong to this environment, and if, unlike <em>k</em>, <em>p</em>
   * belongs to the given set of prefixes, then <em>e(p)</em> will contain the
   * binding <em>k → v'</em> instead of <em>k → v</em>.
   *
   * <p>Note each given prefix can be a prefix of any of the other given
   * prefixes, in which case corresponding values in this environment will
   * appear in several environments in the result.
   *
   * @param prefixes the set of prefixes to select in this environment
   * @return a mapping from prefixes to corresponding environments
   */
  public Map<String, Env> selectByPrefixes (final Iterable<String> prefixes) {
    final Env common = this.unmatchingPrefixes (prefixes);
    return this.selectByPrefixesStripUnmatch (common, prefixes);
  }

  // ---

  /**
   * Selects in this environment all bindings for which one of the given strings
   * (suffixed by a dot) is a prefix of the key, and builds new environments by
   * selecting all sets of matching bindings in turn, and merging it with all
   * bindings for which none of the given strings is a prefix.
   *
   * <p>See {@link #selectByPrefixes(Iterable)} for a complete description.
   */
  public Map<String, Env> selectByPrefixes (final String... prefixes) {
    return this.selectByPrefixes (Arrays.asList (prefixes));
  }

  // ---------------------------------------------------------------------------

}
