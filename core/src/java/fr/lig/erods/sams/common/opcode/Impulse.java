/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common.opcode;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import fr.dyade.aaa.common.Debug;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

import fr.dyade.aaa.agent.Notification;

/**
 * An impulse notification basically only carries its name, even though it is
 * immaginable to convey other kinds of information. (Hence, it's not a final
 * class).
 */
public class Impulse extends Notification {

  // ---

  public final String name;

  // ---

  private static final Logger logmon =
    Debug.getLogger (Impulse.class.getName ());

  // ---

  public Impulse (final String name) {
    this.name = name;
  }

  // ---

  /**
   * Interface to implement to be able to use {@link
   * #maybeHandle(Notification,Receiver)}.
   */
  public interface Receiver {
    public boolean newImpulse (Impulse i);
  }

  // ---

  /**
   * Return type of {@link #maybeHandle(Notification,Receiver)}, indicating
   * whether a notification has been handled or not.
   */
  public enum Status { HANDLED, NOT_HANDLED };

  // ---

  /**
   * Executes the {@link Receiver#newImpulse(Impulse)} method of the given
   * receiver if the given notification is an instance of {@link Impulse}.
   *
   * @param n the notification to handle
   * @param r the impulse receiver
   * @return {@link Status#NOT_HANDLED} if <tt>n</tt> is not an impulse, or if
   *         {@link Receiver#newImpulse(Impulse)} returned <tt>true</tt>;
   *         returns {@link Status#HANDLED} otherwise.
   */
  public static Status maybeHandle (final Notification n, final Receiver r) {
    if (n instanceof Impulse && r.newImpulse ((Impulse) n))
      return Status.HANDLED;
    else
      return Status.NOT_HANDLED;
  }

  // ---

  public StringBuffer toString (final StringBuffer o) {
    return o.append (name).append ('!');
  }

  // ---

  /**
   * Proposed handling mechanism for impulses. See {@link
   * fr.lig.erods.sams.center.DecisionCode#ih} for an example of usage.
   */
  public static abstract class Handler implements Serializable {
    public final void raise () { this.assign (true);  }
    public final void clear () { this.assign (false); }

    /**
     * Method to override. Its suggested usage is the following: build a mapping
     * from impulse names to instances of annonymous inner-classes defining this
     * method as follows. */
    protected abstract void assign (boolean value);
  }

  // ---

  public static abstract class MapReceiver
    extends HashMap<String, Impulse.Handler>
    implements Receiver {

    private final List<String> pending = new LinkedList<String> ();

    public final boolean newImpulse (final Impulse i) {
      // In principle, there must be at most one pending impulse. due to AAA
      // message delivery properties…
      assert (pending.isEmpty ());
      pending.add (i.name);
      // Hum…:
      this.onNewImpulse ();
      // force_step ();
      return true;
    }

    protected abstract void onNewImpulse ();

    public void raisePendings () {
      // Raise all impulse inputs.
      for (final String i: this.pending) {
	final Handler h = this.get (i);
	if (h == null) {
	  if (logmon.isLoggable (BasicLevel.WARN))
	    logmon.log (BasicLevel.WARN, "Unknown impulse named `"+ i +"'.");
	} else {
	  h.raise ();
	}
      }
    }

    public void clearPendings () {
      // Restore initial state of impulse inputs.
      for (final String i: this.pending) {
	final Handler h = this.get (i);
	if (h != null) h.clear ();
      }

      this.pending.clear ();
    }

    public Status maybeHandle (final Notification n) {
      if (n instanceof Impulse && this.newImpulse ((Impulse) n))
	return Status.HANDLED;
      else
	return Status.NOT_HANDLED;
    }

  }

  // ---
}
