/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common.util;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.PriorityQueue;
import java.util.TimerTask;

import fr.dyade.aaa.agent.AgentServer;

/**
 * This class serves as a namespace for cancelable timer code. Note scheduled
 * timer tasks do not survive failures.
 */
public final class CancelableTimer {

  // ---

  public static class Task {
    private final Date expiration;
    private final Runnable runMe;
    private final TimerTask task;

    // ---

    private static Date expirDate (final long ms_delay) {
      final Calendar cal = Calendar.getInstance ();
      final int sec = (int) (ms_delay / (long) 1000);
      final int ms = (int) (ms_delay % (long) 1000);
      cal.add (Calendar.MILLISECOND, ms);
      cal.add (Calendar.SECOND, sec);
      return cal.getTime ();
    }

    // ---

    public Task (final long ms_delay, final Runnable r) {
      this.expiration = expirDate (ms_delay);
      this.runMe = r;
      this.task = new TimerTask () {
	  public void run () {
	    final Task t = heap.poll ();
	    assert (t.runMe == runMe);
	    runMe.run ();
	  }
	};
      heap.add (this);
      reconfigureTimer ();
    }

    // ---

    public void cancel () {
      heap.remove (this);
      reconfigureTimer ();
    }

    // ---

  }

  // ---

  private static final PriorityQueue<Task> heap =
    new PriorityQueue<Task> (4, new Comparator<Task> () {
	public int compare (Task i, Task j) {
	  // Compare according to expiration dates only, and determine i < j iff
	  // date i precedes j.
	  return i.expiration.compareTo (j.expiration);
	}
      });

  // ---

  private static Task scheduled = null;

  // ---

  private static synchronized void reconfigureTimer () {
    final Task next = heap.peek ();
    if (next != null && next == scheduled) return;

    if (scheduled != null) {
      scheduled.task.cancel ();
      AgentServer.getTimer ().purge ();	// eager memory management.
    }

    try {
      if (next != null)
	AgentServer.getTimer ().schedule (next.task, next.expiration);

      scheduled = next;
    } catch (IllegalStateException e) {
      heap.poll ();		// top (== scheduled) seems to have already
				// expired, so, we can remove the head of the
				// queue…
      scheduled = null;
      reconfigureTimer ();
    }
  }

  // ---
}
