/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common.util;

import java.io.Serializable;

/**
 * Abstract identifier type (encapsulating an integer).
 */
public abstract class AbstractId
  implements Comparable<AbstractId>, Serializable {

  // private final short srv = AgentServer.getServerId ();
  private final short val;

  // ---

  protected AbstractId ()
    throws TransientException {
    this.val = alloc ();
  }

  // XXX use with care!
  protected AbstractId (short i) {
    this.val = i;
  }

  public final void release ()
    throws TransientException {
    this.free (this.val);
  }

  // ---

  protected abstract short alloc () throws TransientException;
  protected abstract void free (short i) throws TransientException;

  // ---

  public final int compareTo (final AbstractId other) {
    // assert (this.getClass () == other.getClass ());
    return
      // (this.srv < other.srv) ? -1 : (this.srv > other.srv) ? +1 :
      (this.val < other.val) ? -1 : (this.val > other.val) ? +1 : 0;
  }

  // ---

  public String toString () { return Integer.toString (this.val); }
  public StringBuffer toString (final StringBuffer output) {
    return output// .append (this.srv).append ('-')
      .append (this.val);
  }

  // ---

  public boolean equals (final Object o) {
    return (this.getClass () == o.getClass ()) &&
      // this.srv == ((AbstractId) o).srv &&
      this.val == ((AbstractId) o).val;
  }

  // ---

  public int hashCode () { return (int) // this.srv << 16 +
      this.val; }

  // ---

  public final int intValue () { return this.val; }

  // ---------------------------------------------------------------------------

  public abstract static class Generic extends AbstractId {

    public Generic () throws TransientException { super (); }

    // ---

    private static final ISet defaultIds = ISet.interval (0, Short.MAX_VALUE);

    // ---

    private static final class AllocMap extends PersistentHashMap<Class<?>, ISet> {
      protected void newRef () { availableIds.write (this); }
    }

    private static final PersistentHashMap.Ref<Class<?>, ISet> availableIds =
      new PersistentHashMap.Ref<Class<?>, ISet>
      (new PersistentHashMap.C<AllocMap> () {
	public AllocMap b () { return new AllocMap (); }
      });;

    // ---

    protected synchronized final short alloc ()
      throws TransientException {

      ISet s = availableIds.get (this.getClass ());
      if (s == null) s = defaultIds;
      short i = (short) s.chooseSure ();
      availableIds.put (this.getClass (), s.rem (i));
      return i;
    }

    // ---

    protected synchronized final void free (short i)
      throws TransientException {

      ISet s = availableIds.get (this.getClass ());
      assert (s != null && ! s.contains (i));
      availableIds.put (this.getClass (), s.add (i));
    }

    // ---
  }

  // ---------------------------------------------------------------------------

}
