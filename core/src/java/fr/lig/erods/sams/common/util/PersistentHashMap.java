/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common.util;

import java.io.Serializable;
import java.util.HashMap;

import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Channel;
import fr.dyade.aaa.agent.Notification;

public class PersistentHashMap<K extends Serializable, V extends Serializable>
  extends Persistent<HashMap<K, V>> {

  // ---

  // private final Class<K> keyClass;
  // private final Class<V> valClass;

  // public PersistentHashMap (final Class<K> keyClass,
  // 			    final Class<V> valClass) {
  //   super (new HashMap<K, V> ());
  //   this.keyClass = keyClass;
  //   this.valClass = valClass;
  // }

  // ---

  public PersistentHashMap () {
    super (new HashMap<K, V> ());
  }

  // ---

  public void onRestore () throws Exception {
    super.onRestore ();

    final HashMap<K, V> data = this.pdata;

    for (final K k: data.keySet ())
      if (k instanceof Fuzzy)
	((Fuzzy) k).onRestore ();

    for (final V v: data.values ())
      if (v instanceof Fuzzy)
	((Fuzzy) v).onRestore ();
  }

  // ---

  /**
   * Direct access to the store in memory, for "non-muting" opertation.
   */
  public V get (final K k)
    throws Persistent.NotYetValidatedException {
    return this.get ().get (k);
  }

  public boolean containsKey (final K k)
    throws Persistent.NotYetValidatedException {
    return this.get ().containsKey (k);
  }

  // ---

  // private static class PutOp<K, V> extends Notification {
  //   final K k;
  //   final V v;
  //   PutOp (final K k, final V v) {
  //     this.k = k;
  //     this.v = v;
  //   }
  // }

  public void put (final K k, final V v)
    throws Persistent.NotYetValidatedException {
    check ();
    assert (this.get () != null);
    // Channel.sendTo (this.getId (), new PutOp<K, V> (k, v));
    // Modify transient map:
    this.tdata.put (k, v);
    this.set (this.tdata);
    // Channel.sendTo (this.getId (), new SetOp<K, V> (k, v));
  }

  // ---

  // private static class RemOp<K> extends Notification {
  //   final K k;
  //   RemOp (final K k) { this.k = k; }
  // }

  public V remove (final K k)
    throws Persistent.NotYetValidatedException {
    this.check ();
    assert (this.get () != null);
    // Channel.sendTo (this.getId (), new RemOp<K> (k));
    // Modify transient map:
    final V v = this.tdata.remove (k);
    this.set (this.tdata);
    return v;
  }

  // ---

  // // In principle, we are the only creator of Put and Rem instances we receive
  // // here!
  // @SuppressWarnings ({ "unchecked" })
  //   public void react (final AgentId from, final Notification n)
  //   throws Exception {
  //   if (n instanceof PutOp<?, ?>) {
  //     final PutOp<K, V> p = (PutOp<K, V>) n;
  //     // final K key = this.keyClass.cast (p.k);
  //     // final V val = this.valClass.cast (p.v);
  //     // this.get ().put (key, val);
  //     this.pdata.put (p.k, p.v);
  //     return;
  //   } else if (n instanceof RemOp<?>) {
  //     final RemOp<K> r = (RemOp<K>) n;
  //     this.pdata.remove (r.k);
  //     return;
  //   } else {
  //     super.react (from, n);
  //   }
  // }

  // ---

  /**
   * Helper for creating persistent data of type <code>U</code>. Its typical use
   * is for creating an anonymous instance calling a private constructor of
   * <code>U</code>.
   */
  public static abstract class C<T> extends TransientRef.C<T> {}

  // /**
  //  * Requires public access to the constructor of <code>U</code>.
  //  */
  // public static <
  //   K extends Serializable, V extends Serializable,
  //   U extends PersistentHashMap<K, V>
  //   >
  //   C<U> creator (final Class<U> c) {
  //   return new C<U> () { public U b () {
  // 	try { return c.newInstance (); }
  // 	catch (Exception e) {
  // 	  Debug.getLogger (Debug.Persistent).log (BasicLevel.FATAL, e);
  // 	  System.exit (1);
  // 	  return null;
  // 	}
  //     }};
  // }

  // ---

  /**
   * A generic class <code>Ref&lt;K, V&gt;</code> encapsulates manipulations of
   * transient references to persistent data of type <code>HashMap&lt;K,
   * V&gt;</code>.
   */
  public static class Ref<K extends Serializable, V extends Serializable>
    extends TransientRef<PersistentHashMap<K, V>> {

    /**
     * Note the parameter-less constructor of <code>U</code> must have public
     * access.
     */
    public <U extends PersistentHashMap<K, V>> Ref (final Class<U> c) {
      super (c);
    }

    public <U extends PersistentHashMap<K, V>> Ref (final C<U> c) {
      super (c);
    }

    public HashMap<K, V> get ()
      throws NotYetValidatedException, NullRefException {
      return this.read ().get ();
    }

    public void set (final HashMap<K, V> s)
      throws NotYetValidatedException, NullRefException {
      this.read ().set (s);
    }

    public V get (final K k)
      throws NotYetValidatedException, NullRefException {
      return this.read ().get (k);
    }

    public boolean containsKey (final K k)
      throws NotYetValidatedException, NullRefException {
      return this.read ().containsKey (k);
    }

    public void put (final K k, final V v)
      throws NotYetValidatedException, NullRefException {
      this.read ().put (k, v);
    }

    public V remove (final K k)
      throws NotYetValidatedException, NullRefException {
      return this.read ().remove (k);
    }

    public void sync (final Notification n)
      throws NotYetValidatedException, NullRefException {
      this.read ().sync (n);
    }

    public void sync ()
      throws NotYetValidatedException, NullRefException {
      this.sync (null);
    }

  }

  // ---

}
