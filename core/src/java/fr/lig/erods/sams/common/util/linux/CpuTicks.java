/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common.util.linux;

import java.io.Serializable;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

import org.apache.commons.lang3.text.StrTokenizer;

/**
 * Data structure gathering CPU accumulated ticks that can be retrieved on any
 * GNU/Linux-based operating system.
 *
 * <p>For now, this structure only gives very partial information on multicore
 * systems, as it gathers all CPUs as if they were one.
 */
public class CpuTicks implements Serializable {

  private final long user, nice, system, idle, iowait, rest;

  private CpuTicks (final long user, final long nice, final long system,
		  final long idle, final long iowait, final long... rest) {
    this.user = user;
    this.nice = nice;
    this.system = system;
    this.idle = idle;
    this.iowait = iowait;
    long remaining = 0L;
    for (long r: rest) remaining += r;
    this.rest = remaining;
  }

  // ---

  public long getTotal () {
    return user + nice + system + idle + iowait + rest;
  }

  // ---

  public long getIdle () {
    return idle;
  }

  // ---

  private static final StrTokenizer tkzr = new StrTokenizer ();

  /**
   * Builds and returns an instance by reading <tt>"/proc/stat"</tt>.
   *
   * @throws IOException in case of I/O error when trying to read
   *                     <tt>"/proc/stat"</tt>.
   */
  public static CpuTicks readStat () throws IOException {
    BufferedReader br = null;
    try {
      br = new BufferedReader (new FileReader ("/proc/stat"));
      final String info [] = tkzr.reset (br.readLine ()).getTokenArray ();
      assert (info.length >= 6);
      final long user   = Long.parseLong (info [1]);
      final long nice   = Long.parseLong (info [2]);
      final long system = Long.parseLong (info [3]);
      final long idle   = Long.parseLong (info [4]);
      final long iowait = Long.parseLong (info [5]);
      // TODO: rest…
      return new CpuTicks (user, nice, system, idle, iowait);
    // } catch (Exception e) {
      // heum…
    } finally {
      if (br != null) br.close ();
    }
    // return null;		// XXX brrr…
  }

  // ---

  /**
   * Returns the percentage non-idle CPU utilization between <tt>prev</tt> and
   * <tt>now</tt>.
   */
  public static double relativeUsage (final CpuTicks prev, final CpuTicks now) {
    final long prevTotal = prev.getTotal (), prevIdle = prev.getIdle ();
    final long nowTotal  =  now.getTotal (), nowIdle  =  now.getIdle ();
    return 100.0 *
      ((double) ((nowTotal - prevTotal) - (nowIdle  -  prevIdle)) /
       (double)  (nowTotal - prevTotal));
  }

  // ---

}
