/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common.util;

import java.io.Serializable;
import java.util.NoSuchElementException;

public abstract class ISet implements Serializable, Iterable<Integer> {



  // ---

  public class EmptySet extends Exception { }

  // --

  public abstract ISet add (int i);
  public abstract ISet rem (int i);

  public abstract boolean isEmpty ();
  public abstract int card ();

  public abstract boolean contains (int i);
  public abstract int choose () throws EmptySet;

  /**
   * @throws Error in case of incorrect usage.
   */
  public abstract int chooseSure ();

  // ---

  public abstract StringBuffer toString (final StringBuffer o);
  public final String toString () {
    return this.toString (new StringBuffer ()).toString ();
  }

  // ---

  public abstract java.util.Iterator<Integer> iterator ();

  public abstract class Iterator implements java.util.Iterator<Integer> {
    public abstract boolean hasNext ();
    public abstract Integer next ();
    public final void remove () { throw new UnsupportedOperationException (); }
  }

  // ---

  protected abstract int lowEnd  ();
  protected abstract int highEnd ();
  protected abstract boolean lt (int i);
  protected abstract boolean gt (int i);
  protected abstract boolean borders (ISet other);

  // ---------------------------------------------------------------------------

  private static class Interval extends ISet {

    private final int low, high;

    Interval (int low, int high) {
      this.low = low; this.high = high;
    }

    protected int lowEnd  () { assert (! isEmpty ()); return this.low;  }
    protected int highEnd () { assert (! isEmpty ()); return this.high; }

    protected boolean lt (int i) { return this.high < i; }
    protected boolean gt (int i) { return this.low  > i; }

    public boolean isEmpty () { return this.high < this.low; }
    public int     card    () { return this.high - this.low + 1; }

    public ISet add (int i) {
      assert (! this.contains (i));
      if (this.isEmpty ()) return new Interval (i, i);
      if (this.low - 1 == i)  return new Interval (i, this.high);
      if (this.high + 1 == i) return new Interval (this.low, i);
      if (this.gt (i)) return new Split (new Interval (i, i), this);
      if (this.lt (i)) return new Split (this, new Interval (i, i));
      assert false;
      return null;		// avoids a compilation error
    }

    public ISet rem (int i) {
      assert (contains (i));
      if (i == this.low) return new Interval (this.low + 1, this.high);
      if (i == this.high) return new Interval (this.low, this.high - 1);
      else return new Split (new Interval (this.low, i - 1),
			     new Interval (i + 1, this.high));
    }

    protected boolean borders (ISet other) {
      return this.high + 1 == other.lowEnd ();
    }

    public boolean contains (int i) {
      return this.low <= i && i <= this.high;
    }

    public int choose () throws EmptySet {
      if (this.isEmpty ()) throw new EmptySet ();
      return this.lowEnd ();
    };

    public int chooseSure () {
      if (this.isEmpty ()) throw new Error ("Polling empty set!");
      return this.lowEnd ();
    };

    public StringBuffer toString (final StringBuffer o) {
      return o.append ('[').append (this.low).append (", ").
	append (this.high).append (']');
    }

    // ---

    public java.util.Iterator<Integer> iterator () {
      return new IntervalIterator ();
    }

    private class IntervalIterator extends Iterator {
      private int state;
      private IntervalIterator () { this.state = low; }
      public boolean hasNext () { return this.state <= high; }
      public Integer next () throws NoSuchElementException {
	if (this.state > high) throw new NoSuchElementException ();
	return new Integer (this.state ++);
      }
    }

    // ---

  }

  // ---------------------------------------------------------------------------

  private static class Split extends ISet {

    private final ISet down, up;

    Split (final ISet down, final ISet up) {
      assert (down.highEnd () < up.lowEnd ());
      this.down = down;
      this.up = up;
    }

    protected int lowEnd  () { return this.down.lowEnd (); }
    protected int highEnd () { return this.up.highEnd  ();  }

    protected boolean lt (int i) { return this.up.lt   (i); }
    protected boolean gt (int i) { return this.down.gt (i); }

    protected boolean borders (ISet other) {
      return up.highEnd () + 1 == other.lowEnd ();
    }

    public boolean isEmpty () { // assert (false);
      return false; }

    public int card () { return this.down.card () + this.up.card (); }

    public boolean contains (int i) {
      return down.contains (i) || up.contains (i);
    }

    private static ISet split (ISet down, ISet up) {
      if (down.isEmpty ()) return up;
      if (up.isEmpty ())   return down;
      if (down.borders (up)) {
	if (down instanceof Interval &&
	    up   instanceof Interval) {
	  return new Interval (down.lowEnd (), up.highEnd ());
	} else if (down instanceof Interval) {
	  final Split _up = (Split) up;
	  return new Split (split (down, _up.down), _up.up);
	} else if (up instanceof Interval) {
	  final Split _down = (Split) down;
	  return new Split (_down.down, split (_down.up, up));
	}
      }
      return new Split (down, up);
    }

    public ISet add (int i) {
      if      (down.gt (i))           return split (down.add (i), up);
      else if (up.lt (i))             return split (down, up.add (i));
      else if (down.lowEnd ()  < i && // in between `down' boundaries.
	       down.highEnd () > i)   return split (down.add (i), up);
      else if (up.lowEnd ()    < i && // in between `up' boundaries.
	       up.highEnd ()   > i)   return split (down, up.add (i));
      // pfff… arbitrary choice…
      else                            return split (down.add (i), up);
    }

    public ISet rem (int i) {
      if (down.contains (i)) return split (down.rem (i), up);
      else                   return split (down, up.rem (i));
    }

    public int choose () throws EmptySet {
      return this.down.choose ();
      // if (this.down instanceof Interval) return this.down.choose ();
      // else                               return this.up.choose ();
    }

    public int chooseSure () {
      return this.down.chooseSure ();
      // if (this.down instanceof Interval) return this.down.chooseSure ();
      // else                               return this.up.chooseSure ();
    }

    public StringBuffer toString (final StringBuffer o) {
      return o.append (this.down).append (" U ").append (this.up);
    }

    // ---

    public java.util.Iterator<Integer> iterator () {
      return new SplitIterator ();
    }

    private class SplitIterator extends Iterator {
      private boolean inDown = true;
      private java.util.Iterator<Integer> sub;
      private SplitIterator () { this.sub = down.iterator (); }
      public boolean hasNext () {
	return this.inDown || this.sub.hasNext ();
      }
      public Integer next () throws NoSuchElementException {
	if (this.inDown && ! this.sub.hasNext ()) {
	  this.sub = up.iterator ();
	  this.inDown = false;
	}
	return this.sub.next ();
      }
    }

    // ---
  }

  // ---------------------------------------------------------------------------

  public static ISet set (int... ilist) {
    ISet s = empty ();
    for (int i: ilist) s = s.add (i);
    return s;
  }

  public static ISet interval (int i, int j) {
    return new Interval (i, j);
  }

  public static ISet singleton (int i) {
    return new Interval (i, i);
  }

  public static final ISet empty = new Interval (1, 0);
  public static ISet empty () { return empty; }

  // ---------------------------------------------------------------------------

  /**
   * Mutable ISet.
   */
  public static class Ref implements Serializable, Iterable<Integer> {
    private ISet cell;
    public Ref (final ISet s) { this.cell = s; }
    public Ref (final Ref s) { this.cell = s.cell; }

    public ISet cell () { return this.cell; }
    public void set (final Ref s) { this.cell = s.cell; }
    public void set (final ISet s) { this.cell = s; }
    public void clear () { this.cell = empty; }

    public void add (int i) { this.cell = this.cell.add (i); }
    public void rem (int i) { this.cell = this.cell.rem (i); }
    public int poll () throws EmptySet {
      final int i = this.cell.choose ();
      this.cell = this.cell.rem (i);
      return i;
    }
    public int pollSure () {
      final int i = this.chooseSure ();
      this.cell = this.cell.rem (i);
      return i;
    }

    public boolean isEmpty () { return this.cell.isEmpty (); }
    public int     card ()    { return this.cell.card (); }

    public boolean contains (int i) { return this.cell.contains (i); }
    public int choose () throws EmptySet { return this.cell.choose (); }
    public int chooseSure () { return this.cell.chooseSure (); }

    public StringBuffer toString (final StringBuffer o) {
      return this.cell.toString (o);
    }

    public java.util.Iterator<Integer> iterator () {
      return this.cell.iterator ();
    }

    public static Ref empty () { return new Ref (empty); }
    public static Ref singleton (int i) { return new Ref (ISet.singleton (i)); }
  }

  // ---------------------------------------------------------------------------

  // alias for Ref constructor.
  public static Ref ref (final ISet s) { return new Ref (s); }

  // ---------------------------------------------------------------------------

  // private static void iter (ISet s) {
  //   System.err.print ("{ ");
  //   for (Integer i: s) System.err.print (i + ", ");
  //   System.err.println ("}");
  // }

  // XXX not that complete…
  // public static void main (String [] args) {
  //   ISet e = interval (0, 5);
  //   iter (empty ());
  //   iter (e); System.err.println (e +"\n- "+ 2); e = e.rem (2);
  //   iter (e); System.err.println (e +"\n+ "+ 9); e = e.add (9);
  //   iter (e); System.err.println (e +"\n+ "+ 2); e = e.add (2);
  //   iter (e); System.err.println (e +"\n+ "+ 8); e = e.add (8);
  //   iter (e); System.err.println (e +"\n- "+ 8); e = e.rem (8);
  //   iter (e); System.err.println (e +"\n+ "+ 7); e = e.add (7);
  //   iter (e); System.err.println (e +"\n+ "+ 8); e = e.add (8);
  //   iter (e); System.err.println (e +"\n+ "+ 6); e = e.add (6);
  //   iter (e); System.err.println (e);
  // }

  // private static ISet add (ISet e, int i) {
  //   System.err.println (e +"\n+ "+ i);
  //   return e.add (i);
  // }

  // private static ISet rem (ISet e, int i) {
  //   System.err.println (e +"\n- "+ i);
  //   return e.rem (i);
  // }

  // public static void main (String [] args) {
  //   ISet e = interval (17000, 32767);
  //   e = rem (e, 17000);
  //   e = rem (e, 17001);
  //   e = rem (e, 17002);
  //   e = rem (e, 17003);
  //   e = rem (e, 17004);
  //   e = rem (e, 17005);
  //   e = rem (e, 17006);
  //   e = rem (e, 17007);
  //   e = rem (e, 17008);
  //   e = rem (e, 17009);
  //   e = rem (e, 17010);
  //   e = rem (e, 17011);
  //   e = rem (e, 17012);
  //   e = rem (e, 17013);

  //   e = add (e, 17006);
  //   e = add (e, 17007);
  //   e = add (e, 17010);
  //   e = add (e, 17011);
  //   e = add (e, 17012);
  //   e = add (e, 17013);
  //   e = add (e, 17002);
  //   e = add (e, 17003);
  //   e = add (e, 17008);
  //   e = add (e, 17009);
  // }

}
