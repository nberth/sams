/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common.util;

public class Ansi {

  /** Helper class for coloring console texts. */
  public static final class S {
    private final String s;
    public S (final String s, final C... codes) {
      final StringBuffer sb = new StringBuffer ();
      if (codes.length > 0) for (C c: codes) sb.append (c);
      sb.append (s);
      if (codes.length > 0)                  sb.append (C.RESET);
      this.s = sb.toString ();
    }
    public String toString () { return this.s; }
    public StringBuffer toString (final StringBuffer sb) {
      return sb.append (this.s);
    }
  }

  /** Ansi color codes. */
  public enum C {
    // see `http://pueblo.sourceforge.net/doc/manual/ansi_color_codes.html' and
    // `http://stackoverflow.com/questions/5762491/how-to-print-color-in-console-using-system-out-println'
    RESET   ("\u001B[0m"),
    BOLD    ("\u001B[1m"),
    BLACK  ("\u001B[30m"),
    RED    ("\u001B[31m"),
    GREEN  ("\u001B[32m"),
    YELLOW ("\u001B[33m"),
    BLUE   ("\u001B[34m"),
    PURPLE ("\u001B[35m"),
    CYAN   ("\u001B[36m"),
    WHITE  ("\u001B[37m");
    private final String code;
    C (final String c) { this.code = c; }
    public String toString () { return this.code; }
    public StringBuffer toString (final StringBuffer sb) {
      return sb.append (this.code);
    }
  }

  public static void info (final S prefix, final String s, final C... codes) {
    System.out.print (prefix);
    info (s, codes);
  }

  public static void info (final String s, final C... codes) {
    if (codes.length > 0) for (C c: codes) System.out.print (c);
    System.out.print (s);
    if (codes.length > 0)                  System.out.print (C.RESET);
    System.out.println ();
  }

}
