/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;
import java.util.TreeMap;

import fr.dyade.aaa.agent.Agent;
import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.AgentServer;
import fr.dyade.aaa.agent.Channel;
import fr.dyade.aaa.agent.Notification;
import fr.dyade.aaa.common.Debug;
import fr.lig.erods.sams.common.util.AbstractId;
import fr.lig.erods.sams.common.util.CancelableTimer;
import fr.lig.erods.sams.common.util.ISet;
import fr.lig.erods.sams.common.util.Persistent;
import fr.lig.erods.sams.common.util.PersistentHashMap;
import fr.lig.erods.sams.common.util.TransientException;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Artifacts (named notifications carrying data). Instances of this class carry
 * a map associating strings to arbitrary serializable objects, and provide
 * helpers for specifying acknowledgments and timeouts.
 *
 * <p>An instance of this class, referred to as the "cause", can be attached to
 * and retrieved from any <tt>Not</tt>. Instances of this class can thus pass on
 * a chain of past notifications that lead to their delivery. The cause is
 * determined either manually when a new instance of this class is built, or
 * implicitly when an acknowledgment is sent using the {@link #maybeAck} method.
 * Also, the cause of timeout expiration notifications (instances of {@link
 * Nack}) is the notification for which at least one acknowledgment did not
 * arrive in time.
 *
 * <p>This class provides a convenient mechanism to relay a full set of
 * (serializable) objects between agent reactions. When an instance of
 * <tt>Not</tt> is received by an agent, all the notifications belonging to its
 * chain of causes share their internal map: <i>i.e.</i> mutating the cause
 * <em>n</em><tt>.maybeCause()</tt> (with {@link #setProp(String,
 * Serializable)}, or any mutating method of {@link Env}) also modifies the
 * internal map of <em>n</em>; alternatively, modifying the associations of
 * <em>n</em> <em>before</em> calling the method <em>n</em><tt>.maybeAck()</tt>
 * modifies the internal map of the acknowledgment notification that is sent by
 * the method call.
 *
 * <p>Timeout delays start during the call to {@link #setAck(AgentId, String,
 * long)}, {@link #setAck(AgentId, String, long, int, AckAccountingContext)}, or
 * {@link #setAck(AgentId, String, long, ISet, boolean, AckAccountingContext)};
 * that does not say much in general, however as we normally deal with times on
 * the order of magnitude of at least some seconds in the systems sams is
 * designed to manage, then we may be able to live with that — and
 * intrinsically, the JVMs for (standard editions of) Java cannot fit real-time
 * constraints anyway.
 *
 * <p>At most one acknowledgment can be setup per notification instance.
 */
public class Not extends Notification {

  // ---

  private static final Logger logmon = Debug.getLogger (Not.class.getName ());

  // ---

  /** Name of the notification. */
  private final String name;

  /** The possible cause of this notification, if any. */
  private final Not cause;

  /** The map carried by this notification. */
  private final Map<String, Serializable> props;

  // ---

  /** Basic constructor, without specification of associations. */
  public Not (final String name) {
    this (name, new TreeMap<String, Serializable> ());
  }

  /** Constructor with specification of associations. */
  public Not (final String name, final Entry... entries) {
    this (name);
    this.initEntries (entries);
  }

  /**
   * Constructor with explicit map specification. Note the given map is not
   * copied and is used as is: further modifications of <tt>props</tt>
   * <em>before</em> calling {@link Agent#sendTo(AgentId, Notification)} or
   * {@link Channel#sendTo(AgentId, Notification)} also modify the internal map
   * of the new notification.
   */
  public Not (final String name, final Map<String, Serializable> props) {
    this (name, null, props);
  }

  /** Constructor with cause and specification of associations. */
  public Not (final String name, final Not cause, final Entry... entries) {
    this (name, cause);
    this.initEntries (entries);
  }

  // ---

  /**
   * Constructor with cause specification.
   *
   * Note that the set of properties of the notification built with this
   * constructor is <em>physically equal</em> to the set of properties of the
   * given cause: take care that modifying one also modifies the other, as well
   * as the one of any new notification that could have been created from the
   * cause previously.
   */
  private Not (final String name, final Not cause) {
    this (name, cause, cause.props);
  }

  /**
   * Constructor with separate cause and map specification.
   */
  private Not (final String name, final Not cause,
	       final Map<String, Serializable> props) {
    this.name = name;
    this.props = props;		// take care: no copy here!
    this.cause = cause;
  }

  // ---

  private void initEntries (final Entry... entries) {
    for (Entry e: entries)
      this.props.put (e.k, e.v);
  }

  // ---

  /** Returns the name associated to this notification. */
  public String getName () { return this.name; }

  /** Returns the cause of this notification, if any; returns <tt>null</tt>
      otherwise. */
  public Not maybeCause () { return this.cause; }

  /** Returns the value associated to the given key in this notification. */
  public Serializable getProp (final String key) { return props.get (key); }

  /** Associates a value to a key in the internal map of this specification. */
  public void setProp (final String key, final Serializable value) {
    this.props.put (key, value);
  }

  // ---

  public StringBuffer toString (final StringBuffer output) {
    return output.append (this.name);
  }

  // ---------------------------------------------------------------------------

  /**
   * Instances of this class gather acknowledgment accounting code. Most of its
   * methods are used internally in {@link Not}.
   */
  public static final class AckAccountingContext implements Serializable {

    // ---

    private final AgentId associatedAgentId;

    public AckAccountingContext (final AgentId associatedAgentId) {
      this.associatedAgentId = associatedAgentId;
    }

    // ---

    public static final int INVALID = -1;

    private final ISet.Ref availableIdents =
      ISet.ref (ISet.interval (0, Short.MAX_VALUE));

    private final Map<Integer, ISet> retiringIdents =
      new HashMap <Integer, ISet> ();

    private final Map<Integer, ISet.Ref> expectedAcks =
      new HashMap <Integer, ISet.Ref> ();

    // ---

    private int allocContext (final ISet ids) {
      final int i = this.availableIdents.pollSure ();
      assert (! this.expectedAcks.containsKey (i));
      this.expectedAcks.put (i, ISet.ref (ids));
      return i;
    }

    private void maybeReleaseContext (final int ctx) {
      if (ctx == INVALID) return;
      if (this.expectedAcks.remove (ctx) != null)
      	this.availableIdents.add (ctx);
    }

    private void releaseContext (final int ctx) {
      this.expectedAcks.remove (ctx);
      this.availableIdents.add (ctx);
    }

    // ---

    private ISet determineMissingAcks (final int ctx) {
      if (ctx == INVALID) return ISet.empty;
      final ISet.Ref missing;

      // We cannot be sure that a notification of this accounting context will
      // not eventually arrive later. So, we put this context in a special
      // retirement set; it shall be released when new notifications will arrive
      // from the suspected remote agent servers (those whose identifier belong
      // to `missing').
      if ((missing = this.expectedAcks.remove (ctx)) != null) {
	for (final int from: missing) {
	  ISet s = this.retiringIdents.get (from);
	  s = (s == null) ? ISet.singleton (ctx): s.add (from);
	  this.retiringIdents.put (from, s);
	}
	return missing.cell ();
      } else {
	return ISet.empty;
      }
    }

    // ---

    private int create (final ISet dests) {
      return this.allocContext (dests);
    }

    private int create (final int id) {
      return this.allocContext (ISet.singleton (id));
    }

    // ---

    /**
     * Removes <code>id</code> from all sets of awaited acknowledgments
     * sources. To be used when an agent server is removed from the
     * infrastructure, by giving its identifier to this method.
     */
    public void forget (final int id) {
      for (ISet.Ref r: this.expectedAcks.values ())
	if (r.contains (id))
	  r.rem (id);

      this.releaseRetiredContextsOf (id);
    }

    // ---

    private void releaseRetiredContextsOf (final int id) {
      final ISet s = this.retiringIdents.remove (id);
      if (s != null)
      	for (final int ctx: s)
      	  this.availableIdents.add (ctx);
    }

    // ---

    /**
     * @return whether all expected acknowledgments have been received.
     */
    private boolean account (final int ctx, final int src) {
      if (ctx == INVALID) return true; // XXX What if no accounting?

      final ISet.Ref expected = this.expectedAcks.get (ctx);
      if (expected != null) {
	assert (expected.contains (src));
	expected.rem (src);
	return expected.isEmpty ();
      } else {
	return true;
      }
    }

    // ---

  }

  // ---------------------------------------------------------------------------

  /**
   * The type of identifiers for internal timeout management structures, to be
   * able to retrieve them when notification acknowledgments come bac, or when
   * some timeout expire.
   */
  private static final class AlarmId extends AbstractId.Generic {
    private final short srv = AgentServer.getServerId ();
    private AlarmId () throws TransientException {}
    public final boolean equals (final Object o) {
      return super.equals (o) && this.srv == ((AlarmId) o).srv;
    }
    public final int hashCode () {
      return (int) this.srv << 16 + super.hashCode ();
    }
  }

  // ---

  /**
   * Internal class handling timeouts for a particular (set of) expected
   * acknowledgment(s).
   */
  private static class AlarmNotifier implements Runnable, Persistent.Fuzzy {

    private final AgentId dest;
    private final Not expirationNot;
    private long ms_delay;
    private boolean canceled = false;
    private transient CancelableTimer.Task task = null;

    // ---

    AlarmNotifier (final AgentId dest, final Not expirationNot,
		   final long ms_delay) {
      this.dest = dest;
      this.expirationNot = expirationNot;
      this.ms_delay = ms_delay;
    }

    // ---

    public void onRestore () {
      if (! this.canceled)
	this.enable ();
    }

    private void enable () {
      this.task = new CancelableTimer.Task (ms_delay, this);
    }

    // ---

    /**
     * @return <tt>true</tt> if the task has already completed its execution or
     *         has already been canceled.
     */
    private synchronized boolean cancel () {
      final boolean alreadyCanceled = this.canceled;
      this.canceled = true;
      if (this.task != null) {
    	this.task.cancel ();
    	this.task = null;
      }
      return alreadyCanceled;
    }

    // ---

    public synchronized void run () {
      if (this.canceled) return; // already canceled… just in time.

      if (logmon.isLoggable (BasicLevel.DEBUG))
	logmon.log (BasicLevel.DEBUG, "Sending ack timeout expiration "+
		    "notification `"+ this.expirationNot +"'…");

      Channel.sendTo (this.dest, this.expirationNot);
    }
  }

  // ---------------------------------------------------------------------------

  private static class AckInfo implements Serializable {
    private final AgentId ackdest;
    private final String ackname;
    private AckInfo (final AgentId ackdest, final String ackname) {
      this.ackdest = ackdest;
      this.ackname = ackname;
    }
  }

  // ---

  /** Optional acknowledgment request. */
  private AckInfo ackInfo = null;

  // ---

  /**
   * Requests an acknowledgment without timeout. Any agent that receives this
   * notification can then acknowledge the given destination agent about its
   * correct handling by calling the {@link #maybeAck()} method, hereby sending
   * a new {@link Not} instance as acknowledgment.
   *
   * <p>At most one call to {@link #setAck(AgentId, String)}, {@link
   * #setAck(AgentId, String, long)}, {@link #setAck(AgentId, String, long, int,
   * AckAccountingContext)} or {@link #setAck(AgentId, String, long, ISet,
   * boolean, AckAccountingContext)} methods is allowed per instance of {@link
   * Not}.
   *
   * <p>This method may be used outside of an agent context.
   *
   * @param dest the agent to send the acknowledgment to.
   * @param name the name of the expected acknowledgment notification.
   * @return <tt>this</tt>
   * @throws AlreadyRequestedAckException if an acknowledgment is already
   *           requested for this notification.
   */
  public Not setAck (final AgentId dest, final String name) {
    if (this.ackInfo != null) throw new AlreadyRequestedAckException (name);
    this.ackInfo = new AckInfo (dest, name);
    return this;
  }

  // ---

  /**
   * Sends any acknowledgment requested by the sender of this notification.
   *
   * <p>The contents of the internal map of this instance can be updated before
   * calling this method, making the changes transmitted to the recipient of the
   * acknowledgment.
   *
   * <p>This method may be used outside of an agent context.
   */
  public void maybeAck () {
    if (this.ackInfo == null) return;

    if (logmon.isLoggable (BasicLevel.DEBUG))
      logmon.log (BasicLevel.DEBUG, "Sending ack `"+ this.ackInfo.ackname
		  +"' to `"+ this.ackInfo.ackdest +"'.");

    Channel.sendTo (this.ackInfo.ackdest, new Not (this.ackInfo.ackname, this));
    this.ackInfo = null;
  }

  // ---------------------------------------------------------------------------

  private static class AckAccInfo implements Serializable {
    private final int ackAccountingId;
    private final AgentId ackAccountingAgentId;
    private final boolean once;
    private AckAccInfo (final int id, final AgentId aid, final boolean once) {
      this.ackAccountingId      =  id;
      this.ackAccountingAgentId = aid;
      this.once = once;
    }
  }

  private AckAccInfo ackAccInfo = null;
  private AlarmId ackAlarmId = null;

  // ---

  private static final class AMap
    extends PersistentHashMap<AlarmId, AlarmNotifier> {
    protected void newRef () { alarms.write (this); }
  }

  /** Registers all timeout management structures for this agent server. */
  private static
  final PersistentHashMap.Ref<AlarmId, AlarmNotifier> alarms =
    new PersistentHashMap.Ref<AlarmId, AlarmNotifier>
    (new PersistentHashMap.C<AMap> () {
      public AMap b () { return new AMap (); }
    });

  // ---

  /**
   * Requests an acknowledgment with timeout. Any agent that receives this
   * notification can then acknowledge the given destination agent about its
   * correct handling by calling the {@link #maybeAck()} method, hereby sending
   * a new {@link Not} instance as acknowledgment.
   *
   * <p>If no corresponding acknowledgment has been received (<i>i.e.</i> has
   * been handled by {@link Handlers#maybeHandle(AgentId, Object, Notification,
   * boolean, Not.AckAccountingContext)} or a similar method) when the timeout
   * expires, then an acknowledgment timout expiration notification ({@link
   * Nack}) named after the expected acknowledgment is sent to the given
   * destination agent. Late arrival of an acknowledgment is not detected if it
   * has been requested by using this method.
   *
   * <br/>As the cause of the {@link Nack} notification is set to the
   * notification whose acknowledgment is missing, then its internal map can be
   * retrieved by using the {@link #maybeCause()} (never returning <tt>null</tt>
   * in such a case) and then the {@link #getProp(String)} methods.
   *
   * <p>At most one call to {@link #setAck(AgentId, String)}, {@link
   * #setAck(AgentId, String, long)}, {@link #setAck(AgentId, String, long, int,
   * AckAccountingContext)} or {@link #setAck(AgentId, String, long, ISet,
   * boolean, AckAccountingContext)} methods is allowed per instance of {@link
   * Not}.
   *
   * <p>This method may be used outside of an agent context.
   *
   * @param dest the agent to send the acknowledgment to.
   * @param name the name of the expected acknowledgment notification.
   * @param ms_delay the requested timeout delay, in milliseconds.
   * @return <tt>this</tt>
   * @throws TransientException when shared persistent data have not yet been
   *           recovered.
   * @throws AlreadyRequestedAckException if an acknowledgment is already
   *           requested for this notification.
   *
   * @see Handlers#maybeHandle(AgentId, Object, Notification, boolean,
   *      Not.AckAccountingContext)
   */
  public Not setAck (final AgentId dest, final String name, final long ms_delay)
    throws TransientException {
    return this.setAck (dest, name, ms_delay, false);
  }

  // ---

  /**
   * Requests an acknowledgment with timeout and source accounting. Any agent
   * that receives this notification can then acknowledge the given destination
   * agent about its correct handling by calling the {@link #maybeAck()} method,
   * hereby sending a new {@link Not} instance as acknowledgment.
   *
   * <p>If no corresponding acknowledgment coming from the agent server
   * identified by the given number <tt>src</tt> has been received when the
   * timeout expires, then an acknowledgment timout expiration notification
   * ({@link Nack}) named after the expected acknowledgment is sent to the given
   * destination agent. Late handling of an acknowledgment by {@link
   * Handlers#maybeHandle(AgentId, Object, Notification, boolean,
   * Not.AckAccountingContext)} (or a similar method) makes it throw the {@link
   * TimedOutException}.
   *
   * <br/>As the cause of the {@link Nack} notification is set to the
   * notification whose acknowledgment is missing, then its internal map can be
   * retrieved by using the {@link #maybeCause()} (never returning <tt>null</tt>
   * in such a case) and then the {@link #getProp(String)} methods.
   *
   * <p>At most one call to {@link #setAck(AgentId, String)}, {@link
   * #setAck(AgentId, String, long)}, {@link #setAck(AgentId, String, long, int,
   * AckAccountingContext)} or {@link #setAck(AgentId, String, long, ISet,
   * boolean, AckAccountingContext)} methods is allowed per instance of {@link
   * Not}.
   *
   * <p>This method may be used outside of an agent context.
   *
   * @param dest the agent to send the acknowledgment to.
   * @param name the name of the expected acknowledgment notification.
   * @param ms_delay the requested timeout delay, in milliseconds.
   * @param src the identifier of the agent server from which the acknowledgment
   *          is expected.
   * @param aac the acknowledgment accounting context <em>of the <tt>dest</tt>
   *          agent</em>.
   * @return <tt>this</tt>
   * @throws TransientException when shared persistent data have not yet been
   *           recovered.
   * @throws AlreadyRequestedAckException if an acknowledgment is already
   *           requested for this notification.
   *
   * @see Handlers#maybeHandle(AgentId, Object, Notification, boolean,
   *      Not.AckAccountingContext)
   */
  public Not setAck (final AgentId dest, final String name, final long ms_delay,
		     final int src, final AckAccountingContext aac)
    throws TransientException {
    this.ackAccInfo = new AckAccInfo (aac.create (src), aac.associatedAgentId,
				      false);
    try {
      return this.setAck (dest, name, ms_delay, false);
    } catch (TransientException e) {
      aac.releaseContext (this.ackAccInfo.ackAccountingId);
      this.ackAccInfo = null;
      throw e;
    }
  }

  // ---

  /**
   * Requests acknowledgments from multiple sources, with timeout; to be used
   * when a single notification is sent to several destinations. Any agent that
   * receives this notification can then acknowledge the given destination agent
   * about its correct handling by calling the {@link #maybeAck()} method,
   * hereby sending a new {@link Not} instance as acknowledgment.
   *
   * <p>If <tt>once&nbsp;==&nbsp;true</tt>, then the handler associated to the
   * acknowledgment is executed only once all expected acknowledgments have been
   * received. As a consequence, it may not be executed at all if some
   * acknowledgments are missing. The handler is executed once for each received
   * acknowledgment otherwise.
   *
   * <p>In any case, if some acknowledgments coming from one of the agent server
   * identified by a given number belonging to <tt>srcs</tt> has not been
   * received when the timeout expires, then an instance of {@link Nack} named
   * after the expected acknowledgment is sent to the given destination
   * agent. Late handling of an acknowledgment by the {@link
   * Handlers#maybeHandle(AgentId, Object, Notification, boolean,
   * Not.AckAccountingContext)} (or a similar method) makes it throw the {@link
   * TimedOutException}.
   *
   * <br/>As the cause of the {@link Nack} notification is set to the
   * notification whose one acknowledgment is missing, then its internal map can
   * be retrieved by using the {@link #maybeCause()} (never returning
   * <tt>null</tt> in such a case) and then the {@link #getProp(String)}
   * methods.
   *
   * <p>At most one call to {@link #setAck(AgentId, String)}, {@link
   * #setAck(AgentId, String, long)}, {@link #setAck(AgentId, String, long, int,
   * AckAccountingContext)} or {@link #setAck(AgentId, String, long, ISet,
   * boolean, AckAccountingContext)} methods is allowed per instance of {@link
   * Not}.
   *
   * <p>This method may be used outside of an agent context.
   *
   * @param dest the agent to send the acknowledgment to.
   * @param name the name of the expected acknowledgment notification.
   * @param ms_delay the requested timeout delay, in milliseconds.
   * @param srcs the set of identifiers of the agent servers from which an
   *          acknowledgment is expected.
   * @param once specifies whether the acknowledgment handler should be executed
   *          once all expected acknowledgments have been received.
   * @param aac the acknowledgment accounting context <em>of the <tt>dest</tt>
   *          agent</em>.
   * @return <tt>this</tt>
   * @throws TransientException when shared persistent data have not yet been
   *           recovered.
   * @throws AlreadyRequestedAckException if an acknowledgment is already
   *           requested for this notification.
   *
   * @see Handlers#maybeHandle(AgentId, Object, Notification, boolean,
   *      Not.AckAccountingContext)
   */
  public Not setAck (final AgentId dest, final String name, final long ms_delay,
		     final ISet srcs, final boolean once,
		     final AckAccountingContext aac)
    throws TransientException {
    this.ackAccInfo = new AckAccInfo (aac.create (srcs), aac.associatedAgentId,
				      once);
    try {
      return this.setAck (dest, name, ms_delay, srcs.card () > 1);
    } catch (TransientException e) {
      aac.releaseContext (this.ackAccInfo.ackAccountingId);
      this.ackAccInfo = null;
      throw e;
    }
  }

  // ---

  private Not setAck (final AgentId dest, final String name,
		      final long ms_delay, final boolean multiple)
    throws TransientException {
    if (this.ackInfo != null) throw new AlreadyRequestedAckException (name);
    this.ackInfo = new AckInfo (dest, name);

    if (ms_delay > 0L) {	// XXX hum… what else?
      // Recall this is the context of the sender that requires a timeout.

      // Don't share the props map here; if needed, it will be accessible by
      // using the `cause' notification.
      final AlarmNotifier an =
	new AlarmNotifier (dest, new Nack (name, this), ms_delay);
      this.ackAlarmId = new AlarmId ();
      synchronized (alarms) {
	alarms.put (this.ackAlarmId, an);
      }
      an.enable ();
    }
    return this;
  }

  // ---

  /**
   * Exception thrown by {@link Handlers#maybeHandle(AgentId, Object,
   * Notification, boolean, Not.AckAccountingContext)} (or a similar method)
   * when an acknowledgment arrives after its timeout has expired.
   */
  public class TimedOutException extends Exception {
    public String toString () {
      return "Acknowledgment timeout already expired! "+ name;
    }
  }

  // ---

  /**
   * Disables any timeout task related to this notification, if executed on the
   * agent server where this {@link Not} were instantiated; does nothing
   * otherwise.
   */
  private void maybeDisableTimeout (final AlarmId aid)
    throws TransientException, TimedOutException {
    final AlarmNotifier an;

    if (aid.srv != AgentServer.getServerId ())
      // We run on a different agent server.
      return;

    synchronized (alarms) {
      if ((an = alarms.get (aid)) == null)
	throw new TimedOutException ();

      alarms.remove (aid);
    }

    an.cancel ();
  }

  // ---------------------------------------------------------------------------

  /**
   * Negative acknowledgment, or timeout expiration notification.
   */
  public static class Nack extends Not {

    private Nack (final String expected, final Not cause) {
      super (expected, cause);
    }

    public StringBuffer toString (final StringBuffer output) {
      return super.toString (output.append ("NACK-(")).append (')');
    }
  }

  // ---------------------------------------------------------------------------

  /**
   * The type of handlers of {@link Not}.
   */
  public static abstract class Handler<A> {


    protected abstract void p (final AgentId from, final A to, final Not n)
      throws TransientException, Exception;
  }

  // ---

  /**
   * The type of handlers of acknowledgment timeout expiration notifications
   * ({@link Nack}).
   */
  public static abstract class NHandler<A> {

    /**
     * This method is executed once an acknowledgment timeout expiration
     * notification named <em>n</em> is passed to {@link
     * Handlers#maybeHandle(AgentId, Object, Notification, boolean,
     * Not.AckAccountingContext)} (or a similar method) of an set of handlers
     * ({@link Handlers}) in which this handler is associated to <em>n</em>.
     *
     * @param to the agent which received the notification.
     * @param unresp the notification for which at least an acknowledgment is
     *          missing.
     * @param missing the set of identifiers of the source agent servers from
     *          which an acknowledgment is missing.
     */
    protected abstract void a (final A to, final Not unresp, final ISet missing)
      throws TransientException, Exception;

  }

  // ---

  /**
   * Handling status returned by {@link Handlers#maybeHandle(AgentId, Object,
     * Notification, boolean, Not.AckAccountingContext)} (or a similar method)
     * and the like.
   */
  public enum Status { HANDLED, NOT_HANDLED };

  /**
   * Sets of {@link Not} handlers, coupled with associated acknowledgment
   * timeout expiration notification handlers.
   *
   * <p>Note instances of this class are not serializable, to discourage their
   * use in non-static contexts.
   *
   * @param <A> the kind of objects that are capable of handling this set of
   *          notifications (typically subclasses of {@link Agent}, but one
   *          could delegate this handling to another object).
   */
  public static final class Handlers<A> {

    private final HashMap<String,  Handler<A>> hmap =
      new HashMap<String,  Handler<A>> ();

    private final HashMap<String, NHandler<A>> nacks =
      new HashMap<String, NHandler<A>> ();

    // ---

    /**
     * Registers a handler for ordinary {@link Not} notifications.
     *
     * @param k name of the <tt>Not</tt>s to handle.
     */
    public void put (final String k, final  Handler<A> h) {  hmap.put (k, h); }

    /**
     * Registers a handler for acknowledgment timeout expiration notifications
     * ({@link Nack}).
     *
     * @param k name of the <tt>Nack</tt>s to handle.
     */
    public void aut (final String k, final NHandler<A> h) { nacks.put (k, h); }

    // ---

    /**
     * Tries to handle the given notification, without accounting for
     * acknowledgments.
     *
     * <p>See {@link #maybeHandle(AgentId, Object, Notification, boolean,
     * Not.AckAccountingContext)} for complete documentation (<tt>aac</tt> being
     * <tt>null</tt> in this case).
     *
     * @param from the source of the notification.
     * @param to the agent receiving the notification.
     * @param n the notification received.
     * @param required specifies whether the class of the agent <tt>to</tt> is
     *          the most general one that can handle notifications of type
     *          {@link Not}. (XXX Could it be detected automatically by testing
     *          if its direct superclass is {@link Agent}?)
     * @return {@link Status#NOT_HANDLED} if <tt>n</tt> is not an instance of
     *         {@link Not}, or else if it is not handlable by the registered ;
     *         returns {@link Status#HANDLED} otherwise.
     * @throws UnknownNotException if the notification is an instance of {@link
     *           Nack} or else {@link Not}, whose name does not belong to the
     *           set of corresponding handlers, and if the handling was required
     *           by raising the flag <tt>required</tt>.
     * @throws TimedOutException if the notification is an acknowledgment
     *           arriving after the expiration of its delay (<i>i.e.</i> the
     *           Nack has already been handled).
     * @throws TransientException if some persistent internal data has not
     *           <em>yet</em> been recovered.
     * @throws Exception any exception thrown by a handler.
     */
    public Status maybeHandle (final AgentId from, final A to,
			       final Notification n, final boolean required)
      throws TransientException, UnknownNotException, TimedOutException,
      Exception {
      return maybeHandle (from, to, n, required, null);
    }

    // ---

    /**
     * Tries to handle the given notification.
     *
     * <p>If the given notification is an acknowledgment timeout expiration
     * ({@link Nack}) whose name is contained in the set of handlers registered
     * for this kind of notifications, then the associated Nack handler is
     * executed. Otherwise and similarly, if it is an instance of {@link Not}
     * whose name is contained in the set of handlers registered for this kind
     * of notifications, then the associated notification handler is executed
     * unless the notification is not the last comer of a set of expected
     * acknowledgments for which a "once" policy was specified (see {@link
     * #setAck(AgentId, String, long, ISet, boolean, AckAccountingContext)}).
     * {@link Status#HANDLED} is returned in both cases. If the notification is
     * not an instance of {@link Not} and <tt>required</tt> is <tt>false</tt>,
     * then {@link Status#NOT_HANDLED} is returned. Otherwise a {@link
     * UnknownNotException} is thrown.
     *
     * <p>If the given notification is an acknowledgment (<i>i.e.</i> it has a
     * cause notification), and if an acknowledgment accounting context is
     * provided, then the new acknowledgment delivery is accounted. If no more
     * acknowledgments is awaited for the same cause, then the associated
     * timeout is also disabled, if any, before executing the handler; if the
     * timeout already expired but the awaiting agent (<tt>to</tt>, in principle
     * the caller of this method) has not yet received the corresponding {@link
     * Nack}, then this is considered a valid arrival: the handler is executed
     * anyway and the pending Nack will be ignored; in the last case (the Nack
     * came first), then the {@link TimedOutException} is thrown and the handler
     * is not executed.
     *
     * <p>If the given notification is a Nack and an accounting context is
     * provied, then the associated handler is called with the <tt>missing</tt>
     * argument being the set of missing acknowledgments since the corresponding
     * call to {@link Not#setAck(AgentId, String, long, int,
     * AckAccountingContext)} or {@link Not#setAck(AgentId, String, long, ISet,
     * boolean, AckAccountingContext)}. If <tt>aac == null</tt>, then the
     * <tt>missing</tt> argument is an empty set.
     *
     * @param from the source of the notification.
     * @param to the agent receiving the notification.
     * @param n the notification received.
     * @param required specifies whether the class of the agent <tt>to</tt> is
     *          the most general one that can handle notifications of type
     *          {@link Not}. (XXX Could it be detected automatically by testing
     *          if its direct superclass is {@link Agent}?)
     * @param aac acknowledgment accounting context associated to the agent
     *          <tt>to</tt> (can be <tt>null</tt>).
     * @return {@link Status#NOT_HANDLED} if <tt>n</tt> is not an instance of
     *         {@link Not}, or else if it is not handlable by the registered ;
     *         returns {@link Status#HANDLED} otherwise.
     * @throws UnknownNotException if the notificaion is an instance of {@link
     *           Nack} or else {@link Not}, whose name does not belong to the
     *           set of corresponding handlers, and if the handling was required
     *           by raising the flag <tt>required</tt>.
     * @throws TimedOutException if the notification is an acknowledgment
     *           arriving after the expiration of its delay (<i>i.e.</i> the
     *           Nack has already been handled).
     * @throws TransientException if some persistent internal data has not
     *           <em>yet</em> been recovered.
     * @throws Exception any exception thrown by a handler.
     */
    public Status maybeHandle (final AgentId from, final A to,
			       final Notification n, final boolean required,
			       final AckAccountingContext aac)
      throws TransientException, UnknownNotException, TimedOutException,
      Exception {

      if (! (n instanceof Not))
	return Status.NOT_HANDLED;

      final Not not = (Not) n;
      final String name = not.getName ();
      final NHandler<A> nackHandler;
      final Handler<A> notHandler;

      if (n instanceof Nack &&
	  (nackHandler = nacks.get (name)) != null) {

	final Not unresp = not.cause;
	assert (unresp != null);

	// Get missing acknowledgments
	final ISet missing;
	if (aac != null && unresp.ackAccInfo != null) {
	  final AckAccInfo aai = unresp.ackAccInfo;
	  assert (aac.associatedAgentId.equals (aai.ackAccountingAgentId));
	  missing = aac.determineMissingAcks (aai.ackAccountingId);
	} else	{		// Not accounted.
	  missing = ISet.empty;
	}

	// If the expected acknowledgment eventually arrived after or during the
	// expiration of the alarm, and before the delivery of this Nack, then
	// we do nothing and say it's handled anyway.
	final AlarmNotifier an;
	synchronized (alarms) { an = alarms.remove (unresp.ackAlarmId); }
	if (an != null) {
	  // Handle it if there was still an alarm object.
	  nackHandler.a (to, unresp, missing);
	}

	return Status.HANDLED;

      } else if (! (n instanceof Nack) &&
		 (notHandler = hmap.get (name)) != null) {

	// default is true here, so that we execute the handler if cause is
	// null.
	boolean all_expected_acks_received = true;

	if (not.cause != null) {
	  final Not cause = not.cause;
	  final AckAccInfo aai = cause.ackAccInfo;

	  // Account new acknowledgment to the causing notification, if this is
	  // one.
	  if (aac != null && aai != null &&
	      aac.associatedAgentId.equals (aai.ackAccountingAgentId)) {

	    final int ackAccId = aai.ackAccountingId;
	    all_expected_acks_received = aac.account (ackAccId, from.getTo ());

	    if (all_expected_acks_received)
	      // Note that the accouting context may have already been released
	      // if this notification arrived too late (in which case the
	      // TimedOutException will be thrown below).
	      aac.maybeReleaseContext (ackAccId);
	  }

	  if (all_expected_acks_received && cause.ackAlarmId != null)
	    // Notify cause notification that all expected acknowledgments have
	    // been received; may also throw TimedOutException.
	    cause.maybeDisableTimeout (cause.ackAlarmId);

	  // If we arrive here, then this is not a late arrival of
	  // notification. So we can release the accounting contexts that could
	  // have been put in retirement when receiving Nacks related to an
	  // unresponding agent server with the same identifier; in principle,
	  // this means the agent server identifier has been re-assigned.
	  //
	  // XXX Note this assumes the agent associated to this accounting
	  // context dialogues with only one agent running on the remote
	  // server!!!
	  if (aac != null)
	    aac.releaseRetiredContextsOf (from.getTo ());

	  // Force execution of the handler if requested.
	  if (aai != null && aai.once)
	    all_expected_acks_received = true;
	}

	// Ok, lets handle it now if it is not an acknowledgment, or else if it
	// is the last one of a group of expected acknowledgments.
	if (all_expected_acks_received)
	  notHandler.p (from, to, not);

	return Status.HANDLED;

      } else if (! required) {

	return Status.NOT_HANDLED;

      }

      throw not.new UnknownNotException ();
    }

    // ---
  }

  // ---------------------------------------------------------------------------

  /**
   * Runtime exception thrown when an acknowledgment request happens more than
   * once.
   */
  public class AlreadyRequestedAckException extends RuntimeException {
    private AlreadyRequestedAckException (final String newName) {
      super ("Cannot request acknowledgment `"+ newName +"' for notification `"+
	     name +"': acknowledgment `"+ ackInfo.ackname
	     +"' already requested.");
    }
  }

  // ---

  /**
   * Runtime exception indicating the delivery of an instance of {@link Not} for
   * which no handler has been registered.
   */
  public class UnknownNotException extends RuntimeException {
    private UnknownNotException () {
      super ("Unable to handle notification `"+ name +"'!");
    }
  }

  // ---------------------------------------------------------------------------

  /**
   * Note this operation does not survive failures!
   */
  public static void delayed (final AgentId dest, final String name,
  			      final long delay) {
    AgentServer.getTimer ()
      .schedule (new TimerTask () {
  	  public void run () {
  	    Channel.sendTo (dest, new Not (name));
  	  }
  	}, delay);
  }

  // ---------------------------------------------------------------------------

  /**
   * Custom immutable pair for feeding {@link Not} constructors.
   */
  public static final class Entry /* implements Pair<String, Serializable> */ {
    final String k;
    final Serializable v;
    public Entry (final String k, final Serializable v) {
      this.k = k;
      this.v = v;
    }
  }

  // ---

  /**
   * Helper for shortening the construction of entries.
   */
  public static Entry e (final String k, final Serializable v) {
    return new Entry (k, v);
  }

  /**
   * Builds an entry mapping the given key to its expanded value in the given
   * environment, or to the given fallback value if none exists.
   */
  public static Entry e (final Env e, final String k, final String fb) {
    return new Entry (k, e.get (k, fb));
  }

  // ---------------------------------------------------------------------------
}
