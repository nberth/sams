/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common.opcode;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;
import java.util.Calendar;
import java.util.Collection;

import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Notification;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.Sams;
import fr.lig.erods.sams.common.util.ISet;
import fr.lig.erods.sams.common.util.Persistent;
import fr.lig.erods.sams.common.util.PersistentHashMap;
import fr.lig.erods.sams.common.util.PersistentWakeUpTask;
import fr.lig.erods.sams.common.util.TransientException;
import org.apache.commons.lang3.tuple.Pair;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Heartbeats multiplexers gather input measures, and detect missing ones.
 *
 * <p>Instances of this class understand the following set of artifacts:
 *
 * <ul>
 *
 * <li><b>start</b>: starts the periodic checkup of received measures;
 *
 * <li><b>stop</b>: stops the periodic checkup of received measures;
 *
 * <li><b>forget</b>: instructs the heartbeats multiplexer agent to forget any
 * data about the source whose identifiers are listed in the entry <em>ids</em>
 * of the notification.
 *
 * </ul>
 *
 * <p>Instances of this class also send a <b>missing_heartbeats</b> artifact
 * whenever a measure has not been received from at least one source for a given
 * amount of milliseconds <tt>expiry</tt>. This notification carries the set of
 * sources with missing measures in the <em>ids</em> entry.
 *
 * @param <I> the type of input measures
 * @param <O> the type of output measures
 */
public abstract class HBMuxer<I extends Serializable, O extends Serializable>
  extends Muxer<Pair<Short, I>, O> {

  // ---

  protected static class Entry<I> implements Serializable {
    public final long date;
    public final I value;
    Entry (final long date, final I value) {
      this.date = date;
      this.value = value;
    }
  }

  // ---

  private final AgentId expirDest;
  private final long expiry;
  private ISet expired = ISet.empty ();
  private final Map<Short, Entry<I>> entries = new TreeMap<Short, Entry<I>> ();

  // ---

  /**
   * Builds a named heartbeats multiplexer, that determines sources from which
   * measures have not been received for <tt>expiry</tt> milliseconds as sources
   * for which a heartbeat is missing. Such a situation triggers the
   * notification of a <b>missing_heartbeats</b> artifact to the given
   * <tt>expirDest</tt> agent.
   *
   * @param dummyInput a dummy object of type <tt>I</tt>, to build generic
   *                   superclass
   * @param minRefreshPeriod minimal time between two updates of output measure
   * @param dests destination(s) of the output measures
   */
  @SuppressWarnings ("unchecked")
  public HBMuxer (final I dummyInput,
		  final String name, final long minRefreshPeriod,
		  final long expiry, final AgentId expirDest,
		  final AgentId... dests) {
    super (((Class<Pair<Short, I>>)
	    Pair.of (Short.MAX_VALUE, dummyInput).getClass ()),
	   name, minRefreshPeriod, dests);
    this.expiry = expiry;
    this.expirDest = expirDest;
  }

  // ---

  /**
   * Returns the collection of the freshest values for each source.
   */
  protected final Collection<Entry<I>> values () {
    return this.entries.values ();
  }

  /**
   * Returns the number of sources from which a measure is missing.
   */
  protected final int expired () {
    return this.expired.card ();
  }

  // ---

  /**
   * Method to implement, computing a new measure from the values returned by
   * {@link #values}.
   */
  protected abstract O mux ();

  // ---

  private void gatherExpiredOrigins () {

    // Gather expired origins.
    final long now = Calendar.getInstance ().getTimeInMillis ();
    for (final Map.Entry<Short, Entry<I>> p: this.entries.entrySet ()) {
      final short origin = p.getKey ();
      final Entry<I> e = p.getValue ();
      if (now - e.date > this.expiry) {
  	// Purge on expiration.
  	this.expired = this.expired.add (origin);
      } else if (this.expired.contains (origin)) {
  	// Maybe too late, but let's try to recover anyway…
  	this.expired = this.expired.rem (origin);
      }
    }

    // Purge all expired elements now to avoid messing up the iterator.
    for (Integer origin: this.expired)
      this.entries.remove (origin.shortValue ());
  }

  // ---

  private void update () {

    this.gatherExpiredOrigins ();

    // Compute average and update expired values.
    final O o = this.mux ();

    // Notify suspected failures it first to ensure that the destination (the
    // tier manager) has been notified of suspected failures BEFORE receiving
    // any command from the destination of the measure (the decision code).
    if (this.expirDest != null && ! this.expired.isEmpty ()) {
      final Not not = new Not ("missing_heartbeats",
			       Not.e ("ids", this.expired));
      sendTo (this.expirDest, not);
    }

    // Now, publish the measure.
    this.refreshOutput (o);

    // XXX Reschedule next expiration timer?  Heum. maybe too costly w.r.t the
    // benefits…
  }

  // ---

  public boolean refreshMeasure (final String name,
				 final Pair<Short, I> value)
    throws Exception {
    try {
      // Reception time seems sufficient if we consider the magnitude of the
      // periods we consider…
      final long now = Calendar.getInstance ().getTimeInMillis ();
      // this.entries.put (name, new Entry (now, value.floatValue ()));
      this.entries.put (value.getLeft (),
			new Entry<I> (now, value.getRight ()));
    } catch (ClassCastException e) {
      throw new Exception ("Invalid type for measure `"+ name +"'!", e);
    }
    this.update ();
    return true;
  }

  // ---

  public static class ForceUpdateNot extends Notification {}

  // ---

  private static final class WutMap
    extends PersistentHashMap<AgentId, PersistentWakeUpTask>
    implements Persistent.Fuzzy {
    protected void newRef () { wm = this; }
  }

  private static transient WutMap wm = null;

  static {
    Sams.runOnInit (new Runnable () { public void run () {
      wm = new WutMap ();
    }});
  }

  // ---

  private void start (final Not cause)
    throws TransientException {
    assert (! wm.containsKey (this.getId ()));
    logmon.log (BasicLevel.INFO, "Starting heartbeats muxer `"+
		this.getName () +"'.");
    wm.put (this.getId (),
	    new PersistentWakeUpTask (this.getId (), ForceUpdateNot.class,
				      this.expiry));
    cause.maybeAck ();
  }

  private void stop (final Not cause)
    throws TransientException {
    assert (wm.containsKey (this.getId ()));
    logmon.log (BasicLevel.INFO, "Stopping heartbeats muxer `"+
		this.getName () +"'.");
    final PersistentWakeUpTask t = wm.get (this.getId ());
    t.cancel ();
    wm.remove (this.getId ());
    cause.maybeAck ();
  }

  private void forget (final Not cause) {
    final ISet forget = (ISet) cause.getProp ("ids");
    for (Integer i: forget)
      if (this.expired.contains (i))
	this.expired = this.expired.rem (i);
    cause.maybeAck ();
  }

  // ---

  private static final Not.Handlers<HBMuxer<?, ?>> nh =
    new Not.Handlers<HBMuxer<?, ?>> ();
  static {
    nh.put ("start", new Not.Handler<HBMuxer<?, ?>> () {
    	protected void p (final AgentId from, final HBMuxer<?, ?> to, final Not n)
	  throws TransientException { to.start (n); }});
    nh.put ("stop", new Not.Handler<HBMuxer<?, ?>> () {
    	protected void p (final AgentId from, final HBMuxer<?, ?> to, final Not n)
	  throws TransientException { to.stop (n); }});
    nh.put ("forget", new Not.Handler<HBMuxer<?, ?>> () {
    	protected void p (final AgentId from, final HBMuxer<?, ?> to, final Not n)
	{ to.forget (n); }});
  }

  // ---

  public void react (final AgentId from, final Notification n)
    throws Exception {
    if (n instanceof ForceUpdateNot) { this.update (); return; }
    if (nh.maybeHandle (from, this, n, false) == Not.Status.HANDLED) return;
    super.react (from, n);
  }

  // ---

}
