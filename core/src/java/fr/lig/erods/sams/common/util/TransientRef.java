/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common.util;

import java.io.Serializable;

import fr.dyade.aaa.common.Debug;
import fr.lig.erods.sams.common.Sams;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Encapsulate a potentially null transient reference, and automatically throw a
 * <code>TransientException</code> whenever this reference is read null.
 */
public class TransientRef<S /* extends Persistent */> implements Serializable {

  // ---

  private transient S ref = null;

  // ---

  private static final Logger logmon =
    Debug.getLogger (Persistent.class.getName ()); // Use persistent logger…

  // ---

  /**
   * Using this constructor requires granting public access to the
   * parameter-less constructor of <code>S</code>.
   */
  public <U extends S> TransientRef (final Class<U> c) {
    Sams.runOnInit (new Runnable () { public void run () {
      try {
	ref = c.newInstance ();
      } catch (Exception e) {
	logmon.log (BasicLevel.FATAL, "Error when creating new instance of "+
		    "class "+ c.getName () +"!", e);
	System.exit (1);
      }
    }});
  }

  // ---

  public abstract static class C<S> { public abstract S b (); }

  public <U extends S> TransientRef (final C<U> c) {
    Sams.runOnInit (new Runnable () { public void run () {
      ref = c.b ();
    }});
  }

  // ---

  public S read () throws Persistent.NullRefException {
    if (this.ref == null) throw new Persistent.NullRefException ();
    else return this.ref;
  }

  // ---

  public S write (final S s) {
    final S prev = this.ref;
    this.ref = s;
    return prev;
  }

  // ---
}
