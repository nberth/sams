/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common;

import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;

import fr.dyade.aaa.agent.Agent;
import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Notification;
import fr.dyade.aaa.common.Debug;
import fr.lig.erods.sams.common.util.PersistentHashMap;
import fr.lig.erods.sams.common.util.TransientException;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Namespace for general purpose utilities.
 *
 * <p>This class encapsulates:
 *
 * <ul>
 *
 * <li>An environment containing generic internal values as well as properties
 * specifying the application and its configuration;
 *
 * <li>Some constant fields for defining properties files;
 *
 * <li>A blackboard, <i>i.e.</i> a persistent map that can survive failures.
 *
 * </ul>
 */
public final class Sams {

  // ---

  /** Instantiation of this class is useless. */
  private Sams () {}

  // ---

  private static final Logger logmon = Debug.getLogger (Sams.class.getName ());

  // ---

  public static final String SAMS_ENV_FILE_DEFAULT   = "sams.properties";
  public static final String SAMS_ENV_FILE_PROPERTY  = "sams.env.file";

  public static final String APP_DIR_DEFAULT         = null;
  public static final String APP_DIR_PROPERTY        = "app.dir";

  public static final String APP_ENV_FILE_DEFAULT    = "etc/env.properties";
  public static final String APP_ENV_FILE_PROPERTY   = "app.env.file";

  public static final String CFG_DIR_DEFAULT         = null;
  public static final String CFG_DIR_PROPERTY        = "cfg.dir";

  public static final String CFG_ENV_FILE_DEFAULT    = "env.properties";
  public static final String CFG_ENV_FILE_PROPERTY   = "cfg.env.file";

  public static final String LOCAL_ENV_FILE_DEFAULT  = "local.properties";
  public static final String LOCAL_ENV_FILE_PROPERTY = "local.env.file";

  // ---

  /**
   * Global environment properties file or resource name, looked up in current
   * directory or root of classpath.
   *
   * Defaults to the value of system property {@link #SAMS_ENV_FILE_PROPERTY},
   * or else {@link #SAMS_ENV_FILE_DEFAULT}.
   */
  public static final String SAMS_ENV_FILE =
    System.getProperty (SAMS_ENV_FILE_PROPERTY, SAMS_ENV_FILE_DEFAULT);

  /**
   * Application-specific directory.
   *
   * Defaults to the value of system property {@link #APP_DIR_PROPERTY}, or else
   * <tt>null</tt>.
   */
  public static final String APP_DIR =
    System.getProperty (APP_DIR_PROPERTY, APP_DIR_DEFAULT);

  /**
   * Application-specific properties file or resource name, looked up in {@link
   * #APP_DIR} (from the current directory if relative) or root of the
   * classpath.
   *
   * Defaults to the value of system property {@link #APP_ENV_FILE_PROPERTY}, or
   * else {@link #APP_ENV_FILE_DEFAULT}.
   */
  public static final String APP_ENV_FILE =
    System.getProperty (APP_ENV_FILE_PROPERTY, APP_ENV_FILE_DEFAULT);

  /**
   * Application-specific configuration directory.
   *
   * Defaults to the value of system property {@link #CFG_DIR_PROPERTY}, or else
   * <tt>null</tt>.
   */
  public static final String CFG_DIR =
    System.getProperty (CFG_DIR_PROPERTY, CFG_DIR_DEFAULT);

  /**
   * Application-specific configuration properties file or resource name, looked
   * up in {@link #CFG_DIR} (from the current directory if relative) or root of
   * the classpath.
   *
   * Defaults to the value of system property {@link #CFG_ENV_FILE_PROPERTY}, or
   * else {@link #CFG_ENV_FILE_DEFAULT}.
   */
  public static final String CFG_ENV_FILE =
    System.getProperty (CFG_ENV_FILE_PROPERTY, CFG_ENV_FILE_DEFAULT);

  /**
   * Local, site-specific, environment properties file or resource name, looked
   * up in current directory or root of classpath.
   *
   * Defaults to the value of system property {@link #LOCAL_ENV_FILE_PROPERTY},
   * or else {@link #LOCAL_ENV_FILE_DEFAULT}.
   */
  public static final String LOCAL_ENV_FILE =
    System.getProperty (LOCAL_ENV_FILE_PROPERTY, LOCAL_ENV_FILE_DEFAULT);

  // ---

  /**
   * Local environment. It is initialized by loading the following resources in
   * turn, each new value for an already defined key overriding the previous
   * one:
   *
   * <ul>
   *
   * <li>{@link #SAMS_ENV_FILE};
   *
   * <li>{@link #APP_ENV_FILE}, looking first in {@link #APP_DIR} directory if
   * non-null;
   *
   * <li>{@link #CFG_ENV_FILE}, looking first in {@link #CFG_DIR} directory if
   * non-null;
   *
   * <li>{@link #LOCAL_ENV_FILE};
   *
   * </ul>
   *
   * <p>Its contents should not be modified once the agent server has started.
   */
  public static final Env env = new Env ();
  static {
    env.load (SAMS_ENV_FILE);
    env.load (APP_ENV_FILE, APP_DIR);
    env.load (CFG_ENV_FILE, CFG_DIR);
    env.load (LOCAL_ENV_FILE);
    // env.loadSysProps ();
  }

  // ---

  private static LinkedList<Runnable> onInit =
    new LinkedList<Runnable> ();

  private static void onInit () {
    final LinkedList<Runnable> l = onInit;
    onInit = null;		// avoid messing up the iterator…
    for (final Runnable r: l) r.run ();
  }

  private static void onRestore () throws IOException {
    onInit = null;		// avoid wasting some memory stupidly.
  }

  /**
   * Register the given runnable to be executed right after intialization of the
   * agent server. They are not executed at all in case of recovery.
   *
   * The typical usage of this method is the recovery of persistent data (e.g.,
   * subclasses of <code>fr.lig.erods.sams.common.util.Persistent</code>), like
   * exemplified below:
   *
   * <blockquote><pre>
   * static final class StringMap
   *   extends PersistentHashMap&lt;String, Serializable&gt; {
   *   protected void newRef () { staticStringMap = this; }
   * }
   *
   * static transient StringMap staticStringMap = null;
   *
   * static {
   *   Sams.runOnInit (new Runnable () { public void run () {
   *     staticStringMap = new StringMap ();
   *   }});
   * }
   * </pre></blockquote>
   *
   * In this example, the <code>staticStringMap</code> object is not
   * instantiated durign recovery of the agent server; instead it is assigned
   * when reloaded in memory from the persistent storage. Of course, this can
   * cause errors when the map is used before the object is loaded…
   *
   * @see fr.lig.erods.sams.common.util.Persistent
   */
  public static void runOnInit (final Runnable r) {
    if (onInit == null) r.run ();
    else onInit.add (r);
  }

  // ---

  /**
   * To be called during sams' init sequence.
   */
  public static void init (final String name) throws IOException {
    logmon.log (BasicLevel.DEBUG, "init ()");
    Sams.env.put ("node.name", name);
    onInit ();
  }

  /**
   * To be called at the begining of sams' restore sequence.
   */
  public static void restore (final String name) {
    logmon.log (BasicLevel.DEBUG, "restore ()");
    Sams.env.put ("node.name", name);
    try {
      onRestore ();
    } catch (IOException e) {
      logmon.log (BasicLevel.ERROR,
		  "Sams seems unable to restore from previous state!", e);
    }
  }

  // ---

  private static final class Blackboard
    extends PersistentHashMap<Serializable, Serializable> {
    protected void newRef () { bb = this; }
  }

  private static transient Blackboard bb = null;

  static {
    Sams.runOnInit (new Runnable () { public void run () {
      logmon.log (BasicLevel.DEBUG, "Creating new blackboard.");
      bb = new Blackboard ();
    }});
  }

  // ---

  /**
   * Saves an object into the blackboard.
   *
   * <p>Should only be used in agent reaction execution context, unless the
   * agent server is stopped.
   */
  public synchronized static void save (final Serializable k,
					final Serializable v)
    throws TransientException {
    bb.put (k, v);
  }

  // ---

  /**
   * Loads an object from the blackboard.
   *
   * <p>Should only be used in agent reaction execution context, unless the
   * agent server is stopped.
   */
  public synchronized static Serializable load (final Serializable k)
    throws TransientException {
    return bb.get (k);
  }

  // ---

}
