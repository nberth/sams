/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.common;

import java.io.Serializable;
import java.net.UnknownHostException;

import fr.dyade.aaa.common.Debug;
import fr.lig.erods.sams.common.util.ISet;
import fr.lig.erods.sams.common.util.PersistentHashMap;
import fr.lig.erods.sams.common.util.TransientException;
import org.apache.commons.lang3.text.StrTokenizer;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

public class Node implements Serializable {

  // ---

  private static final Logger slogmon = Debug.getLogger (Node.class.getName ());

  // ---

  private final int id;
  public final String user, host;

  // ---

  private static final StrTokenizer uhtkzr = new StrTokenizer ()
    .setDelimiterChar ('@')
    .setIgnoreEmptyTokens (false); // in case it starts or ends with `@'…

  // ---

  public Node (final String uh)
    throws TransientException, InvalidSpecifierException {

    final String info [] = (uh != null)
      ? uhtkzr.reset (uh).getTokenArray ()
      : new String [] {};

    if (info.length > 2 || (info.length == 2 && "".equals (info [1])) ||
	info.length < 1 ||                      "".equals (info [0]))
      throw new InvalidSpecifierException (uh);

    if (info.length == 2) {
      this.user = info [0];
      this.host = info [1];
    } else {
      this.host = info [0];
      this.user = System.getProperty ("user.name");
      slogmon.log (BasicLevel.INFO, "Missing user in node specifier. Using `"+
		   this.user +'@'+ this.host +"'.");
    }

    this.id = allocId ();

  }

  // ---

  public boolean equals (Object o) {
    return (o instanceof Node) && this.id == ((Node) o).id;
  }

  public int hashCode () {
    return this.id;
  }

  // ---

  public String getUser () { return this.user; }
  public String getHost () { return this.host; }

  // ---

  public String toString () {
    return this.user +'@'+ this.host +'-'+ this.id;
  }

  public StringBuffer toString (final StringBuffer o) {
    return o.append (this.user).append ('@').append (this.host)
      .append (this.id);
  }

  // ---------------------------------------------------------------------------

  // private static final class Ids extends Persistent<ISet> {
  //   private Ids () { super (ISet.interval (0, Short.MAX_VALUE)); }
  //   protected void newRef () { availableIds.write (this); }
  // }

  // private static final Persistent.Ref<ISet> availableIds =
  //   new Persistent.Ref<ISet> (new Persistent.C<Ids> () {
  // 	public Ids b () { return new Ids (); }
  //     });

  // ---

  private static int allocId ()
    throws TransientException {
    // final ISet s = availableIds.get ();
    // final int i = s.chooseSure ();
    // availableIds.set (s.rem (i));
    Integer n = (Integer) Sams.load ("node.id.current");
    if (n == null) n = new Integer (1);
    final int i = n.intValue ();
    Sams.save ("node.id.current", i + 1);
    return i;
  }

  // ---

  // Never used…
  // private static void freeId (int i)
  //   throws TransientException {
  //   availableIds.set (availableIds.get ().add (i));
  // }

  // ---------------------------------------------------------------------------

  private static final ISet defaultPorts;
  static {
    final String low =  Sams.env.get ("ports.range.low", null);
    final String high = Sams.env.get ("ports.range.high", null);
    defaultPorts =
      ISet.interval ((low  != null) ? new Integer (low) : 17000,
  		     (high != null) ? new Integer (high) : Short.MAX_VALUE);
  }

  // ---

  // `host → available ports' mapping.
  private static final class AllocMap extends PersistentHashMap<String, ISet> {
    protected void newRef () { availablePorts.write (this); }
  }

  private static final PersistentHashMap.Ref<String, ISet> availablePorts =
    new PersistentHashMap.Ref<String, ISet>
    (new PersistentHashMap.C<AllocMap> () {
      public AllocMap b () { return new AllocMap (); }
    });

  // ---

  public static class OutOfPortException extends Exception {
    OutOfPortException (final String h) {
      super ("Unable to allocate new port for host `"+ h +"'!");
    }
  }

  // ---

  public static int allocPort (final String host)
    throws OutOfPortException, TransientException {
    synchronized (availablePorts) {
      ISet s = availablePorts.get (host);
      if (s == null) s = defaultPorts;
      try {
	int i = s.choose ();
	if (slogmon.isLoggable (BasicLevel.DEBUG))
	  slogmon.log (BasicLevel.DEBUG, "Allocating port "+i+" for `"+host
		       +"'.");
	availablePorts.put (host, s.rem (i));
	availablePorts.sync ();
	return i;
      } catch (ISet.EmptySet e) {
	throw new OutOfPortException (host);
      }
    }
  }

  public int allocPort ()
    throws OutOfPortException, TransientException {
    return Node.allocPort (this.getHost ());
  }

  // ---

  public static void releasePort (final String host, final int port)
    throws TransientException {
    if (slogmon.isLoggable (BasicLevel.DEBUG))
      slogmon.log (BasicLevel.DEBUG, "Releasing port "+port+" for `"+host+"'.");
    synchronized (availablePorts) {
      availablePorts.put (host, availablePorts.get (host).add (port));
    }
  }

  public void releasePort (final int port)
    throws TransientException {
    Node.releasePort (this.getHost (), port);
  }

  // ---

  public static final void initLocalHost (final String s)
    throws UnknownHostException, TransientException {
    final String h = (s != null) ? s :
      java.net.InetAddress.getLocalHost ().getHostName ();
    Sams.save ("local.host", h);
  }

  static {
    Sams.runOnInit (new Runnable () { public void run () {
      try {
	initLocalHost (Sams.env.get ("local.host", null));
      } catch (Exception e) {
	slogmon.log (BasicLevel.ERROR, "Unable to initialize local info!", e);
	System.exit (1);
      }
    }});
  }

  // ---

  public static class InvalidSpecifierException extends Exception {
    public InvalidSpecifierException (final String s) {
      super ("Invalid specifier: `" + s + "'");
    }
  }

  // ---

}
