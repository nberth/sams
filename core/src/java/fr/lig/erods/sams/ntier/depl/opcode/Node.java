/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.depl.opcode;

import java.io.IOException;

import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Notification;
import fr.lig.erods.sams.common.Env;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.Sams;
import fr.lig.erods.sams.common.util.Ansi.C;
import fr.lig.erods.sams.common.util.CommandBuilder;
import fr.lig.erods.sams.common.util.ProcessHandle;
import fr.lig.erods.sams.common.util.TransientException;
import fr.lig.erods.sams.depl.opcode.Task;
import fr.lig.erods.sams.depl.opcode.sensors.LinuxCPUMonitor;
import fr.lig.erods.sams.depl.opcode.sensors.UnixProcessMonitor;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Actuator agent specialized for remote process management in multi-tiers
 * applications.
 *
 * Not yet robust to hard failures!
 */
public abstract class Node extends Task {

  // ---

  private static final Not.Handlers<Node> nh = new Not.Handlers<Node> ();
  static {

    nh.put ("init", new Not.Handler<Node> () {
    	protected void p (final AgentId from, final Node to, final Not n)
	  throws Exception {
	  to.init (n);
	}});
    nh.put ("start", new Not.Handler<Node> () {
    	protected void p (final AgentId from, final Node to, final Not n)
	  throws Exception {
    	  to.start (n);
    	}});
    nh.put ("restart", new Not.Handler<Node> () {
    	protected void p (final AgentId from, final Node to, final Not n)
	  throws Exception {
    	  to.restart (n);
    	}});
    nh.put ("stop", new Not.Handler<Node> () {
    	protected void p (final AgentId from, final Node to, final Not n)
	  throws Exception {
    	  to.stop (n);
    	}});

    // ---

    nh.put ("configure_node", new Not.Handler<Node> () {
    	protected void p (final AgentId from, final Node to, final Not n)
	  throws Exception {
	  to.configure (n);
	}});

    // ---

    nh.put ("start_task_monitor", new Not.Handler<Node> () {
    	protected void p (final AgentId from, final Node to, final Not n)
	  throws Exception {
    	  to.start_app_monitor (n);
    	}});
  }

  // ---

  protected final CommandBuilder cb;
  protected transient ProcessHandle currentProcess = null;

  private long    appMonPeriod;
  private AgentId appMonDestId;

  // ---

  protected void init (final Not cause)
    throws IOException, Exception {
    logmon.log (BasicLevel.DEBUG, "Init notification received.");

    appMonPeriod = Long.parseLong ((String) cause.getProp ("task-monitor.period"));
    appMonDestId = (AgentId) cause.getProp ("task-monitor.dest.id");

    currentProcess = cb.byKey ("init");
    sendAfterProcess (this.getId (), new Not ("start", cause));
  }

  // ---

  protected void start (final Not cause1)
    throws TransientException, Exception {
    logmon.log (BasicLevel.DEBUG, "Start notification received.");

    // Pop by one cause, noting that the set of properties is shared with any
    // existing previous causes… (and this will be completed below).
    final Not init = cause1.maybeCause ();
    assert (init != null);
    // this.setupNodeStartedProperties (init);
    env.exportFieldsGroup ("as_tail", init);

    // {
    //   logmon.log (BasicLevel.INFO, "Starting Unix load monitor.");
    //   final Long p = (Long) init.getProp ("load-monitor.period");
    //   final AgentId d = (AgentId) init.getProp ("load-monitor.dest.id");
    //   UnixLoadMonitor.start (Main.env.getS ("node.name"), d, p.longValue ());
    // }

    // XXX maybe we should start it AFTER the process has started…
    {
      info ("Starting Linux CPU monitor.", C.PURPLE);
      final Long   p = new Long  ((String) init.getProp ("cpu-monitor.period"));
      final Double a = new Double ((String) init.getProp ("cpu-monitor.alpha"));
      final AgentId d = (AgentId) init.getProp ("cpu-monitor.dest.id");
      // LinuxCPUMonitor.start (Main.env.getS ("node.name"), d, hl, p.longValue ());
      LinuxCPUMonitor.start (Sams.env.get ("node.name"), a, p, d);
    }

    info ("Starting task.", C.PURPLE);

    currentProcess = cb.byKey ("start")
      .thenSendAck (init)
      .thenSend (this.getId (), new Not ("start_task_monitor"));
  }

  // ---

  protected void restart (final Not cause)
    throws TransientException, Exception {
    info ("Stopping Linux process monitor.", C.PURPLE);
    UnixProcessMonitor.stop ();

    info ("Restarting task.", C.PURPLE);
    currentProcess = cb.byKey ("restart")
      .thenSendAck (cause)
      .thenSend (this.getId (), new Not ("start_task_monitor"));
  }

  // ---

  // private void setupNodeStartedProperties (final Not ack) {
  //   env.exportFieldsGroup ("as_tail", ack);
  // }

  // ---

  protected void start_app_monitor (final Not cause) {
    if (env.containsKey ("pidfile")) {
      info ("Starting Linux process monitor.", C.PURPLE);
      UnixProcessMonitor.start (appMonDestId, env.get ("pidfile"), appMonPeriod);
    } else {
      logmon.log (BasicLevel.WARN,
		  "Not startign the process monitor: PID file unspecified.");
    }
    cause.maybeAck ();
  }

  // ---

  protected void stop (final Not cause)
    throws TransientException, Exception {

    logmon.log (BasicLevel.DEBUG, "Stop notification received.");

    info ("Stopping Linux process monitor.", C.PURPLE);
    UnixProcessMonitor.stop ();

    info ("Stopping Linux CPU monitor.", C.PURPLE);
    // UnixLoadMonitor.stop ();
    LinuxCPUMonitor.stop ();

    info ("Stopping task.", C.PURPLE);
    currentProcess = cb.byKey ("stop")
      .thenSendAck (cause);
  }

  // ---

  protected abstract void configure (final Not cause) throws Exception;

  // ---

  public void react (AgentId from, Notification n) throws Exception {
    if (nh.maybeHandle (from, this, n, false) == Not.Status.HANDLED) return;
    super.react (from, n);
  }

  // ---

  protected final void sendAfterProcess (final AgentId id, final Not n)
    throws IOException {
    if (currentProcess != null) currentProcess.thenSend (id, n);
    else                        sendTo (id, n);
  }

  // ---

  protected Node (final Env env) {
    super (env);
    this.cb = new CommandBuilder (env);
  }

  // ---

  // protected void setupNodeStartedProperties (final Not ack) {}

  // ---------------------------------------------------------------------------

  // "Most concrete sub-class" defines log name.
  protected final String getLogTopic () {
    return this.getClass ().getName ();
  }

  // ---

}
