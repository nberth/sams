/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.center.opcode;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fr.dyade.aaa.agent.Agent;
import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Notification;
import fr.lig.erods.sams.center.DecisionCode;
import fr.lig.erods.sams.center.Remote;
import fr.lig.erods.sams.common.Env;
import fr.lig.erods.sams.common.Node;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.opcode.Impulse;
import fr.lig.erods.sams.common.opcode.Measure;
import fr.lig.erods.sams.common.util.Ansi.C;
import fr.lig.erods.sams.common.util.Ansi;
import fr.lig.erods.sams.common.util.ISet;
import fr.lig.erods.sams.common.util.TransientException;
import fr.lig.erods.sams.ntier.center.Cluster;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Abstract class encapsulating common operating code for tier management.
 */
abstract class Tier extends Agent {

  // ---

  protected final String name;
  protected final AgentId dc;
  protected final AgentId source;
  protected final Env env;
  protected final Not.AckAccountingContext aac;

  // ---

  private final Cluster cluster;
  private final Env taskEnv;
  protected final Map<Integer, Remote> remotes =
    new HashMap<Integer, Remote> ();

  // load data gatherer.
  // private final AgentId loadMuxer;

  /** CPU percentages gatherer. */
  private final AgentId cpuLoadMuxer;

  private transient Ansi.S prefix;

  // ---------------------------------------------------------------------------

  // State-related sets of remote identifiers.

  protected final ISet.Ref deploying = ISet.Ref.empty ();
  protected final ISet.Ref starting  = ISet.Ref.empty ();
  protected final ISet.Ref running   = ISet.Ref.empty ();
  protected final ISet.Ref stopping  = ISet.Ref.empty ();
  protected final ISet.Ref stopped   = ISet.Ref.empty ();

  protected final ISet.Ref suspected = ISet.Ref.empty ();
  protected final ISet.Ref repairing = ISet.Ref.empty ();
  protected final ISet.Ref failed    = ISet.Ref.empty (); // no remote agent.

  // ---

  // Note `to' can be null, in which case the identifier disapears from the
  // specified sets.
  protected static void transitToFrom (final int id, final ISet.Ref to,
				       final ISet.Ref... from) {
    if (to != null) {
      assert (! to.contains (id));
      to.add (id);
    }
    boolean done = false;
    for (ISet.Ref s: from)
      if (s.contains (id)) {
	s.rem (id);
	assert !done;
	done = true;
      }
    assert done;
  }

  // ---

  protected final void info (final String s, final Ansi.C... codes) {
    if (logmon.isLoggable (BasicLevel.INFO))
      logmon.log (BasicLevel.INFO, name +" tier: "+ s);

    Ansi.info (this.prefix, s, codes);
  }

  // ---

  private static final Not.Handlers<Tier> nh = new Not.Handlers<Tier> ();
  static {

    nh.put ("init", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException { to.init (); }});
    nh.put ("start", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException { to.start (); }});
    nh.put ("stop", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException { to.stop (); }});

    // ---

    nh.put ("deployment_started", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException { to.deployment_started (n); }});
    nh.aut ("deployment_started", new Not.NHandler<Tier> () { // NACK!
    	protected void a (final Tier to, final Not unresp, final ISet missing)
    	  throws TransientException {
	  to.deployment_started_timeout (unresp); }});

    // ---

    nh.put ("deployment_done", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException { to.deployment_done (n); }});
    nh.put ("deployment_stops", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException {
	  to.deployment_stops (from.getTo (), n); }});
    nh.aut ("deployment_stops", new Not.NHandler<Tier> () { // NACK!
    	protected void a (final Tier to, final Not unresp, final ISet missing)
	  throws TransientException {
	  to.deployment_stops_timeout (unresp, missing); }});

    // ---

    nh.put ("undeployment_done", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException { to.undeployment_done (n); }});
    nh.aut ("undeployment_done", new Not.NHandler<Tier> () { // NACK!
    	protected void a (final Tier to, final Not unresp, final ISet missing)
    	  throws TransientException {
	  to.undeployment_done_timeout (unresp); }});

    // ---

    nh.put ("start_node", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException { to.deploy_and_start_node (); }});
    nh.put ("task_started", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException { to.task_started (from.getTo (), n); }});
    nh.aut ("task_started", new Not.NHandler<Tier> () { // NACK!
    	protected void a (final Tier to, final Not unresp, final ISet missing)
	  throws TransientException {
    	  to.task_started_timeout (unresp, missing); }});

    // ---

    nh.put ("stop_node", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException { to.stop_and_undeploy_node (); }});
    nh.put ("task_stopped", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException {
	  to.task_stopped (from.getTo (), n); }});
    nh.aut ("task_stopped", new Not.NHandler<Tier> () { // NACK!
    	protected void a (final Tier to, final Not unresp, final ISet missing)
	  throws TransientException {
    	  to.task_stopped_timeout (unresp, missing); }});

    // ---

    nh.put ("task_failure", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException { to.task_failure (from.getTo (), n); }});
    nh.put ("task_repaired", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException {
	  to.task_repaired (from.getTo (), n); }});
    nh.aut ("task_repaired", new Not.NHandler<Tier> () { // NACK!
    	protected void a (final Tier to, final Not unresp, final ISet missing)
	  throws TransientException {
	  to.task_repaired_timeout (unresp, missing); }});
    nh.put ("repair_task", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
    	  throws TransientException { to.repair_task (); }});
    nh.put ("forget_task", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
    	  throws TransientException { to.forget_task (); }});

    // ---

    nh.put ("missing_heartbeats", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
    	  throws TransientException { to.missing_heartbeats (n); }});
    nh.put ("falsely_suspected_failure", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException {
	  to.falsely_suspected_failure (from.getTo (), n); }});
    nh.aut ("falsely_suspected_failure", new Not.NHandler<Tier> () { // NACK!
    	protected void a (final Tier to, final Not unresp, final ISet missing)
	  throws TransientException {
	  to.plausible_failure (unresp, missing); }});
    nh.put ("repair_node", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException { to.repair_node (); }});
    nh.put ("forget_node", new Not.Handler<Tier> () {
    	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException { to.forget_node (); }});
    nh.put ("node_repaired_delayed_not", new Not.Handler<Tier> () {
	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException { to.node_repaired_delayed_not (); }});

    // ---

    nh.put ("configure_tier_as_source", new Not.Handler<Tier> () {
	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException {
	  assert (from.getTo () == 0); // local
	  to.configure_nodes (n); }});
    nh.put ("source_notified_about_node_shutdown", new Not.Handler<Tier> () {
	protected void p (final AgentId from, final Tier to, final Not n)
	  throws TransientException {
	  assert (from.getTo () == 0); // local
	  to.source_notified_about_node_shutdown (n); }});
    nh.aut ("source_notified_about_node_shutdown", new Not.NHandler<Tier> () {
	protected void a (final Tier to, final Not unresp, final ISet missing)
	  throws TransientException {
	  to.source_notified_about_node_shutdown_timeout (unresp, missing); }});

    // ---

    nh.put ("nodes_configured", new Not.Handler<Tier> () {
	protected void p (final AgentId from, final Tier to, final Not n)
	  // The notification itself is meaningless
	  throws TransientException {
	  to.nodes_configured (n.maybeCause ()); }});
    nh.aut ("nodes_configured", new Not.NHandler<Tier> () {
	protected void a (final Tier to, final Not unresp, final ISet missing)
	  throws TransientException {
	  to.nodes_configured_timeout (unresp, missing); }});

    // ---

  }

  // ---------------------------------------------------------------------------

  Tier (final String name, final Env env, final DecisionCode<?, ?, ?> dc,
	final Cluster cluster, final Env taskEnv, final Tier source)
    throws IOException {
    super (name);

    this.fixed = true;

    assert (name != null && env != null && dc != null && cluster != null
	    && taskEnv != null);
    this.name = name;
    this.env = env;
    this.source = (source != null) ? source.getId () : null;
    this.cluster = cluster;
    this.taskEnv = taskEnv;
    this.aac = new Not.AckAccountingContext (this.getId ());

    // Note it is really important not to keep a reference to the agent `dc'.
    // It is here for typing reasons only, and communications with it should be
    // done through a3 messaging only.
    this.dc = dc.getId ();

    // Init an internal field.
    env.put ("last_host", "");

    // {
    //   final Agent lm =		// XXX 15s.
    // 	new UnixLoadMuxer (this.name + ".load-info", 15000L,
    // 			   this.getId (), this.model);
    //   this.loadMuxer = lm.getId ();
    //   lm.deploy ();
    // }

    {
      final Long expirationDelay = // defaults to 7s.
	new Long (env.get ("heartbeats-muxer.expiration-delay", "7000"));
      final Long minRefreshPeriod = // defaults to 3s.
	new Long (env.get ("heartbeats-muxer.min-refresh-period", "3000"));
      final LinuxCPUMuxer lm =
	new LinuxCPUMuxer (this.name + ".cpu-info", minRefreshPeriod,
			   expirationDelay, this.getId (), this.dc);
      this.cpuLoadMuxer = lm.getId ();
      lm.deploy ();
    }
  }

  // ---

  protected void agentInitialize (final boolean firstTime) throws Exception {
    super.agentInitialize (firstTime);
    this.prefix = new Ansi.S (name + ": ", C.GREEN);
  }

  // ---

  private void init () {
    DecisionCode.bindOpCode (this.dc, name, this.getId ());
  }

  // ---

  private void start () {
    // // Start load-info muxer.
    // sendTo (this.loadMuxer, new Not ("start"));

    // Start cpu-info muxer.
    sendTo (this.cpuLoadMuxer, new Not ("start"));
  }

  // ---

  protected void deploy_and_start_node ()
    throws TransientException {
    this.deploy_and_start_node (false);
  }

  private void onAddError (boolean repairing) {
    if (repairing) {
      this.node_repairings --;
      sendTo (this.dc, new Measure<Integer> (this.name + ".node_repairings",
					     this.node_repairings));
      sendTo (this.dc, new Impulse (name + ".node_repair_error"));
    } else {
      sendTo (this.dc, new Impulse (name + ".add_error"));
    }
  }

  protected void deploy_and_start_node (boolean repairing)
    throws TransientException {

    final Node n = this.cluster.alloc ();
    if (n == null) {
      info ("Unable to deploy new node: no remaining host in cluster!",
	    C.BOLD, C.RED);
      this.onAddError (repairing);
      return;
    }

    final Remote r;
    try {
      r = new Remote (this.taskEnv, n, this.getId ());
      this.remotes.put (r.id, r);
    } catch (Exception e) {
      logmon.log (BasicLevel.ERROR, "Error when instanciating new node.", e);
      this.onAddError (repairing);
      return;
    }

    info ("Starting deployment of "+ r +"…", C.BOLD, C.CYAN);

    if (repairing) this.repairing.add (r.id);
    this.deploying.add (r.id);

    // That's a hack to use the timing mechanims of Not acknowledgements, but
    // makes us receive the "deployment_started" notification twice if
    // everithing goes ok, or the "deployment_started_timeout" nack otherwise.
    final long timeout = 1000000L;
    final Not ackMe =
      new Not ("deployment_started",
	       Not.e ("remote.id", r.id),
	       Not.e ("repairing", repairing),
	       Not.e ("timeout", timeout))
      .setAck (this.getId (), "deployment_started", timeout);
    r.startDeployment (ackMe);
  }

  // ---

  protected void deployment_started (final Not not)
    throws TransientException {
    final Remote r = r (not);
    info ("Deployment of "+ r +" successfully started.");
  }

  // ---

  protected Remote r (final int rid) {
    return this.remotes.get (rid);
  }

  protected Remote r (final Not n) {
    return r ((Integer) n.getProp ("remote.id"));
  }

  protected void deployment_started_timeout (final Not unresp)
    throws TransientException {
    final Remote r = r (unresp);

    info ("Restarting deployment of "+ r +"…", C.YELLOW);

    final boolean repairing = (Boolean) unresp.getProp ("repairing");
    final long timeout = (Long) unresp.getProp ("timeout") * 2L;
    if (timeout < this.timeout ("deployment.threshold", Long.MAX_VALUE)) {
      final Not ackMe =
	new Not ("deployment_started",
		 Not.e ("remote.id", r.id),
		 Not.e ("repairing", repairing),
		 Not.e ("timeout", timeout))
	.setAck (this.getId (), "deployment_started", timeout);
      r.startDeployment (ackMe);
    } else {
      // Cancel deployment.
      this.onAddError (repairing);
      this.undeploy_node (r, true);
    }
  }

  // ---

  protected void prepareInitNot (final Remote r, final Not init)
    throws TransientException, Node.OutOfPortException { }

  // ---

  protected void deployment_done (final Not not)
    throws TransientException {

    final AgentId agent = (AgentId) not.getProp ("deployment.agent.id");
    final Remote r = this.remotes.get ((int) agent.getTo ());

    info (r +" successfully deployed.");

    r.agent = agent;
    transitToFrom (r.id, this.starting, this.deploying);

    final Not init =
      new Not ("init",
	       // Not.e (this.env, "load-monitor.period", 15000L), // 15s.
	       // Not.e ("load-monitor.dest.id", this.loadMuxer),
	       Not.e (this.env, "cpu-monitor.period", "5000"), // 5s.
	       Not.e (this.env, "cpu-monitor.alpha", "0.5"),
	       Not.e ("cpu-monitor.dest.id", this.cpuLoadMuxer),
	       Not.e (this.env, "task-monitor.period", "5000"), // 5s.
	       Not.e ("task-monitor.dest.id", this.getId ()));
    try {
      this.prepareInitNot (r, init);
      this.prepareConfigNot (init); // Also send configuration information.
      init.setAck (this.getId (), "task_started",
		   this.timeout ("init", 50000L), r.id, this.aac);
      this.sendTo (agent, init);
    } catch (Node.OutOfPortException e) {
      logmon.log (BasicLevel.ERROR, "Error when allocating resources.", e);
      info ("Error when allocating resources.", C.BOLD, C.RED);
      this.releaseNodeData (r);
      this.onAddError (this.repairing.contains (r.id));
      this.repairing.rem (r.id);
      transitToFrom (r.id, this.stopped, this.starting);
      this.kill_node (r);
    }
  }

  // ---

  /**
   * Executed once the task has successfully started on remote <tt>rid</tt>.
   */
  protected void task_started (final int rid, final Not cause)
    throws TransientException {

    final Remote r = r (rid);

    info ("Task started on node "+ r +".");

    // Register the host name of the newly started remote.
    env.put ("last_host", r.node.host);

    // Get custom infos communicated by remote node.
    this.importTaskAsTailItf (cause);

    if (this.starting.contains (rid)) {
      transitToFrom (rid, this.running, this.starting);
      sendTo (this.dc, new Impulse (name + ".add_ack"));
    } else {
      this.node_repaired (r, cause);
    }

    if (this.source != null) {
      // Notify source tier about some configuration data.
      final Not conf = new Not ("configure_tier_as_source");
      this.exportTailTierItf (conf);
      sendTo (this.source, conf);
    }

    cause.maybeAck ();
  }

  // ---

  /**
   * @param unresp is the notification sent by "us" that did not receive an
   * acknowledgement in time.
   */
  protected void task_started_timeout (final Not unresp, final ISet missing)
    throws TransientException {

    for (int rid: missing) {
      info ("Task on "+ r (rid) +" was unable not start!", C.RED);

      this.onAddError (this.repairing.contains (rid));
      this.repairing.rem (rid);

      // XXX Enter first in suspected state?
      this.undeploy_node (r (rid), true);
    }
  }

  // ---------------------------------------------------------------------------

  /**
   * This method is executed right after the effective shutdown of a node.
   */
  protected void releaseNodeData (final Remote r)
    throws TransientException {
    // Nothing to do in the general case…
  }

  // ---------------------------------------------------------------------------

  private void stop () throws TransientException {
    if (! this.deploying.isEmpty ()) {
      info ("Delaying stop operation 'cause deployments are currently being "+
	    "performed.", C.BOLD, C.YELLOW);
      // XXX Non-persistent operation! But this is necessary because we do not
      // acknowledge its completion to the decision code, nor send end-of-stop
      // impulses.
      //
      // We should also raise a flag to avoid starting the remote tasks…
      Not.delayed (this.getId (), "stop", 10000L); // 10s.
      return;
    }

    info ("Sending stop signals to remote agents.");

    for (int i: this.starting)  this.stop_and_undeploy_node (i, true);
    for (int i: this.running)   this.stop_and_undeploy_node (i, true);
    // for (int i: this.stopping); // do nothing.
    // for (int i: this.stopped);	// do nothing.
    // for (int i: this.suspected) this.undeploy_node (r (i));
    // for (int i: this.repairing) this.undeploy_node (r (i));
    // for (int i: this.failed)    this.undeploy_node (r (i));

    // Stop cpu-info muxer.
    sendTo (this.cpuLoadMuxer, new Not ("stop"));
  }

  // ---

  /**
   * Called when an arbitrary node is to be removed, while the tier continues to
   * run (typically, when down-sizing).
   */
  protected int peekRunningRemoteToStop (final boolean _fullstop) {
    // Choose randomly by default.
    return this.running.isEmpty () ? Remote.NONE : this.running.chooseSure ();
  }

  // ---

  private void stop_and_undeploy_node (int rid, final boolean fullstop)
    throws TransientException {

    if (rid == Remote.NONE) {
      // Let's choose one node to shutdown. Do it like that (for now! — it may be
      // a tricky choice in the future).
      if ((rid = this.peekRunningRemoteToStop (fullstop)) == Remote.NONE) {
	// XXXXXX ???
	info ("Asked to stop a node, but none is a valid candidate!",
	      C.BOLD, C.RED);
	return;
      }
    } else {
      // XXX may also be in deploying?…
      assert (this.running.contains (rid));
    }

    this.stop_node (rid);
  }

  private void stop_and_undeploy_node (final int rid)
    throws TransientException {
    this.stop_and_undeploy_node (rid, false);
  }

  private void stop_and_undeploy_node ()
    throws TransientException {
    this.stop_and_undeploy_node (Remote.NONE);
  }

  // ---

  protected void really_stop_node (final Remote r)
    throws TransientException {
    final Not stop = new Not ("stop")
      .setAck (this.getId (), "task_stopped",
	       this.timeout ("stop", 30000L), r.id, this.aac);
    sendTo (r.agent, stop);
  }

  // ---

  protected void stop_node (final int rid)
    throws TransientException {
    final Remote r = r (rid);

    info ("Stopping task on node "+ r +"…", C.BOLD, C.CYAN);

    transitToFrom (r.id, this.stopping, this.running, this.starting);

    if (this.source != null) {
      // Notify source tier about some configuration data.
      final Not conf =
	new Not ("configure_tier_as_source", Not.e ("remote.id", r.id));
      this.exportTailTierItf (conf);
      conf.setAck (this.getId (), "source_notified_about_node_shutdown",
		   this.timeout ("notify_source", 10000L));
      sendTo (this.source, conf);
    } else {
      this.really_stop_node (r);
    }
  }

  // ---

  private void source_notified_about_node_shutdown (final Not cause)
    throws TransientException {
    final Remote r = r (cause);
    assert (r != null);
    info ("Source tier successfully notified about shutdown of node "+ r +".",
	  C.CYAN);
    this.really_stop_node (r);
  }

  private void source_notified_about_node_shutdown_timeout (final Not unresp,
							    final ISet missing)
    throws TransientException {
    assert (missing == null || missing.isEmpty ());
    info ("Unresponsive source; shutting down anyway!", C.RED);
    this.really_stop_node (r (unresp));
  }

  // ---

  /**
   * Executed once the remote task has successfully stopped its execution.
   */
  protected void task_stopped (final int rid, final Not cause)
    throws TransientException {
    final Remote r = r (rid);
    info ("Task on "+ r +" stopped.");
    transitToFrom (rid, this.stopped, this.stopping);
    this.kill_node (r);
  }

  // ---

  /**
   * @param unresp is the notification sent by "us" that did not receive an
   * acknowledgement in time.
   */
  private void task_stopped_timeout (final Not unresp, final ISet missing)
    throws TransientException {
    for (int rid: missing) {
      // XXX Set it failed? suspect it?
      info ("Task on "+ r (rid) +" seems unable to stop; killing node ayway…",
	    C.RED);
      transitToFrom (rid, this.stopped, this.stopping);
      this.kill_node (r (rid));	// try to kill it anyway…
    }
  }

  // ---

  /**
   * Blindly kills the nodes' task without further notice… `deployment_stops'
   * should be received anyway; in any case, the remote node is undeployed after
   * some time.
   *
   * This should work whatever the state of the remote node, except if the
   * network connection is down…
   */
  protected void kill_node (final Remote r)
    throws TransientException {
    // The acknowledgement here serves to detect (in a best effort way) if the
    // node is still responsive…
    final Not kill = new Not ("real-stop") // 10s.
      .setAck (this.getId (), "deployment_stops",
	       this.timeout ("kill", 20000L), r.id, this.aac);
    sendTo (r.agent, kill);
  }

  // ---

  private void deployment_stops (final int rid, final Not cause)
    throws TransientException {
    final Remote r = r (rid);
    info (r +" stops.");
    this.undeploy_node (r);
  }

  // ---

  private void deployment_stops_timeout (final Not unresp, final ISet missing)
    throws TransientException {
    for (int rid: missing) {
      final Remote r = r (rid);
      info (r +" seems unable to stop!", C.BOLD, C.RED);
      // Try to forcibly undeploy anyway!
      this.undeploy_node (r, true);
    }
  }

  // ---

  private void undeploy_node (final Remote r)
    throws TransientException {
    this.undeploy_node (r, false);
  }

  private void undeploy_node (final Remote r, final boolean failed)
    throws TransientException {

    info ("Undeploying "+ r +"…");

    transitToFrom (r.id, null,	// sink ≈ forget it
		   this.starting,
		   this.running, // in case of failure.
		   this.stopped);
    r.agent = null;
    this.releaseNodeData (r);

    // Do this now, so that we are certain the node is available again (even if
    // some of its ports are not) when the current call terminates:
    this.cluster.release (r.node, failed);

    // That's a hack to use the timing mechanims of Not acknowledgements, but
    // makes us receive the "undeployment_done" notification twice if everithing
    // goes ok, or the "undeployment_done_timeout" nack otherwise.
    final long timeout = 1000000L;
    final Not ackMe =
      new Not ("undeployment_done",
	       Not.e ("remote.id", r.id),
	       Not.e ("timeout", timeout))
      .setAck (this.getId (), "undeployment_done", timeout);
    r.startUndeployment (ackMe);

    // stop awaiting for further acks, in case.
    this.aac.forget (r.id);
  }

  private void undeployment_done (final Not not)
    throws TransientException {
    final Remote r = r (not);
    assert (r != null);
    info (r +" seems undeployed.", C.BOLD, C.CYAN);
    this.remotes.remove (r.id);
    r.destroy ();
  }

  // ---

  private void undeployment_done_timeout (final Not unresp)
    throws TransientException {

    final Remote r = r (unresp);
    assert (r != null);

    info ("Restarting undeployment of "+ r +"…", C.YELLOW);

    final long timeout = (Long) unresp.getProp ("timeout") * 2L;
    if (timeout < this.timeout ("undeployment.threshold", Long.MAX_VALUE)) {
      final Not ackMe =
	new Not ("undeployment_done",
		 Not.e ("remote.id", r.id),
		 Not.e ("timeout", timeout))
	.setAck (this.getId (), "undeployment_done", timeout); // * 10 (?)
      r.startUndeployment (ackMe);
    } else {
      // If undeployment constantly fails, then there is most certainly a
      // bug somewhere…
      info ("Forgeting "+ r +"", C.BOLD, C.RED);

      this.remotes.remove (r.id);
      r.destroy ();
    }
  }

  // ---------------------------------------------------------------------------

  // identifiers in this set should also be in one of `running', …
  private final ISet.Ref taskFailures = new ISet.Ref (ISet.empty);
  private int task_repairings = 0;

  /**
   * Sent by the Unix _task monitor_. Once it has been received, it is
   * certain that the remote task has stopped.
   */
  private void task_failure (final int rid, final Not not)
    throws TransientException {
    final Remote r = r (rid);
    info ("Task failure on "+ r +".", C.BOLD, C.RED);

    // We just have to remember what node is concerned, and to notify the model
    // about the task failure.
    this.taskFailures.add (rid);
    sendTo (this.dc, new Impulse (this.name + ".task_failure"));
  }

  // ---

  private void repair_task ()
    throws TransientException {

    try {
      final int rid = this.taskFailures.poll ();
      final Remote r = r (rid);

      info ("Trying to repair task on "+ r +".", C.BOLD, C.YELLOW);

      this.task_repairings ++;
      sendTo (this.dc, new Measure<Integer> (this.name + ".task_repairings",
					     this.task_repairings));

      final Not restart = new Not ("restart")
	.setAck (this.getId (), "task_repaired",
		 this.timeout ("restart", 20000L), r.id, this.aac);

      sendTo (r.agent, restart);
    } catch (ISet.EmptySet e) {
      // XXX nothing to do?
    }
  }

  // ---

  private void task_repaired (final int rid, final Not cause)
    throws TransientException {

    final Remote r = r (rid);
    info ("Task on "+ r +" successfully restarted.");

    this.task_repairings --;
    sendTo (this.dc, new Measure<Integer> (this.name + ".task_repairings",
					   this.task_repairings));
    sendTo (this.dc, new Impulse (name + ".task_repair_ack"));
    task_repairing_cont ();
  }

  // ---

  private void task_repaired_timeout (final Not unresp, final ISet missing)
    throws TransientException {

    assert (missing.card () == 1);
    final Remote r = r (missing.chooseSure ());


    info ("Task on "+ r +" seems unable to restart!", C.BOLD, C.RED);

    // Immediate?
    this.task_repairings --;
    sendTo (this.dc, new Measure<Integer> (this.name + ".task_repairings",
					   this.task_repairings));
    sendTo (this.dc, new Impulse (name + ".task_repair_error"));
    this.kill_node (r);
    task_repairing_cont ();
  }

  // ---

  protected void forget_task ()
    throws TransientException {

    final Remote r = r (this.taskFailures.pollSure ());
    info ("Not repairing task on "+ r +".", C.BOLD, C.YELLOW);
    this.kill_node (r);

    // If there still are failed tasks, we send "task_failure" impulses…
    task_repairing_cont ();
  }

  // ---

  private void task_repairing_cont () {
    if (this.taskFailures.isEmpty ()) return;
    sendTo (this.dc, new Impulse (this.name + ".task_failure"));
  }

  // ---------------------------------------------------------------------------

  private int node_repairings = 0;

  /**
   * Sent by the Linux CPU muxer_.
   */
  private void missing_heartbeats (final Not not)
    throws TransientException {

    final ISet.Ref forget = new ISet.Ref (ISet.empty);

    for (int rid: (ISet) not.getProp ("ids")) {

      if (! this.running.contains (rid)) {
	forget.add (rid);
  	// hum?
  	continue;
      }

      // What could we do to be sure? A second suspicion notification?:
      if (this.suspected.contains (rid)) {
	// Apparently, very bad situations of failure…
	info ("Failure of node nº "+ rid +" suspected by heartbeat muxer "+
	      "(twice in a row!).", C.BOLD, C.RED);

	on_failure (rid);
	continue;
      }

      // Not sure this is a real situation of failure…
      info ("Failure of node nº "+ rid +" suspected by heartbeat muxer.",
	    C.RED);

      // This will surely become an impulse, to be sure…!
      this.suspected.add (rid);
      sendTo (this.dc,
	      new Measure<Integer> (this.name +".suspected_node_failures",
				    this.suspected.card ()));

      // To check once again, let's play some ping-pong… (this is actually a
      // double check), in parallel with the heartbeat muxer.
      sendTo (r (rid).agent,
	      new Not ("ping")
	      .setAck (this.getId (), "falsely_suspected_failure",
		       this.timeout ("failure_suspicion", 5000L),
		       rid, this.aac));
    }

    if (! forget.isEmpty ()) {
      sendTo (this.cpuLoadMuxer,
	      new Not ("forget", Not.e ("ids", forget.cell ())));
    }
  }

  // ---

  private void falsely_suspected_failure (final int rid,
					  final Not cause)
    throws TransientException {

    // Hum… apparently I was wrong.
    if (! this.suspected.contains (rid)) // too late…
      return;

    info (r (rid) +" seems alive anyway…", C.YELLOW);

    this.suspected.rem (rid);
    sendTo (this.dc,
	    new Measure<Integer> (this.name +".suspected_node_failures",
				  this.suspected.card ()));

    sendTo (this.cpuLoadMuxer,	// XXX ???
	    new Not ("forget", Not.e ("ids", ISet.singleton (rid))));
  }

  // ---

  private void plausible_failure (final Not unresp, final ISet missing)
    throws TransientException {

    // The node may have already been forgotten in the acknowledgement
    // accounting context (e.g., if the heartbeats muxer overruns the ping-pong:
    // in such a case `on_failure' was called first in `missing_heartbeats').
    if (missing.card () >= 1) {
      assert (missing.card () == 1);
      final int rid = missing.chooseSure ();

      // Note rid may be null if the remote node has been unregistered meanwhile.
      if (this.suspected.contains (rid)) {
	info ("Failure of "+ r (rid) +", detected by ping.", C.BOLD, C.RED);
	on_failure (rid);
      }

      // Otherwise, the _load muxer_ has reported the loss of heartbeat a second
      // time.
    }
  }

  // ---

  protected void on_failure (final int rid)
    throws TransientException {

    // Radical stuff, because the remote node is apparently not sending its
    // heartbeats or not respondig…
    this.suspected.rem (rid);
    sendTo (this.dc,
	    new Measure<Integer> (this.name +".suspected_node_failures",
				  this.suspected.card ()));

    this.notifyFailure (rid);
    sendTo (this.cpuLoadMuxer,
	    new Not ("forget", Not.e ("ids", ISet.singleton (rid))));

    this.undeploy_node (r (rid), true);
  }

  // ---

  protected void notifyFailure (final int rid) {
    sendTo (this.dc, new Impulse (this.name + ".node_failure"));
  }

  // ---

  private void forget_node () {
    info ("Not recovering from last failure.");
  }

  // ---

  /**
   * Triggers the repairing of a node; by default, this basically means choosing
   * a node from the cluster and starting it.
   *
   * <p>Indirectly triggers the execution of {@link #node_repaired(Remote, Not)}.
   */
  protected void repair_node ()
    throws TransientException {

    // Maybe there could be some configuration to recover (hence to remember in
    // `on_failure')… if there is a master for instance…
    info ("About to repair a failed node…", C.BOLD, C.YELLOW);

    this.node_repairings ++;
    sendTo (this.dc, new Measure<Integer> (this.name + ".node_repairings",
					   this.node_repairings));

    // By default, repairing boils down to choose and start a new node.
    this.deploy_and_start_node (true);
  }

  // ---

  // XXX That's more a "replacement" than a "repair"…
  protected void node_repaired (final Remote r, final Not cause) {
    info ("Failed node successfully replaced by "+ r, C.BOLD, C.YELLOW);

    this.repairing.rem (r.id);

    // Delay the lowering of this measure by a configurable amount of time…
    //
    // XXX Take care it's not robust to failures of the center!
    Not.delayed (this.getId (), "node_repaired_delayed_not",
		 Long.parseLong (env.get ("node-repairing.extra-delay",
					  "10000"))); // 10s
  }

  // ---

  private void node_repaired_delayed_not () {
    this.node_repairings --;
    sendTo (this.dc, new Measure<Integer> (this.name + ".node_repairings",
					   this.node_repairings));
    sendTo (this.dc, new Impulse (name + ".note_repair_ack"));
  }

  // ---------------------------------------------------------------------------

  /**
   * Prepares the given notification (named "configure_node"), that will be sent
   * to all nodes belonging to this tier.
   */
  protected void prepareConfigNot (final Not conf)
    throws TransientException {
    // Fill custom infos to communicate to remote nodes.
    this.env.exportFieldsGroup ("tail", conf);
  }

  /**
   * Triggers the configuration of all nodes belonging to this
   * tier. Acknowledges the given cause once all nodes have been configured
   * successfully.
   *
   * <p>Associated to notification <b>configure_tier_as_source</b> sent by tail
   * tier.
   *
   * <p>If the tier is running (<i>i.e.</i> some nodes are running), then this
   * method indirectly triggers one execution of either {@link
   * #nodes_configured(Not)} or {@link #nodes_configured_timeout(Not, ISet)},
   * respectively indicating full success or at least one failure.
   */
  protected void configure_nodes (final Not cause)
    throws TransientException {

    // Retrieve custom infos communicated by tail tier.
    this.env.importFieldsGroup ("tail", "as_tail", cause);

    final ISet.Ref dests = ISet.Ref.empty ();
    for (int id : this.running)   dests.add (id);
    // for (int id : this.suspected) dests.add (id); // in case they recover…
    // for (int id : this.repairing) dests.add (id); // XXX (see below)

    if (dests.isEmpty ()) {
      info ("No node to configure yet.");
      // Nothing to do, so acknowledge immediately.
      cause.maybeAck ();
      return;
    }

    info ("Reconfiguring "+ dests.card () +" remote(s)…");

    final Not reconf = new Not ("configure_node", cause);
    this.prepareConfigNot (reconf);
    reconf.setAck (this.getId (), "nodes_configured",
		   this.timeout ("configure_node", 10000L), dests.cell (), true,
		   this.aac);

    for (int rid: dests) sendTo (r (rid).agent, reconf);
  }

  // ---

  /**
   * Executed once all nodes have been successfully configured.
   */
  protected void nodes_configured (final Not responded) {
    info ("All remotes successfully configured.", C.CYAN);

    // Argument of the current method call is the notification we sent to the
    // nodes `configure_node'; we can now acknowledge its prior cause.
    responded.maybeCause ().maybeAck ();
  }

  // ---

  /**
   * Executed iff some nodes has not acknowledged their new configuration.
   */
  protected void nodes_configured_timeout (final Not unresp,
					   final ISet missing) {
    // Note `missing' could be empty in case one node has been undeployed since
    // we sent the `configure' signal.
    info (missing.card () +" missing acknowledgement(s)!", C.RED);

    // TODO Maybe suspect them…

    // Acknowledge its prior cause anyway.
    unresp.maybeCause ().maybeAck ();
  }

  // ---------------------------------------------------------------------------

  /**
   * Fills the environment with all data that need to be imported from the
   * remote task.
   *
   * <p>Typically, this method imports the `as_tail' fields group from the given
   * notification into this tier's environment.
   */
  protected void importTaskAsTailItf (final Not not) {
    this.env.importFieldsGroup ("as_tail", not);
  }

  // ---

  /**
   * Fills the given notification with all data that need to be exported to the
   * source tier.
   *
   * <p>Typically, this method exports the `as_tail' fields group from this
   * tier's environment into the notification.
   */
  protected void exportTailTierItf (final Not not) {
    // Fill custom infos to communicate to source tier.
    this.env.exportFieldsGroup ("as_tail", not);
  }

  // ---------------------------------------------------------------------------

  public void react (final AgentId from, final Notification n)
    throws Exception {
    try {
      // Any Not that got here should be "handleable"…
      if (nh.maybeHandle (from, this, n, true, this.aac) == Not.Status.HANDLED)
	return;
      else
	super.react (from, n);
    } catch (Not.TimedOutException e) {
      info ("Acknowledgement `"+ n +"' arrived too late… ignoring.", C.YELLOW);
      e.printStackTrace (System.err);
    }
  }

  // ---

  protected long timeout (final String k, final long d) {
    final String s = this.env.get ("timeout." + k, null);
    return (s != null) ? Long.parseLong (s) : d;
  }

  // ---

  // "Most concrete sub-class" defines log name.
  protected final String getLogTopic () {
    return this.getClass ().getName ();
  }

  // ---

}
