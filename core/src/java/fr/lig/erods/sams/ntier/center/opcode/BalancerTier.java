/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.center.opcode;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fr.dyade.aaa.agent.Agent;
import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Notification;
import fr.lig.erods.sams.center.DecisionCode;
import fr.lig.erods.sams.center.Remote;
import fr.lig.erods.sams.common.Env;
import fr.lig.erods.sams.common.Node;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.util.Ansi.C;
import fr.lig.erods.sams.common.util.TransientException;
import fr.lig.erods.sams.ntier.center.Cluster;
import fr.lig.erods.sams.ntier.common.Worker;

/**
 * A balancer tier is the source of a replicated one. It receives information
 * about new or removed workers by means of notifications that precede a generic
 * `configuration' notification (handled in the super-class).
 *
 * @param <W> the type of workers
 */
public abstract class BalancerTier<W extends Worker> extends Tier {

  // ---

  final protected Map<Node, W> workers = new HashMap<Node, W> ();

  // ---

  protected BalancerTier (final String name, final Env env,
			  final DecisionCode<?, ?, ?> dc, final Cluster cluster,
			  final Env taskEnv, final Tier source)
    throws IOException {
    super (name, env, dc, cluster, taskEnv, source);
  }

  // ---------------------------------------------------------------------------

  private static final Not.Handlers<BalancerTier<?>> nh =
    new Not.Handlers<BalancerTier<?>> ();
  static {

    nh.put ("new_worker", new Not.Handler<BalancerTier<?>> () {
	protected void p (final AgentId from, final BalancerTier<?> to, final Not n)
	  throws TransientException {
	  assert (from.getTo () == 0); // local
	  to.new_worker (n);
	}});
    nh.put ("rem_worker", new Not.Handler<BalancerTier<?>> () {
	protected void p (final AgentId from, final BalancerTier<?> to, final Not n)
	  throws TransientException {
	  assert (from.getTo () == 0); // local
	  to.rem_worker (n);
	}});

  }

  // ---

  protected void new_worker (final Not not)
    throws TransientException {
    final Node n = (Node) not.getProp ("worker.node.id");
    assert (n != null);

    final W w = this.makeWorker (n, not);
    info ("New worker notification received: "+ w, C.CYAN);

    this.workers.put (n, w);

    // XXX Force asynchronous configuration… bypassing the model (for now?)
    // this.configure_nodes (not);
    not.maybeAck ();
  }

  // ---

  protected void rem_worker (final Not not)
    throws TransientException {
    final Node n = (Node) not.getProp ("worker.node.id");
    assert (n != null);

    info ("Obsolete worker notification received: "+ this.workers.get (n),
	  C.CYAN);

    this.workers.remove (n);

    // XXX Force asynchronous configuration… bypassing the model (for now?)
    // this.configure_nodes (cause);
    not.maybeAck ();
  }

  // ---------------------------------------------------------------------------

  protected void prepareConfigNot (final Not conf)
    throws TransientException {
    super.prepareConfigNot (conf);
    conf.setProp ("workers.ids", this.makeWorkersSet ());
  }

  // ---

  protected void prepareInitNot (final Remote r, final Not init)
    throws TransientException, Node.OutOfPortException {
    super.prepareInitNot (r, init);
    init.setProp ("workers.ids", this.makeWorkersSet ());
  }

  // ---

  protected abstract W makeWorker (final Node node, final Not not)
    throws TransientException;

  // ---

  protected abstract Worker.Set makeWorkersSet ();

  // ---

  public void react (final AgentId from, final Notification n)
    throws Exception {
    if (nh.maybeHandle (from, this, n, false) == Not.Status.HANDLED) return;
    else super.react (from, n);
  }

  // ---
}
