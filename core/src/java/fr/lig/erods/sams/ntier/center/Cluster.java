/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.center;

import java.io.Serializable;
import java.util.Queue;
import java.util.LinkedList;

import fr.dyade.aaa.common.Debug;
import fr.lig.erods.sams.common.Env;
import fr.lig.erods.sams.common.Node;
import fr.lig.erods.sams.common.util.TransientException;
import org.apache.commons.lang3.text.StrTokenizer;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

public class Cluster implements Serializable {

  // ---

  private static final Logger logmon =
    Debug.getLogger (Cluster.class.getName ());

  // ---

  private final Queue<Node> availableNodes = new LinkedList<Node> ();
  private final boolean reuseFailedNodes;

  // ---

  public Cluster (final String name, final Env env)
    throws TransientException, Node.InvalidSpecifierException {

    // try {
    if (env.containsKey (name +".nodes")) {
      for (final String n: new StrTokenizer (env.get (name +".nodes"))
	     .getTokenList ()) {
	// Instanciate nodes here 'cause they come from a file.
	this.availableNodes.offer (new Node (n));
      }
    }

    this.reuseFailedNodes =
      "true".equals (env.get (name +".reuse-failed-nodes", "false")) ||
      "true".equals (env.get ("reuse-failed-nodes", "false"));

    // } catch (Node.InvalidSpecifierException e) {
    //   for (final Node n: this.availableNodes)
    // 	Node.byId (nid).destroy ();
    //   throw e;
    // }
  }

  // ---

  public Node alloc ()
    throws TransientException {

    if (availableNodes.size () == 0) {
      logmon.log (BasicLevel.INFO,
		  "Trying to peek a node from an empty cluster.");
      return null;
    }
    final Node n = availableNodes.remove ();

    if (logmon.isLoggable (BasicLevel.DEBUG))
      logmon.log (BasicLevel.DEBUG, "Allocating node "+ n +".");

    return n;
  }

  // ---

  public void release (final Node n, final boolean failed)
    throws TransientException {

    if (logmon.isLoggable (BasicLevel.DEBUG) && failed)
      logmon.log (BasicLevel.DEBUG, "Releasing failed node "+ n +".");
    else if (logmon.isLoggable (BasicLevel.DEBUG))
      logmon.log (BasicLevel.DEBUG, "Releasing node "+ n +".");

    if (! failed || this.reuseFailedNodes)
      availableNodes.offer (n);
  }

  // ---

}
