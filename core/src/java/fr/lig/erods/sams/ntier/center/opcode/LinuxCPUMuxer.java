/*
 * This file is part of sams-core.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.center.opcode;

import java.io.Serializable;

import fr.dyade.aaa.agent.AgentId;
import fr.lig.erods.sams.common.opcode.HBMuxer;
import org.objectweb.util.monolog.api.BasicLevel;

public final class LinuxCPUMuxer extends HBMuxer<Float, LinuxCPUMuxer.O> {

  // ---

  /**
   * Output data type.
   */
  public static final class O implements Serializable {
    public final int alive_nodes, cpu_percentage_avg;
    private O (int alive_nodes, int percentage) {
      this.alive_nodes = alive_nodes;
      this.cpu_percentage_avg = percentage;
    }
  }

  // ---

  public LinuxCPUMuxer (final String name, final long minRefreshPeriod,
			final long expiry, final AgentId expirDest,
			final AgentId... dests) {
    super (new Float (0.0), name, minRefreshPeriod, expiry, expirDest, dests);
  }

  // ---

  protected O mux () {
    if (logmon.isLoggable (BasicLevel.DEBUG))
      logmon.log (BasicLevel.DEBUG, "Updating output value of `"+
		  this.getName () +"'.");

    // Mean computation for values that are sufficiently fresh.
    double acc = 0.0;
    int n = 0;
    for (final Entry<Float> e: this.values ()) {
      acc += e.value.doubleValue ();
      n   += 1;
    }
    acc /= (double) n;
    final int avg = (int) Math.floor (acc);

    return new O (n, avg);
  }

  // ---

}
