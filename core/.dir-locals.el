((nil . ((eval . (set (make-local-variable 'my-project-path)
                      (file-name-directory
                       (let ((d (dir-locals-find-file ".")))
                         (if (stringp d) d (car d))))))
	 (eval . (message "Project directory set to `%s'." my-project-path))
	 (eval . (set 'compile-command (format "cd %s && ANT_ARGS= ant -emacs" my-project-path)))
	 (my-license . "This file is part of sams-core.

Copyright Grenoble University, (2013)

Author: Nicolas Berthier <nicolas.berthier@inria.fr>

This software is a computer program whose purpose is to autonomously manage
computing systems using reactive control techniques.

This software is governed by the CeCILL license under French law and abiding
by the rules of distribution of free software.  You can use, modify and/ or
redistribute the software under the terms of the CeCILL license as circulated
by CEA, CNRS and INRIA at the following URL \"http://www.cecill.info\".

As a counterpart to the access to the source code and rights to copy, modify
and redistribute granted by the license, users are provided only with a
limited warranty and the software's author, the holder of the economic
rights, and the successive licensors have only limited liability.

In this respect, the user's attention is drawn to the risks associated with
loading, using, modifying and/or developing or reproducing the software by
the user in light of its specific status of free software, that may mean that
it is complicated to manipulate, and that also therefore means that it is
reserved for developers and experienced professionals having in-depth
computer knowledge. Users are therefore encouraged to load and test the
software's suitability as regards their requirements in conditions enabling
the security of their systems and/or data to be ensured and, more generally,
to use and operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
")
	 (fill-column . 80)))
 (java-mode . ((eval . (defun my-make-header ()
			 (interactive)
			 (save-excursion
			   (let ((comment-start "/* ")
				 (comment-end   " */")
				 (comment-style 'extra-line))
			     (beginning-of-buffer)
			     (insert my-license)
			     (comment-region (point-min) (point)))))))))
