<?xml version="1.0" encoding="UTF-8"?>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
This file is part of sams multi-tier application management examples.

Copyright Grenoble University, (2013)

Author: Nicolas Berthier <nicolas.berthier@inria.fr>

This software is a computer program whose purpose is to autonomously manage
computing systems using reactive control techniques.

This software is governed by the CeCILL license under French law and abiding
by the rules of distribution of free software.  You can use, modify and/ or
redistribute the software under the terms of the CeCILL license as circulated
by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy, modify
and redistribute granted by the license, users are provided only with a
limited warranty and the software's author, the holder of the economic
rights, and the successive licensors have only limited liability.

In this respect, the user's attention is drawn to the risks associated with
loading, using, modifying and/or developing or reproducing the software by
the user in light of its specific status of free software, that may mean that
it is complicated to manipulate, and that also therefore means that it is
reserved for developers and experienced professionals having in-depth
computer knowledge. Users are therefore encouraged to load and test the
software's suitability as regards their requirements in conditions enabling
the security of their systems and/or data to be ensured and, more generally,
to use and operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->
<project basedir="." default="dist" name="sqltiers">

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- general properties... +++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <property file="build.properties"/>

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- import common build file ++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <import file="${common.etc}/build/common.xml"/>

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- tasks targets... ++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <!-- optional tasks properties -->
  <property file="${basedir}/etc/tasks.properties"/>
  <include file="${tasks.dir}/build.xml" as="tasks"/>

  <target name="tasks.init" depends="tasks.mproxy.init,
				     tasks.mysql.init"/>

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- targets... ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <path id="argos.path">
    <pathelement path="${common.src}/argos"/>
    <pathelement path="${src.dir}/argos"/>
  </path>
  <union id="argos-inputs">
    <fileset dir="${common.src}/argos"
	     includes="repair.argos, startup2.argos,
		       tier_model.argos, tier_model_repl.argos"/>
    <fileset-opt dir="${src.dir}/argos" includes="*.argos"/>
  </union>

  <!-- Note  order matters  in the  definition of these  resources… there  is an
       unexplicited  dependency relation  between  files in  src  and in  common
       directories: files in the latter MUST  NOT depend on files in the former,
       at  risk of  needing  to  define other  source  subgroups (hence  further
       confusing the build process). -->
  <path id="ept.path">
    <pathelement path="${common.src}/ept"/>
    <pathelement path="${src.dir}/ept"/>
  </path>
  <union id="ept-to-preprocess"/> <!--nothing to preprocess only-->
  <union id="ept-to-synth"/>	  <!--nothing to synth only-->
  <union id="ept-to-preprocess-n-synth">
    <fileset dir="${common.src}/ept" includes="br_tiers_ctrld.ept.pp"/>
  </union>
  <union id="ept-to-compile">
    <fileset dir="${common.src}/ept" includes="include/tier_itf.ept"/>
    <fileset-opt dir="${src.dir}/ept" includes="tick.ept"/>
  </union>

  <!-- + + -->

  <path id="java.path">
    <pathelement path="${src.dir}/java"/>
    <pathelement path="${common.src}/java"/>
  </path>

  <!-- + + -->

  <import file="${common.etc}/build/hept-jna-project.xml" as="sub"/>
  <target name="example.doc"       depends="sub.example.doc"/>
  <target name="example.whole.doc" depends="sub.whole.doc" extensionOf="doc"/>

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- center jar... +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <property name="center.jar" value="${project.name}-center.jar"/>

  <target name="center.jar"  extensionOf="jars">
    <jar-center destfile="${build}/${center.jar}">
      <fileset dir="${project.classes}">
	<include name="fr/lig/erods/sams/ntier/center/*"/>
	<exclude name="fr/lig/erods/sams/ntier/center/BRBRTiersTick*"/>
	<include name="fr/lig/erods/sams/ntier/center/opcode/Mproxy*"/>
	<include name="fr/lig/erods/sams/ntier/center/opcode/Mysql*"/>
	<include name="fr/lig/erods/sams/ntier/common/Mproxy*"/>
	<include name="fr/lig/erods/sams/ntier/common/Mysql*"/>
      </fileset>
    </jar-center>
  </target>

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- mproxy-depl target... +++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <target name="mproxy-depl.jar" extensionOf="jars">
    <jar-depl destfile="${build}/mproxy-depl.jar">
      <fileset dir="${project.classes}">
	<include name="fr/lig/erods/sams/ntier/depl/opcode/Mproxy*"/>
	<include name="fr/lig/erods/sams/ntier/common/Mproxy*"/>
      </fileset>
    </jar-depl>
  </target>

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- mysql-depl target... ++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <target name="mysql-depl.jar" extensionOf="jars">
    <jar-depl destfile="${build}/mysql-depl.jar">
      <fileset dir="${project.classes}">
	<include name="fr/lig/erods/sams/ntier/depl/opcode/Mysql*"/>
	<include name="fr/lig/erods/sams/ntier/common/Mysql*"/>
      </fileset>
    </jar-depl>
  </target>

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- gathering depl targets... +++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <target name="app-depl.jars" depends="mproxy-depl.jar,
					mysql-depl.jar"/>

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- outputs +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <!-- Application-specific directories -->

  <property name="app.name" value="${project.name}-app"/>

  <presetdef name="app">
    <sams-app>
      <center-resources>
	<fileset file="${build}/${center.jar}"/>
	<resources refid="jna.classpath"/>
      </center-resources>
      <lib-resources>
	<!-- TODO arch-specific build; center must have the same ABI as the
	     build host for now! -->
	<fileset file="${project.lib}"/>
      </lib-resources>
      <depl-jars>
	<fileset file="${build}/mproxy-depl.jar"/>
	<fileset file="${build}/mysql-depl.jar"/>
      </depl-jars>
      <center-properties>
	<fileset dir="${common.src}/etc">
	  <include name="tasks.properties"/>
	  <include name="tiers/mproxy.properties"/>
	  <include name="tiers/mysql.properties"/>
	  <include name="tiers.properties"/>
	</fileset>
	<fileset dir="${etc.dir}" includes="env.properties"/>
      </center-properties>
      <remote-properties>
	<fileset dir="${common.src}/remote/etc">
	  <include name="mproxy.properties"/>
	  <include name="mysql.properties"/>
	</fileset>
      </remote-properties>
      <remote-executables>
	<fileset dir="${common.src}/remote/bin">
	  <include name="mproxy-cmd"/>
	  <include name="mysql-cmd"/>
	</fileset>
      </remote-executables>
    </sams-app>
  </presetdef>

  <target name="app.dir" depends="tasks.init" extensionOf="dist">
    <app name="${app.name}">
      <app-archives>
	<fileset file="${build.tasks}/mproxy-i686.tgz"/>
	<fileset file="${build.tasks}/mproxy-amd64.tgz"/>
	<fileset file="${build.tasks}/mysql-i686.tar"/>
	<fileset file="${build.tasks}/mysql-amd64.tar"/>
      </app-archives>
    </app>
  </target>

  <target name="app.dist" depends="app.dir" extensionOf="dist"
	  unless="${skip-archives}">
    <sams-app-archive name="${app.name}"/>
  </target>

  <!-- Application-specific configuration directories -->

  <presetdef name="cfg">
    <sams-cfg tiers-props-dir="${common.etc}"
	      targets-specs-dir="${common.etc}/target-specs">
      <tier-properties>
	<include name="tiers/mproxy.properties"/>
	<include name="tiers/mysql.properties"/>
      </tier-properties>
      <extra-configs>
	<fileset file="${common.etc}/a3debug.cfg"/>
      </extra-configs>
    </sams-cfg>
  </presetdef>

  <target name="cfg.dir" extensionOf="dist">
    <cfg name="${project.name}-cfg-${target.name}" targets-spec="${target.name}"/>
  </target>

  <target name="cfg.dist" depends="cfg.dir" extensionOf="dist"
	  unless="${skip-archives}">
    <sams-cfg-archive name="${project.name}-cfg-${target.name}"/>
  </target>

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- deploy ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <!-- <import file="${common.etc}/build/redetar.xml"/> -->

  <!-- <property name="vm" value="center"/> -->

  <!-- <target name="sams.dist.deploy" depends="sams.init"> -->
  <!--   <redetar dest="${vm}" archive="${sams.dir}/output/sams.tgz" tarflag="z"/> -->
  <!-- </target> -->

  <!-- <target name="dist.deploy" depends="dist"> -->
  <!--   <redetar archive="${dist.dir}/${app-amd64.name}.tar" dest="${vm}"/> -->
  <!--   <redetar archive="${dist.dir}/${cfg-vcenter.name}.tar" dest="${vm}"/> -->
  <!-- </target> -->

  <!-- <target name="deploy" depends="sams.dist.deploy, dist.deploy"/> -->

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

</project>
