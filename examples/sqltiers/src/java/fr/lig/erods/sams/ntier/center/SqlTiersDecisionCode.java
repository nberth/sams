/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.center;

import fr.dyade.aaa.agent.AgentId;
import org.objectweb.util.monolog.api.BasicLevel;

public final class SqlTiersDecisionCode
  extends
  NtierDecisionCode<BRTiersTick, BRTiersTick.Input, BRTiersTick.Output> {

  // ---

  private AgentId mproxy;
  private AgentId mysql;

  // ---

  public SqlTiersDecisionCode () {
    super (new BRTiersTick ("sqltiers"), new BRTiersTick.Input ());
    this.iv.t1_notifs = this.defaultNotifs (); // mproxy
    this.iv.t1_config = this.makeConfig (1, 1);
    this.iv.t2_notifs = this.defaultNotifs ();  // mysql
    this.iv.t2_config = this.makeConfig (1, 3); // static csts for now
    this.perTierInit ("mproxy", this.iv.t1_notifs);
    this.perTierInit ("mysql",  this.iv.t2_notifs);
  }

  // ---

  public void registerOpCode (final String name, final AgentId aid) {
    logmon.log (BasicLevel.INFO, "Registering opcode `"+ name +"'.");
    if ("mproxy".equals (name)) { mproxy = aid; }
    if ("mysql".equals  (name)) { mysql  = aid; }
  }

  // ---

  protected final void interpretOutputs (final BRTiersTick.Output o) {

    if (o.t1_repair_inhibited != 0)
      System.err.println ("Repair (mproxy) has been inhibited.");
    if (o.t2_repair_inhibited != 0)
      System.err.println ("Repair (mysql) has been inhibited.");
    if (o.t2_sizing_inhibited != 0)
      System.err.println ("Sizing (mysql) has been inhibited.");

    // Notify tiers' operating code objects about infos and commands.
    notifyTier (mproxy, o.t1_infos, o.t1_cmds);
    notifyTier (mysql,  o.t2_infos, o.t2_cmds);

    // Publish some of the relevant outputs (apparently, we cannot properly put
    // this code in NtierDecisionCode, due to ugly constraints on structures
    // understandable by JNA).
    notifyListener (o.started, "started");
  }

  // ---

}
