/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.center;

import java.util.Map;

import fr.lig.erods.sams.center.ModelBuilder;
import fr.lig.erods.sams.common.Env;
import fr.lig.erods.sams.common.Sams;
import fr.lig.erods.sams.ntier.center.opcode.ApacheTier;
import fr.lig.erods.sams.ntier.center.opcode.TomcatTier;

public class ATModelBuilder implements ModelBuilder {

  public ModelBuilder.Result build (final String... args) throws Exception {
    final String prefixes [] = new String [] { "apache", "tomcat" };

    final ATDecisionCode dc = new ATDecisionCode ();

    final Env clustersEnv = new Env ("clusters.properties", Sams.CFG_DIR);
    final Cluster apacheCluster = new Cluster ("apache", clustersEnv);
    final Cluster tomcatCluster = new Cluster ("tomcat", clustersEnv);

    final Map<String, Env> taskEnvs =
      new Env ("etc/tasks.properties", Sams.APP_DIR)
      .load ("tasks.properties", Sams.CFG_DIR)
      .load ("hosts.properties", Sams.CFG_DIR).selectByPrefixes (prefixes);
    final Env apacheTaskEnv = taskEnvs.get ("apache");
    final Env tomcatTaskEnv = taskEnvs.get ("tomcat");

    final Map<String, Env> tierEnvs =
      new Env ("etc/tiers.properties", Sams.APP_DIR)
      .load ("tiers.properties", Sams.CFG_DIR).selectByPrefixes (prefixes);
    final Env apacheTierEnv = tierEnvs.get ("apache")
      .load ("etc/tiers/apache.properties", Sams.APP_DIR)
      .load ("tiers/apache.properties", Sams.CFG_DIR);
    final Env tomcatTierEnv = tierEnvs.get ("tomcat")
      .load ("etc/tiers/tomcat.properties", Sams.APP_DIR)
      .load ("tiers/tomcat.properties", Sams.CFG_DIR);

    final ApacheTier apacheTier =
      new ApacheTier ("apache", apacheTierEnv, dc, apacheCluster, apacheTaskEnv,
		      null);
    final TomcatTier tomcatTier =
      new TomcatTier ("tomcat", tomcatTierEnv, dc, tomcatCluster, tomcatTaskEnv,
		      apacheTier);

    return new ModelBuilder.Result (dc, apacheTier, tomcatTier);
  }
}
