/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.center;

import fr.dyade.aaa.agent.AgentId;
import org.objectweb.util.monolog.api.BasicLevel;

public final class ATSqltiersDecisionCode
  extends
  NtierDecisionCode<BRBRTiersTick, BRBRTiersTick.Input, BRBRTiersTick.Output> {

  // ---

  private AgentId apache;
  private AgentId tomcat;
  private AgentId mproxy;
  private AgentId mysql;

  // ---

  public ATSqltiersDecisionCode () {
    super (new BRBRTiersTick ("apache-tomcat-sqltiers"),
	   new BRBRTiersTick.Input ());
    this.iv.t1_notifs = this.defaultNotifs (); // apache
    this.iv.t1_config = this.makeConfig (1, 1);
    this.iv.t2_notifs = this.defaultNotifs ();  // tomcat
    this.iv.t2_config = this.makeConfig (1, 3); // static csts for now
    this.iv.t3_notifs = this.defaultNotifs (); // mproxy
    this.iv.t3_config = this.makeConfig (1, 1);
    this.iv.t4_notifs = this.defaultNotifs ();  // mysql
    this.iv.t4_config = this.makeConfig (1, 3);
    this.perTierInit ("apache", this.iv.t1_notifs);
    this.perTierInit ("tomcat", this.iv.t2_notifs);
    this.perTierInit ("mproxy", this.iv.t3_notifs);
    this.perTierInit ("mysql",  this.iv.t4_notifs);
  }

  // ---

  public void registerOpCode (final String name, final AgentId aid) {
    logmon.log (BasicLevel.INFO, "Registering opcode `"+ name +"'.");
    if ("apache".equals (name)) { apache = aid; }
    if ("tomcat".equals (name)) { tomcat = aid; }
    if ("mproxy".equals (name)) { mproxy = aid; }
    if ("mysql".equals  (name)) { mysql  = aid; }
  }

  // ---

  protected final void interpretOutputs (final BRBRTiersTick.Output o) {

    if (o.t1_repair_inhibited != 0)
      System.err.println ("Repair (apache) has been inhibited.");
    if (o.t2_repair_inhibited != 0)
      System.err.println ("Repair (tomcat) has been inhibited.");
    if (o.t2_sizing_inhibited != 0)
      System.err.println ("Sizing (tomcat) has been inhibited.");

    if (o.t3_repair_inhibited != 0)
      System.err.println ("Repair (mproxy) has been inhibited.");
    if (o.t4_repair_inhibited != 0)
      System.err.println ("Repair (mysql) has been inhibited.");
    if (o.t4_sizing_inhibited != 0)
      System.err.println ("Sizing (mysql) has been inhibited.");

    // Notify tiers' operating code objects about infos and commands.
    notifyTier (apache, o.t1_infos, o.t1_cmds);
    notifyTier (tomcat, o.t2_infos, o.t2_cmds);
    notifyTier (mproxy, o.t3_infos, o.t3_cmds);
    notifyTier (mysql,  o.t4_infos, o.t4_cmds);

    // Publish some of the relevant outputs (apparently, we cannot properly put
    // this code in NtierDecisionCode, due to ugly constraints on structures
    // understandable by JNA).
    notifyListener (o.started, "started");
  }

  // ---

}
