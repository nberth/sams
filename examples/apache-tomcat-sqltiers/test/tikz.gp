set terminal lua tikz color solid size 5in,3in
# set terminal lua tikz \
#   solid originreset size 9,6.5
set output "output.tex"
#
load "common.gp"
#
set style line 1 lt 1 lw 2 linecolor rgb "red"
set style line 10 lt 2 linecolor rgb "grey"
#
# set key autotitle column nobox samplen 1 noenhanced
#
