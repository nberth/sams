# set terminal svg rounded size 450,360
# set output 'output.svg'
set termoption dash
unset colorbox
#
set tmargin 0
set bmargin 0
set lmargin 10
set rmargin 3
unset xtics
set yrange [-2 : 102]
set grid
unset key
unset title
#
load "xrange.gp"
