((nil . ((eval . (set (make-local-variable 'my-project-path)
                      (file-name-directory
                       (let ((d (dir-locals-find-file ".")))
                         (if (stringp d) d (car d))))))
	 (eval . (load-file (concat my-project-path "/../common/etc/emacs-licensing.el")))
	 (fill-column . 80))))
