((nil . ((eval . (set (make-local-variable 'my-project-path)
                      (file-name-directory
                       (let ((d (dir-locals-find-file ".")))
                         (if (stringp d) d (car d))))))
	 (eval . (message "Project directory set to `%s'." my-project-path))
	 (eval . (set 'compile-command (format "cd %s && ANT_ARGS= ant -emacs" my-project-path)))
	 (eval . (load-file (concat my-project-path "/../common/etc/emacs-licensing.el")))
	 (fill-column . 80))))
