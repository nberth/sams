#!/bin/bash
# -*- coding: latin-1 -*-
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# This file is part of sams multi-tier application management examples.
#
# Copyright Grenoble University, (2013)
#
# Author: Nicolas Berthier <nicolas.berthier@inria.fr>
#
# This software is a computer program whose purpose is to autonomously manage
# computing systems using reactive control techniques.
#
# This software is governed by the CeCILL license under French law and abiding
# by the rules of distribution of free software.  You can use, modify and/ or
# redistribute the software under the terms of the CeCILL license as circulated
# by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
#
# As a counterpart to the access to the source code and rights to copy, modify
# and redistribute granted by the license, users are provided only with a
# limited warranty and the software's author, the holder of the economic
# rights, and the successive licensors have only limited liability.
#
# In this respect, the user's attention is drawn to the risks associated with
# loading, using, modifying and/or developing or reproducing the software by
# the user in light of its specific status of free software, that may mean that
# it is complicated to manipulate, and that also therefore means that it is
# reserved for developers and experienced professionals having in-depth
# computer knowledge. Users are therefore encouraged to load and test the
# software's suitability as regards their requirements in conditions enabling
# the security of their systems and/or data to be ensured and, more generally,
# to use and operate it in the same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

src="$(readlink -f "$1")"; shift;
# depdest="$(readlink -f "$1")"; shift;
targetdir="$(readlink -f "$1")"; shift;
includes=( "$(dirname "${src}")" "$@" );

file_lookup () {
    local m="$1";
    for a in "${includes[@]}"; do
	if [ -f "${a}/${m}" ]; then
	    echo $(readlink -f "${a}/${m}");
	    break;
	fi;
    done;
}

# 1-level expand...
expand () {
    while read -e line; do
	case "$line" in
	    "include "*)
		# echo ">>>> ${line}";
		line="${line//include /}";
		# echo ">>>> ${line}";
		file="$(file_lookup "${line}")";
		if [ "x${file}" = "x" ]; then
		    echo "File \`${line}' not found!" >/dev/stderr;
		    exit 1;
		fi;
		deps="${deps} ${file}";
		cat "${file}";
		;;
	    *)
		echo "${line}";
		;;
	esac;
    done;
}

base="$(basename "${src}" .ept.pp)";

dest="$(readlink -f "${targetdir}/${base}.ept")";
deps="${src}";
# deps=(
#     $(scan_file_deps "${src}")
# );

expand < "${src}" > "${dest}" || {
    rm -f "${dest}";
    exit 1;
}

# dest="${dests[*]}";
# dest="${dests// /, }";
deps="${deps[*]}";
deps="${deps// /, }";

# > "${depdest}"
cat <<EOF
<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- This file was automatically generated. -->

<project>
  <dependset>
    <targetfilelist dir="." files="${dest}"/>
    <srcfilelist dir="." files="${deps}"/>
  </dependset>
</project>
EOF
