<?xml version="1.0" encoding="UTF-8"?>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
This file is part of Argos.

Copyright Grenoble University, (2013)

Author: Nicolas Berthier <nicolas.berthier@inria.fr>

This software is a computer program whose purpose is to autonomously manage
computing systems using reactive control techniques.

This software is governed by the CeCILL license under French law and abiding
by the rules of distribution of free software.  You can use, modify and/ or
redistribute the software under the terms of the CeCILL license as circulated
by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy, modify
and redistribute granted by the license, users are provided only with a
limited warranty and the software's author, the holder of the economic
rights, and the successive licensors have only limited liability.

In this respect, the user's attention is drawn to the risks associated with
loading, using, modifying and/or developing or reproducing the software by
the user in light of its specific status of free software, that may mean that
it is complicated to manipulate, and that also therefore means that it is
reserved for developers and experienced professionals having in-depth
computer knowledge. Users are therefore encouraged to load and test the
software's suitability as regards their requirements in conditions enabling
the security of their systems and/or data to be ensured and, more generally,
to use and operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->
<project name="argos-build">

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
       +
       + Ant build specification module for Argos.
       +
       +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
       +
       + Usage details: `init' and `clean' extension points must be defined
       + somewhere else
       +
       +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <condition property="deps" value=".deps">
    <not><isset property="deps"/></not>
  </condition>
  <property name="argos.deps" value="${deps}/argos"/>

  <!-- Compiler may already be defined. -->
  <condition property="argos.compiler" value="argos2lus">
    <not><isset property="argos.compiler"/></not>
  </condition>

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <target name="argos-build.init" extensionOf="init">
    <mkdir dir="${argos.deps}"/>
  </target>

  <target name="argos-build.clean" extensionOf="realclean">
    <delete dir="${argos.deps}"/>
  </target>

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <!-- Import any exising dependencies… -->
  <import>
    <fileset dir="${argos.deps}" includes="**/*.xml" erroronmissingdir="false"/>
    <!-- … yet add the current file to this set so that ant does not complain
         about empty fileset (this works since files are never imported more
         than once). -->
    <fileset file="${ant.file.argos-build}"/>
  </import>

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  <!-- Macro encapsulating `argos2lus -\-hept-compat -\-deps…' -->
  <macrodef name="argos2ept"
	    description="Argos → Heptagon translation (+ dependencies)">
    <attribute name="destdir" default="${basedir}/_build"/>
    <attribute name="subdir" default="."/>
    <element name="inputs"/>
    <element name="otherargs" optional="true"/>
    <sequential>
      <local name="argos2ept.status"/>
      <local name="inputs.pp"/>
      <mkdir dir="@{destdir}/@{subdir}"/>
      <mkdir dir="${argos.deps}/@{subdir}"/>
      <property name="argos2ept.status" value="0"/>
      <pathconvert property="inputs.pp" pathsep=" ">
	<inputs/>
	<map from="${basedir}/" to=""/>
      </pathconvert>
      <echo taskname="argos2ept">ept ← ${inputs.pp}</echo>
      <apply executable="${argos.compiler}" taskname="argos2ept"
	     failonerror="false" resultproperty="argos2ept.status"
	     parallel="false">
	<otherargs/>
  	<arg value="-o"/>
  	<targetfile/>
  	<srcfile/>
  	<inputs/>
	<mapper type="glob" from="*.argos" to="@{destdir}/@{subdir}/*.ept"/>
  	<redirector logError="true">
  	  <outputmapper type="glob"
			from="*.argos" to="${argos.deps}/@{subdir}/*.xml"/>
  	</redirector>
  	<arg value="--hept-compat"/>
  	<arg value="--deps"/>
  	<arg value="-"/>
  	<arg value="--depmod"/>
  	<arg value="ant"/>
      </apply>
      <!-- Remove  the dependency files that  may have been created  (due to the
           use of a `redirector'), but left empty due to a syntax error. -->
      <!-- FIXME:  Should the compiler make  its best to output  an ant-friendly
           dependency file that says nothing?-->
      <delete>
	<fileset dir="${argos.deps}/@{subdir}" includes="*.xml">
	  <size value="0" when="equal"/>
	</fileset>
      </delete>
      <fail message="argos2lus exited with status ${argos2ept.status}.">
	<condition>
	  <not><equals arg1="${argos2ept.status}" arg2="0"/></not>
	</condition>
      </fail>
    </sequential>
  </macrodef>

  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

</project>
