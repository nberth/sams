/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

process hold (unused) (o) { automaton { init X X { } -> X with true / o; } }
process hold_when (i)(o) { automaton { init X X { } -> X with  i / o; +> X; } }
process hold_when2 (i)(o1, o2) { automaton { init X X { } -> X with  i / o1, o2; +> X; } }

process equals (i)(o) { hold_when (i)(o) }
process copy (i)(o) { hold_when (i)(o) }
process neg  (i)(o) { hold_when (~i)(o) }
process unless   (i)(o) { neg (i)(o) }
process hold_when_not (i)(o) { neg (i)(o) }
process hold_unless   (i)(o) { neg (i)(o) }
process hold_unless2  (i)(o1, o2) { hold_when2 (~i)(o1, o2) }

process var_equalsp (int a, int b) (ok) { automaton { X {} -> X with a = b / ok; +> X; } }
process var_cpy (int i) (int o) { automaton { X {} -> X with true / o = i; } }
