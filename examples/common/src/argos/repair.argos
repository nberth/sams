/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
include 'include/utils.argos';

process repair
    (ok, node_failure, task_failure, actually_repairing_node, actually_repairing_task)
    (repair_node_req, forget_node_req, repair_task_req, forget_task_req)
  {
    internal repair_node, repair_task,
      {
	equals (~repair_task & ~actually_repairing_node & node_failure & ok) (repair_node)
	||
	copy (repair_node) (repair_node_req)
	||
	equals (~actually_repairing_task & task_failure & ok) (repair_task)
	||
	copy (repair_task) (repair_task_req)
	||
	// XXX `~ok'?
	copy (task_failure & ~repair_task) (forget_task_req)
	||
	// Note one could delay the decision to forget this node... but this
	// would require adding a counter.  Currently, forget every node failure
	// notified while a repairing is being performed...
	copy (node_failure & ~repair_node) (forget_node_req)
      }
  }
