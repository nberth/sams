/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
include 'include/utils.argos';

//

main tier_model
    (
     start_req,
     stop_req,
     repair_node_req,
     forget_node_req,
     repair_task_req,
     forget_task_req,
     repair_ack,
     repair_error,
     add_ack,
     add_error,
     new_failure,
    )
    (
     currently_running,
     currently_repairing,
     start_cmd,
     stop_cmd,
     repair_node_cmd,
     forget_node_cmd,
     repair_task_cmd,
     forget_task_cmd,
     add_cmd,
    );

process tier_model
    (
     start_req,
     stop_req,
     repair_node_req,
     forget_node_req,
     repair_task_req,
     forget_task_req,
     repair_ack,
     repair_error,
     add_ack,
     add_error,
     new_failure,
    )
    (
     currently_running,
     currently_repairing,
     start_cmd,
     stop_cmd,
     repair_node_cmd,
     forget_node_cmd,
     repair_task_cmd,
     forget_task_cmd,
     add_cmd,
    )
  {
    automaton
      {
	Working
	{
	  internal stopping,
	    {
	      automaton
		{
		  Off {}
		    -> Starting with start_req / start_cmd, add_cmd;
		    +> Off;

		  Starting {}
		    -> Off with add_ack & stop_req / stop_cmd;
		    +> On  with add_ack / currently_running;
		    +> Off with stop_req / stop_cmd;
		    +> Off with add_error;
		    +> Starting;

		  On
		  {
		    hold_unless (stopping) (currently_running)
		    ||
		    internal repairing_done,
		      {
			automaton
			  {
			    Nominal {}
			      -> Repairing with repair_node_req / repair_node_cmd, currently_repairing;
			      +> Nominal;
			    Repairing { hold_unless (repairing_done) (currently_repairing) }
			      -> Nominal   with repair_ack     / repairing_done;
			      +> Adding    with repair_error   / add_cmd;
			      // fallback to add command if unable to repair.
			      +> Repairing;
			    Adding { hold_unless (repairing_done) (currently_repairing) }
			      -> Nominal   with add_ack / repairing_done;
			      +> Adding    with add_error / add_cmd; // bad idea: keep adding for now...
			      +> Adding;
			  }
		      }
		    ||
		    automaton
		      {
			S {}
			  -> S with forget_node_req / forget_node_cmd;
			  +> S;
		      }
		    ||
		    copy (repair_task_req) (repair_task_cmd)
		    ||
		    copy (forget_task_req) (forget_task_cmd)
		  }
		  // Consider stop command as immediate:
		    -> Off with stop_req / stop_cmd, stopping;

		}
	    }
	}
      }
  }
