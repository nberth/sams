/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.center.opcode;

import java.io.IOException;

import fr.dyade.aaa.agent.Agent;
import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Notification;
import fr.lig.erods.sams.center.DecisionCode;
import fr.lig.erods.sams.center.Remote;
import fr.lig.erods.sams.common.Env;
import fr.lig.erods.sams.common.Node;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.util.Ansi.C;
import fr.lig.erods.sams.common.util.ISet;
import fr.lig.erods.sams.common.util.TransientException;
import fr.lig.erods.sams.ntier.center.Cluster;
import org.objectweb.util.monolog.api.BasicLevel;

public class MysqlTier extends ReplicatedTier {

  // ---

  private final Remote.PortManager pm = new Remote.PortManager ();
  private int master = Remote.NONE; // No master at all at the beginning.
  private boolean bootstrap = true;

  // ---

  public MysqlTier (final String name, final Env env,
		    final DecisionCode<?, ?, ?> dc,
		    final Cluster cluster, final Env taskEnv,
		    final BalancerTier<?> balancer)
    throws IOException {
    super (name, env, dc, cluster, taskEnv, balancer);
  }

  // ---

  private static final Not.Handlers<MysqlTier> nh =
    new Not.Handlers<MysqlTier> ();
  static {

    nh.put ("master_dumped", new Not.Handler<MysqlTier> () {
	protected void p (final AgentId from, final MysqlTier to, final Not n)
	  throws TransientException {
	  to.master_dumped (from.getTo (), n);
	}});
    nh.put ("slave_started", new Not.Handler<MysqlTier> () {
	protected void p (final AgentId from, final MysqlTier to, final Not n)
	  throws TransientException {
	  to.slave_started (from.getTo (), n);
	}});

  }

  // ---

  /**
   * Intercepts `task_started' notifications (acknowledgements), and performs
   * further actions before actually executing the overwridden method if the new
   * node is a slave.
   */
  protected void task_started (final int rid, final Not cause)
    throws TransientException {

    if (rid == this.master) {
      // Notify new master info to associated balancer. This will be received in
      // the `new_worker' notification.
      info ("New master started!");
      bootstrap = false;
      super.task_started (rid, cause);
    } else {
      info ("New slave started!");
      if (this.slaves_awaiting_dump.isEmpty ()) {
	// Delay final startup code until the replication has started.
	sendTo (r (this.master).agent,
		new Not ("dump", cause)
		.setAck (this.getId (), "master_dumped")); // no timeout for now
	this.slaves_awaiting_dump.add (rid);
      }
    }
  }

  // ---

  private final ISet.Ref slaves_awaiting_dump = ISet.Ref.empty ();

  // ---

  private void master_dumped (final int rid, final Not cause)
    throws TransientException {

    final Node master = r (rid).node;

    for (int new_slave: this.slaves_awaiting_dump) {
      sendTo (r (new_slave).agent,
	      // go back to the original cause (received by `task_started').
	      new Not ("enslave_with_dump", cause.maybeCause ().maybeCause (),
		       Not.e ("dump.user", master.user),
		       Not.e ("dump.host", master.host),
		       Not.e ("master.host", master.host))
	      .setAck (this.getId (), "slave_started")); // no timeout for now
      slaves_feeding_from_dump.add (new_slave);
    }

    slaves_awaiting_dump.clear ();
  }

  private final ISet.Ref slaves_feeding_from_dump = ISet.Ref.empty ();

  // ---

  private void slave_started (final int rid, final Not cause)
    throws TransientException {
    if (slaves_feeding_from_dump.contains (rid))
      slaves_feeding_from_dump.rem (rid);
    super.task_started (rid, cause.maybeCause ().maybeCause ());
  }

  // ---

  protected void task_stopped (final int rid, final Not cause)
    throws TransientException {
    if (rid == this.master && this.remotes.size () > 1) {
      info ("WARNING: Mastership transfer not implemented yet! Bugs ahead…",
	    C.YELLOW, C.BOLD);
      this.master = this.remotes.keySet ().iterator ().next ();
    }
    super.task_stopped (rid, cause);
  }

  // ---

  protected final void prepareInitNot (final Remote r, final Not init)
    throws TransientException, Node.OutOfPortException {
    super.prepareInitNot (r, init);
    if (this.master == Remote.NONE) this.master = r.id;

    logmon.log (BasicLevel.DEBUG, "Mysql identifier is `"+ r.id +"'.");
    init.setProp ("master", r.id == this.master);
    init.setProp ("server.id", r.id);
    init.setProp ("bootstrap_database", bootstrap);

    // TODO: port.names
    info ("Allocating Mysql port for "+ r +".");
    init.setProp ("port", pm.alloc (r));
    info ("Port for Mysql task on "+ r +" is "+ init.getProp ("port") +".",
	  C.PURPLE);
  }

  // ---

  protected final void releaseNodeData (final Remote r)
    throws TransientException {
    super.releaseNodeData (r);
    info ("Releasing Mysql port for "+ r +".");
    pm.releaseAll (r);
  }

  // ---

  protected int peekRunningRemoteToStop (final boolean fullstop) {

    // Choose randomly among slaves.
    assert (this.master != Remote.NONE);

    final ISet.Ref c = new ISet.Ref (this.running);
    // for (int i: this.suspected.cell ()) c.add (i);
    // for (int i: this.repairing.cell ()) c.add (i);
    for (int i: this.starting.cell ()) c.add (i);

    if (! fullstop) {
      assert (c.contains (this.master));
      c.rem (this.master);
    }

    if (c.isEmpty ()) {
      info ("Asked to stop a node, but none is a valid candidate!",
	    C.BOLD, C.RED);
      return Remote.NONE;
    }

    return c.chooseSure ();
  }

  // ---

  // protected void forget_task ()
  //   throws TransientException {

  //   final Remote r = r (this.taskFailures.pollSure ());
  //   if (

  // }

  // ---

  public void react (final AgentId from, final Notification n)
    throws Exception {
    if (nh.maybeHandle (from, this, n, false) == Not.Status.HANDLED) return;
    else super.react (from, n);
  }

  // ---

}
