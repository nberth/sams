/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.depl.opcode;

import fr.dyade.aaa.agent.Agent;
import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Notification;
import fr.lig.erods.sams.common.Env;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.util.Ansi.C;
import fr.lig.erods.sams.ntier.depl.opcode.Node;

public class MysqlNode extends Node {

  // ---

  public MysqlNode (final Env env) { super (env); }

  // ---

  protected final void init (final Not cause) throws Exception {
    env.maybeCopyFrom ("bootstrap_database", cause, "false");
    env.maybeCopyFrom ("master", cause, "false");
    env.maybeCopyFrom ("port", cause);
    env.maybeCopyFrom ("server.id", cause);
    super.init (cause);
  }

  // ---

  protected final void configure (final Not cause) throws Exception {
    // ?
    cause.maybeAck ();
  }

  // ---

  protected void dump (final Not cause) throws Exception {

    info ("Dumping database.", C.PURPLE);

    cause.setProp ("dump.file",       getProp ("dump.file"));
    cause.setProp ("master.user",     getProp ("master.user"));
    cause.setProp ("master.password", getProp ("master.password"));
    cause.setProp ("master.port",     getProp ("master.port"));

    currentProcess = cb.byKey ("dump")
      .thenSendAck (cause);
  }

  // ---

  protected void enslave_with_dump (final Not cause) throws Exception {

    env.surelyCopyFrom ("master.host", cause);
    env.surelyCopyFrom ("master.user", cause);
    env.surelyCopyFrom ("master.password", cause); // hugh…!
    env.surelyCopyFrom ("master.port", cause);
    env.surelyCopyFrom ("dump.user", cause);
    env.surelyCopyFrom ("dump.host", cause);
    env.surelyCopyFrom ("dump.file", cause);

    info ("Bowing down to "+ getProp ("master.host") +':'+
	  getProp ("master.port") +'.', C.PURPLE);

    currentProcess = cb.byKey ("enslave_with_dump")
      .thenSendAck (cause);
  }

  // ---

  protected void enslave (final Not cause) throws Exception {

    env.surelyCopyFrom ("master.host", cause);
    env.surelyCopyFrom ("master.user", cause);
    env.surelyCopyFrom ("master.password", cause); // hugh…!
    env.surelyCopyFrom ("master.port", cause);

    info ("Bowing down to "+ getProp ("master.host") +':'+
	  getProp ("master.port") +'.', C.PURPLE);

    currentProcess = cb.byKey ("enslave")
      .thenSendAck (cause);
  }

  // ---

  protected void masterize (final Not cause) throws Exception {

    info ("Promiting to master.", C.PURPLE);

    currentProcess = cb.byKey ("masterize")
      .thenSendAck (cause);
  }

  // ---

  private static final Not.Handlers<MysqlNode> nh =
    new Not.Handlers<MysqlNode> ();
  static {
    nh.put ("dump", new Not.Handler<MysqlNode> () {
    	protected void p (final AgentId from, final MysqlNode to, final Not n)
	  throws Exception {
    	  to.dump (n);
    	}});
    nh.put ("enslave_with_dump", new Not.Handler<MysqlNode> () {
    	protected void p (final AgentId from, final MysqlNode to, final Not n)
	  throws Exception {
    	  to.enslave_with_dump (n);
    	}});
    nh.put ("enslave", new Not.Handler<MysqlNode> () {
    	protected void p (final AgentId from, final MysqlNode to, final Not n)
	  throws Exception {
    	  to.enslave (n);
    	}});
    nh.put ("masterize", new Not.Handler<MysqlNode> () {
    	protected void p (final AgentId from, final MysqlNode to, final Not n)
	  throws Exception {
    	  to.masterize (n);
    	}});
  }

  // ---

  public void react (AgentId from, Notification n) throws Exception {
    if (nh.maybeHandle (from, this, n, false) == Not.Status.HANDLED) return;
    super.react (from, n);
  }

  // ---

}
