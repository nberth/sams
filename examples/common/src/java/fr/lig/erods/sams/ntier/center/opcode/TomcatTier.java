/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.center.opcode;

import java.io.IOException;

import fr.lig.erods.sams.center.DecisionCode;
import fr.lig.erods.sams.center.Remote;
import fr.lig.erods.sams.common.Env;
import fr.lig.erods.sams.common.Node;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.util.Ansi.C;
import fr.lig.erods.sams.common.util.TransientException;
import fr.lig.erods.sams.ntier.center.Cluster;

public class TomcatTier extends ReplicatedTier {

  private final Remote.PortManager pm = new Remote.PortManager ();

  // ---

  public TomcatTier (final String name, final Env env,
		     final DecisionCode<?, ?, ?> dc,
		     final Cluster cluster, final Env taskEnv,
		     final BalancerTier<?> balancer)
    throws IOException {
    super (name, env, dc, cluster, taskEnv, balancer);
  }

  // ---

  protected final void prepareInitNot (final Remote r, final Not init)
    throws TransientException, Node.OutOfPortException {
    super.prepareInitNot (r, init);

    // TODO: port.names
    info ("Allocating Tomcat port for "+ r +".");
    init.setProp ("shutdown_port", pm.alloc (r));
    init.setProp ("ajp_port",      pm.alloc (r));
    init.setProp ("http_port",     pm.alloc (r));
    init.setProp ("https_port",    pm.alloc (r));
    info ("HTTP port for Tomcat task on "+ r +" is "+ init.getProp ("http_port")
	  +".", C.PURPLE);
  }

  // ---

  protected final void releaseNodeData (final Remote r)
    throws TransientException {
    super.releaseNodeData (r);
    info ("Releasing Tomcat ports for "+ r +".");
    pm.releaseAll (r);
  }

  // ---

}
