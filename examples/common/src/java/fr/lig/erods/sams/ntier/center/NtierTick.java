/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.center;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import com.sun.jna.Structure;
import fr.lig.erods.sams.center.Tick;

public abstract class NtierTick<I extends NtierTick.InputVector, O>
  implements Tick<I, O>, Serializable {

  abstract void restore ();

  public static class Tier_notifs extends Structure implements Serializable {
    public static class ByValue extends Tier_notifs implements Structure.ByValue {}
    public int alive_nodes;
    public int task_failure;
    public int task_repair_ack;
    public int task_repair_error;
    public int task_repairings;
    public int cpu_percentage_avg;
    public int add_ack;
    public int add_error;
    public int suspected_node_failures;
    public int node_failures;
    public int repair_ack;
    public int repair_error;
    public int node_repairings;
    protected List<String> getFieldOrder () {
      return Arrays.asList(new String[] {
	  "alive_nodes",
	  "task_failure",
	  "task_repair_ack",
	  "task_repair_error",
	  "task_repairings",
	  "cpu_percentage_avg",
	  "add_ack",
	  "add_error",
	  "suspected_node_failures",
	  "node_failures",
	  "repair_ack",
	  "repair_error",
	  "node_repairings",
	});
    }
  }

  public static class Tier_config extends Structure implements Serializable {
    public static class ByValue extends Tier_config implements Structure.ByValue {}
    public int min_nodes;
    public int max_nodes;
    public int init_nodes;
    protected List<String> getFieldOrder () {
      return Arrays.asList(new String[] {
	  "min_nodes",
	  "max_nodes",
	  "init_nodes",
	});
    }
  }

  public static class Tier_infos extends Structure implements Serializable {
    public int underload;
    public int overload;
    public int nodes;
    public int running;
    protected List<String> getFieldOrder () {
      return Arrays.asList(new String[] {
	  "underload",
	  "overload",
	  "nodes",
	  "running",
	});
    }
  }

  public static class Tier_cmds extends Structure implements Serializable {
    public int add_node_cmd;
    public int rem_node_cmd;
    public int repair_node_cmd;
    public int forget_node_cmd;
    public int repair_task_cmd;
    public int forget_task_cmd;
    public int start_cmd;
    public int stop_cmd;
    protected List<String> getFieldOrder () {
      return Arrays.asList(new String[] {
	  "add_node_cmd",
	  "rem_node_cmd",
	  "repair_node_cmd",
	  "forget_node_cmd",
	  "repair_task_cmd",
	  "forget_task_cmd",
	  "start_cmd",
	  "stop_cmd",
	});
    }
  }

  public static abstract class InputVector implements Serializable {
    public boolean whole_start_req;
    public boolean whole_stop_req;
    public boolean fake_upsize;
    public boolean fake_downsize;
  }

}
