/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.depl.opcode;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import fr.lig.erods.sams.common.Env;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.util.Ansi.C;
import fr.lig.erods.sams.ntier.common.ApacheWorker;
import fr.lig.erods.sams.ntier.depl.opcode.Node;
import org.objectweb.util.monolog.api.BasicLevel;

public final class ApacheNode extends Node {

  // ---

  private enum State { STOPPED, INITIALIZED, STARTED };

  private final Properties workers = new Properties ();
  private State state = State.STOPPED;

  // ---

  public ApacheNode (final Env env) { super (env); }

  // ---

  protected final void init (final Not cause) throws Exception {
    assert (this.state == State.STOPPED);

    super.init (cause);

    this.setupWorkers (cause);
    this.state = State.INITIALIZED;
  }

  // ---

  protected final void start (final Not cause) throws Exception {
    assert (this.state == State.INITIALIZED);
    this.writeWorkers ();

    super.start (cause);

    this.state = State.STARTED;
  }

  // ---

  protected final void restart (final Not cause) throws Exception {
    assert (this.state == State.STARTED);
    this.writeWorkers ();

    super.restart (cause);
  }

  // ---

  // protected final void setupNodeStartedProperties (final Not ack) {
  //   env.exportFieldsGroup ("as_tail", ack);
  // }

  // ---

  protected final void configure (final Not cause) throws Exception {
    // Only configuration notification we can receive is workers-related.
    this.setupWorkers (cause);

    // If the node was already running, then restart it so that it takes the new
    // workers into account (this also indirectly acknowledges `cause').
    // Otherwize, acknowledge directly.
    if (this.state == State.STARTED) this.restart (cause);
    else                             cause.maybeAck ();
  }

  // ---------------------------------------------------------------------------

  private void setupWorkers (final Not cause) throws IOException {

    final ApacheWorker.Set workers =
      (ApacheWorker.Set) cause.getProp ("workers.ids");
    if (workers == null) {
      info ("Invalid event received: no workers specified.", C.RED);
      return;
    }

    this.workers.clear ();
    this.workers.put ("worker.list", "wlb, status");
    this.workers.put ("worker.status.type", "status");
    this.workers.put ("worker.status.mount", "/jkstatus");
    this.workers.put ("worker.status.read_only", "true");
    this.workers.put ("worker.wlb.type", "lb");
    this.workers.put ("worker.wlb.sticky_session", "true"); // XXX hum…
    for (ApacheWorker w: workers) this.addWorker (w);
  }

  // ---

  private void addWorker (final ApacheWorker desc)
    throws IOException {

    String lst = this.workers.getProperty ("worker.wlb.balance_workers");
    lst = (lst == null) ? desc.name : lst +','+ desc.name;

    if (logmon.isLoggable (BasicLevel.DEBUG))
      logmon.log (BasicLevel.DEBUG, "Registering worker `"+ desc.name+ "'.");

    this.workers.put ("worker.wlb.balance_workers", lst);
    this.workers.put ("worker."+ desc.name +".type", desc.type);
    this.workers.put ("worker."+ desc.name +".host", desc.host);
    this.workers.put ("worker."+ desc.name +".port", desc.port.toString ());
    // this.workers.put ("worker."+ desc.name +".lbfactor", "100"); // XXX
    // this.workers.put ("worker."+ desc.name +".socket_timeout", "30");
  }

  // ---

  private void writeWorkers () throws IOException {
    // Avoid writing…
    if (state == State.STOPPED) return;
    this.workers
      .store (new FileOutputStream (this.getProp ("workers.properties")),
	      "apache workers - auto generated");
  }

  // ---

}
