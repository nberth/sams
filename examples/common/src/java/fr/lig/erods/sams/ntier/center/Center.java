/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.center;

import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Channel;
import fr.dyade.aaa.agent.Notification;
import fr.lig.erods.sams.center.Cmd;
import fr.lig.erods.sams.center.ModelDriver;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.Sams;
import fr.lig.erods.sams.common.opcode.Impulse;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Agent discussing with the decision code.
 *
 * Listens to the following notification instructions:
 *
 * <ul>
 *
 * <li><b>start</b> send <b>start</b> impulse to the decision code
 *
 * <li><b>stop</b> send <b>start</b> impulse to the decision code
 *
 * <li><b>up</b> send <b>fake_upsize</b> impulse to the decision code
 * (ntier-specific, temporary, for testing purpose)
 *
 * <li><b>down</b> send <b>fake_downsize</b> impulse to the decision code
 * (ntier-specific, temporary, for testing purpose)
 *
 * </ul>
 *
 * Also listens to the following notifications:
 *
 * <ul>
 *
 * <li><b>started</b> should be received from the decision code once it has
 * started
 *
 * </ul>
 */
public class Center extends ModelDriver {

  // ---

  public Center (final AgentId dc) {
    super ("center", dc);
  }

  // ---

  private static final Not.Handlers<Center> nh = new Not.Handlers<Center> ();
  static {

    // ---

    nh.put ("start", new Not.Handler<Center> () {
	protected void p (final AgentId from, final Center to, final Not n) {
	  to.start (n);
	}});
    nh.put ("started", new Not.Handler<Center> () {
	protected void p (final AgentId from, final Center to, final Not n)
	  throws Exception {
	  to.started (n);
	}});

    // ---

    nh.put ("stop", new Not.Handler<Center> () {
	protected void p (final AgentId from, final Center to, final Not n)
	  throws Exception {
	  to.stop (n);
	}});

    // ---

    nh.put ("up", new Not.Handler<Center> () {
	protected void p (final AgentId from, final Center to, final Not n) {
	  to.fake_upsize ();
	}});
    nh.put ("down", new Not.Handler<Center> () {
	protected void p (final AgentId from, final Center to, final Not n) {
	  to.fake_downsize ();
	}});

    // ---

  }

  // ---

  protected final Cmd.Map allCmds () {
    final String [] basicCmds = new String [] {
      "start", "stop", "up", "down"
    };
    final Cmd.Map cmds = new Cmd.Map ();
    final AgentId dest = this.getId ();
    for (final String c: basicCmds) {
      cmds.put  (c, new Cmd.Basic () { protected void run () {
	Channel.sendTo (dest, new Not (c));
      }});
    }
    return cmds;
  }

  // ---

  public void react (final AgentId from, final Notification n)
    throws Exception {
    // Force error reporting for unhandled `Not' instances…
    if (nh.maybeHandle (from, this, n, true) == Not.Status.HANDLED) return;
    super.react (from, n);
  }

  // ---

  private boolean started = false;

  // ---

  private void start (final Not cause) {
    if (this.started) return;	// in case…
    sendTo (this.dc,
	    new Not ("tick.start",
		     Not.e ("listener.id", this.getId ()),
		     Not.e (Sams.env, "tick.max-delay", "10000"))); // 10s.
    // This will trigger a first reaction, and hence should start the
    // deployment.
    sendTo (this.dc, new Impulse ("start"));
  }

  // ---

  private void started (final Not cause) throws Exception {
    logmon.log (BasicLevel.INFO, "Whole center successfully started.");
    started = true;
  }

  // ---

  private void stop (final Not cause) throws Exception {
    // if (this.started) {
    sendTo (this.dc, new Impulse ("stop"));
    sendTo (this.dc, new Not ("tick.stop"));
    this.started = false;
    // }
  }

  // ---

  /**
   * Artificial test, sending "fake_upsize" impulse to the decision code.
   */
  private void fake_upsize () {
    logmon.log (BasicLevel.INFO, "`fake_upsize'!.");
    sendTo (this.dc, new Impulse ("fake_upsize"));
  }

  /**
   * Artificial test, sending "fake_downsize" impulse to the decision code.
   */
  private void fake_downsize () {
    logmon.log (BasicLevel.INFO, "`fake_downsize'!.");
    sendTo (this.dc, new Impulse ("fake_downsize"));
  }

  // ---

  protected String getLogTopic () { return Center.class.getName (); }

}
