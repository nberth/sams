/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.depl.opcode;

import fr.lig.erods.sams.common.Env;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.ntier.depl.opcode.Node;

public class TomcatNode extends Node {

  private enum State { STOPPED, INITIALIZED, STARTED };
  private State state = State.STOPPED;

  // ---

  public TomcatNode (final Env env) { super (env); }

  // ---

  protected final void init (final Not cause) throws Exception {
    assert (this.state == State.STOPPED);
    this.initEnv (cause);
    super.init (cause);
    this.state = State.INITIALIZED;
  }

  // ---

  protected final void start (final Not cause) throws Exception {
    assert (this.state == State.INITIALIZED);
    super.start (cause);
    this.state = State.STARTED;
  }

  // ---

  protected final void restart (final Not cause) throws Exception {
    assert (this.state == State.STARTED);
    super.restart (cause);
  }

  // ---

  // protected final void setupNodeStartedProperties (final Not ack) {
  //   this.env.exportFieldsGroup ("as_tail_node", ack);
  // }

  // ---

  // Tomcat is sometimes a little non-cooperative…
  protected final void real_stop (final Not not) throws Exception {
    this.currentProcess = cb.byKey ("kill");
    super.real_stop (not);
  }

  // ---

  protected final void configure (final Not cause) throws Exception {
    this.updateInfos (cause);

    // If the node was already running, then restart it so that it takes the new
    // workers into account (this also indirectly acknowledges
    // `cause'). Otherwize, acknowledge directly.
    if (this.state == State.STARTED) this.restart (cause);
    else                             cause.maybeAck ();
  }

  // ---------------------------------------------------------------------------

  protected final void initEnv (final Not not) {
    env.maybeCopyFrom ("shutdown_port", not);
    env.maybeCopyFrom ("http_port", not);
    env.maybeCopyFrom ("ajp_port", not);
    env.maybeCopyFrom ("https_port", not);
    this.updateInfos (not);
  }

  // ---

  private void updateInfos (final Not not) {
    env.importFieldsGroup ("tail", not);
  }

  // ---

}
