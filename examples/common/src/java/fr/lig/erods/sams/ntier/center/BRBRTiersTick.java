/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.center;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import com.sun.jna.Native;
import com.sun.jna.Structure;

public class BRBRTiersTick
  extends NtierTick<BRBRTiersTick.Input, BRBRTiersTick.Output> {

  private final String libname;

  BRBRTiersTick (final String libname) {
    // Register the library in this class 'cause it's the one declaring the
    // native methods.
    this.libname = libname;
    this.restore ();
  }

  void restore () {
    Native.register (this.libname);
  }

  public static class Memory extends Structure implements Serializable {
    public byte[] contents = new byte[800];
    protected List<String> getFieldOrder () {
      return Arrays.asList(new String[] {
	  "contents",
	});
    }
  }

  public static class Output extends Structure {
    public int started;
    public int t1_repair_inhibited;
    public int t2_repair_inhibited;
    public int t2_sizing_inhibited;
    public int t3_repair_inhibited;
    public int t4_repair_inhibited;
    public int t4_sizing_inhibited;
    public Tier_infos t1_infos;
    public Tier_cmds  t1_cmds;
    public Tier_infos t2_infos;
    public Tier_cmds  t2_cmds;
    public Tier_infos t3_infos;
    public Tier_cmds  t3_cmds;
    public Tier_infos t4_infos;
    public Tier_cmds  t4_cmds;
    protected List<String> getFieldOrder () {
      return Arrays.asList(new String[] {
	  "started",
	  "t1_repair_inhibited",
	  "t2_repair_inhibited",
	  "t2_sizing_inhibited",
	  "t3_repair_inhibited",
	  "t4_repair_inhibited",
	  "t4_sizing_inhibited",
	  "t1_infos",
	  "t1_cmds",
	  "t2_infos",
	  "t2_cmds",
	  "t3_infos",
	  "t3_cmds",
	  "t4_infos",
	  "t4_cmds",
	});
    }
  }

  public static class Input extends NtierTick.InputVector {
    public Tier_notifs.ByValue t1_notifs;
    public Tier_config.ByValue t1_config;
    public Tier_notifs.ByValue t2_notifs;
    public Tier_config.ByValue t2_config;
    public Tier_notifs.ByValue t3_notifs;
    public Tier_config.ByValue t3_config;
    public Tier_notifs.ByValue t4_notifs;
    public Tier_config.ByValue t4_config;
  }

  private static native void Tick__tick_reset (Memory state);
  private static native void Tick__tick_step (int whole_start_req,
					      int whole_stop_req,
					      int fake_upsize,
					      int fake_downsize,
					      Tier_notifs.ByValue t1_notifs,
					      Tier_config.ByValue t1_config,
					      Tier_notifs.ByValue t2_notifs,
					      Tier_config.ByValue t2_config,
					      Tier_notifs.ByValue t3_notifs,
					      Tier_config.ByValue t3_config,
					      Tier_notifs.ByValue t4_notifs,
					      Tier_config.ByValue t4_config,
					      Output out,
					      Memory state);

  private final Memory state = new Memory ();
  private transient Output output = null;

  public void reset () {
    Tick__tick_reset (this.state);
  }

  public Output step (final Input iv) {
    if (this.output == null) this.output = new Output ();
    Tick__tick_step (iv.whole_start_req ? 1 : 0,
		     iv.whole_stop_req  ? 1 : 0,
		     iv.fake_upsize     ? 1 : 0,
		     iv.fake_downsize   ? 1 : 0,
		     iv.t1_notifs,
		     iv.t1_config,
		     iv.t2_notifs,
		     iv.t2_config,
		     iv.t3_notifs,
		     iv.t3_config,
		     iv.t4_notifs,
		     iv.t4_config,
		     this.output,
		     this.state);
    return this.output;
  }
}
