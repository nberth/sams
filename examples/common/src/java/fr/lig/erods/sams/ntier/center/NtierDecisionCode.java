/*
 * This file is part of sams multi-tier application management examples.
 *
 * Copyright Grenoble University, (2013)
 *
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 *
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lig.erods.sams.ntier.center;

import fr.dyade.aaa.agent.AgentId;
import fr.dyade.aaa.agent.Notification;
import fr.lig.erods.sams.center.DecisionCode;
import fr.lig.erods.sams.common.Not;
import fr.lig.erods.sams.common.opcode.Impulse;
import fr.lig.erods.sams.common.opcode.Measure;
import fr.lig.erods.sams.ntier.center.opcode.LinuxCPUMuxer;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Abstract class encapsulating the common part of all decision code agents for
 * the multi-tier examples.
 *
 * <p>The following kinds of {@link fr.lig.erods.sams.common.opcode.Impulse}
 * notifications are handled:
 *
 * <ul>
 *
 * <li><em>High-level command impulses</em> from a model driver (see {@link
 * fr.lig.erods.sams.center.ModelDriver}) — commands of this kind are typically
 * very high-level instructions for the model such as <b>start</b> or
 * <b>stop</b> (not to be confounded with the notifications described above that
 * act on the decision code itself), that request to act on the managed system
 * as a whole;
 *
 * <li><em>Low-level impulses</em> from agents constituting the operating code;
 * this kind of impulse is meant to notify sudden changes in the discrete states
 * of the managed resources;
 *
 * </ul>
 *
 * <p>Instances of {@link fr.lig.erods.sams.common.opcode.Measure} notifications
 * should be directly handled in the sub-classes because they are parametric in
 * the type of data they contain. They carry <em>low-level measures</em> from
 * agents constituting the operating code; this kind of notification signals
 * fresher values for measures; they are typically received directly from
 * sensors or multiplexers that aggregate such values.
 *
 * <p>Decision code agents also send several kinds of notifications (for now,
 * only as instances of {@link fr.lig.erods.sams.common.Not}):
 *
 * <ul>
 *
 * <li><em>Low-level commands</em> to agents in the operating code; (Note that
 * for these, some sort of "LowLevelCommand" sub-class of {@link
 * fr.dyade.aaa.agent.Notification} should also fit the purpose, as long as the
 * operating code understands it);
 *
 * <li><em>High-level monitoring information</em> to a model driver, optionally
 * — these can convey <b>started</b> or <b>stopped</b> notifications for
 * instance;
 *
 * </ul>
 *
 */
public abstract class NtierDecisionCode<T extends NtierTick<I, O>,
					  I extends NtierTick.InputVector, O>
  extends DecisionCode<T, I, O> {

  /** The identifier of the model driver agent, that can receive some
      information about the status of the managed system. */
  private AgentId listener;

  // ---

  protected NtierTick.Tier_config.ByValue makeConfig (final int min,
						      final int max) {
    final NtierTick.Tier_config.ByValue v =
      new NtierTick.Tier_config.ByValue ();
    v.min_nodes = min;
    v.max_nodes = max;
    v.init_nodes = min;
    return v;
  }

  protected NtierTick.Tier_notifs.ByValue defaultNotifs () {
    return new NtierTick.Tier_notifs.ByValue ();
  }

  public NtierDecisionCode (final T t, final I iv) {
    super (t, iv);
    this.iv.whole_start_req = false;
    this.iv.whole_stop_req  = false;
    this.iv.fake_upsize     = false;
    this.iv.fake_downsize   = false;
    this.init ();
  }

  // ---

  /**
   * Reloads the tick's native library.
   */
  protected void agentInitialize (final boolean firstTime) throws Exception {
    super.agentInitialize (firstTime);
    if (! firstTime)
      this.tick.restore ();
  }

  // ---

  private final Measure.MapReceiver<Integer> imr = new
    Measure.MapReceiver<Integer> (Integer.class);

  private final Measure.MapReceiver<LinuxCPUMuxer.O> lmr = new
    Measure.MapReceiver<LinuxCPUMuxer.O> (LinuxCPUMuxer.O.class);

  // ---

  public void react (final AgentId from, final Notification n)
    throws Exception {
    if (n instanceof Measure) {	// first filter
      if (imr.maybeHandle (n) == Measure.Status.HANDLED) return;
      if (lmr.maybeHandle (n) == Measure.Status.HANDLED) return;
    }
    super.react (from, n);
  }

  // ---

  protected void perTierInit (final String tier,
			      final NtierTick.Tier_notifs notifs) {

    ih.put (tier +".add_error", new Impulse.Handler () {
	protected void assign (boolean b) { notifs.add_error = b ? 1 : 0; }});
    ih.put (tier +".add_ack", new Impulse.Handler () {
	protected void assign (boolean b) { notifs.add_ack = b ? 1 : 0; }});
    ih.put (tier +".task_failure", new Impulse.Handler () {
	protected void assign (boolean b) { notifs.task_failure = b ? 1 : 0; }});
    ih.put (tier +".task_repair_ack", new Impulse.Handler () {
	protected void assign (boolean b) { notifs.task_repair_ack = b ? 1 : 0; }});
    ih.put (tier +".task_repair_error", new Impulse.Handler () {
	protected void assign (boolean b) { notifs.task_repair_error = b ? 1 : 0; }});
    ih.put (tier +".node_failure", new Impulse.Handler () {
	protected void assign (boolean b) { notifs.node_failures = b ? 1 : 0; }});
    ih.put (tier +".note_repair_ack", new Impulse.Handler () {
	protected void assign (boolean b) { notifs.repair_ack = b ? 1 : 0; }});
    ih.put (tier +".node_repair_error", new Impulse.Handler () {
	protected void assign (boolean b) { notifs.repair_error = b ? 1 : 0; }});

    imr.put (tier +".task_repairings", new Measure.Handler<Integer> () {
	protected void assign (Integer value) { notifs.task_repairings = value; }});
    imr.put (tier +".node_repairings", new Measure.Handler<Integer> () {
	protected void assign (Integer value) { notifs.node_repairings = value; }});
    imr.put (tier +".suspected_node_failures", new Measure.Handler<Integer> () {
	protected void assign (Integer value) { notifs.suspected_node_failures = value; }});

    lmr.put (tier +".cpu-info", new Measure.Handler<LinuxCPUMuxer.O> () {
	protected void assign (LinuxCPUMuxer.O info) {
	  if (logmon.isLoggable (BasicLevel.DEBUG))
	    logmon.log (BasicLevel.DEBUG, "Load information for `"+ tier
			+"' received: "+ info.alive_nodes
			+" node(s), cpu = "+ info.cpu_percentage_avg
			// +"%, suspected failures = "+ info.suspected_failures
			+".");
	  notifs.alive_nodes	    = info.alive_nodes;
	  notifs.cpu_percentage_avg = info.cpu_percentage_avg;
	  // notifs.suspected_failures = info.suspected_failures;
	}
      });
  }

  // ---

  private void init () {
    ih.put ("start", new Impulse.Handler () {
	protected void assign (boolean b) { iv.whole_start_req = b; } });
    ih.put ("stop",  new Impulse.Handler () {
	protected void assign (boolean b) { iv.whole_stop_req = b; } });
    ih.put ("fake_downsize", new Impulse.Handler () {
	protected void assign (boolean b) { iv.fake_downsize = b; } });
    ih.put ("fake_upsize", new Impulse.Handler () {
	protected void assign (boolean b) { iv.fake_upsize = b; } });
  }

  // ---

  protected void checkInputConstraints () {
    super.checkInputConstraints (); // in case…

    // XXX really check this? after all, they are impulses…
    assert (! (this.iv.whole_start_req & this.iv.whole_stop_req));
  }

  // ---

  protected void notifyTier (final AgentId tier,
			     final NtierTick.Tier_infos infos,
			     final NtierTick.Tier_cmds cmds) {
    if (cmds.repair_node_cmd != 0) notifyTierCmd (tier, "repair_node");
    if (cmds.forget_node_cmd != 0) notifyTierCmd (tier, "forget_node");
    if (cmds.repair_task_cmd != 0) notifyTierCmd (tier, "repair_task");
    if (cmds.forget_task_cmd != 0) notifyTierCmd (tier, "forget_task");
    if (cmds.add_node_cmd    != 0) notifyTierCmd (tier, "start_node");
    if (cmds.rem_node_cmd    != 0) notifyTierCmd (tier, "stop_node");
    if (cmds.start_cmd	     != 0) notifyTierCmd (tier, "start");
    if (cmds.stop_cmd	     != 0) notifyTierCmd (tier, "stop");
  }

  private void notifyTierCmd (final AgentId tier, final String name) {
    if (logmon.isLoggable (BasicLevel.DEBUG))
      logmon.log (BasicLevel.DEBUG, "Sending command `"+ name +"' to agent `"+
		  tier +"'.");
    sendTo (tier, new Not (name));
  }

  // ---

  protected void notifyListener (final int b, final String name) {
    notifyListener (b != 0, name);
  }

  private void notifyListener (final boolean b, final String name) {
    if (b) {
      assert (this.listener != null);
      if (logmon.isLoggable (BasicLevel.DEBUG))
	logmon.log (BasicLevel.DEBUG, "Sending impulse `"+ name +"' to listener.");
      sendTo (this.listener, new Not (name));
    }
  }

  // ---

  protected void start (final Not cause) {
    super.start (cause);
    this.listener = (AgentId) cause.getProp ("listener.id");
  }

  // ---

  // "Most concrete sub-class" defines log name.
  protected final String getLogTopic () {
    return this.getClass ().getName ();
  }

  // ---

}
