#!/bin/bash
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# This file is part of sams multi-tier application management examples.
#
# Copyright Grenoble University, (2013)
#
# Author: Nicolas Berthier <nicolas.berthier@inria.fr>
#
# This software is a computer program whose purpose is to autonomously manage
# computing systems using reactive control techniques.
#
# This software is governed by the CeCILL license under French law and abiding
# by the rules of distribution of free software.  You can use, modify and/ or
# redistribute the software under the terms of the CeCILL license as circulated
# by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
#
# As a counterpart to the access to the source code and rights to copy, modify
# and redistribute granted by the license, users are provided only with a
# limited warranty and the software's author, the holder of the economic
# rights, and the successive licensors have only limited liability.
#
# In this respect, the user's attention is drawn to the risks associated with
# loading, using, modifying and/or developing or reproducing the software by
# the user in light of its specific status of free software, that may mean that
# it is complicated to manipulate, and that also therefore means that it is
# reserved for developers and experienced professionals having in-depth
# computer knowledge. Users are therefore encouraged to load and test the
# software's suitability as regards their requirements in conditions enabling
# the security of their systems and/or data to be ensured and, more generally,
# to use and operate it in the same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

dir="mysql"
root="${PWD}/${dir}";

cmd="$1"; shift;
pidfile="$1"; shift;
port="$1"; shift;

user="$USER";
group="$USER";

cnf="${root}/config/my.cnf";
socket="${root}/mysql.sock";
mysql_bin="mysql-bin";
mysql_relay="mysql-relay-bin";

mysql_cmd () {
    "${root}/bin/mysql" --user=root --socket="${socket}" "$@";
}

rubis_bootstrap () {
    echo "Updating dates in rubis tables..." >/dev/stderr;
    mysql_cmd -e "
      update rubis.items
        set start_date=NOW(), end_date=ADDDATE(NOW(), 1+(RAND()*7))
        where start_date < end_date;
      update rubis.items
        set start_date=NOW(), end_date=SUBDATE(NOW(), 1+(RAND()*7))
        where start_date > end_date;
    ";
}

create_proxy_user () {
    echo "Configuring proxy user..." >/dev/stderr;
    mysql_cmd -e "
      create user 'proxy'@'%' identified by 'toto';
      grant all privileges on *.* to 'proxy'@'%' identified by 'toto'
        with grant option;
          
      create user 'proxy'@'localhost' identified by 'toto';
      grant all privileges on *.* to 'proxy'@'localhost' identified by 'toto'
        with grant option;
          
      # Apparently, there can be an authenticattion problem if we do not add
      # this and the proxy runs on the same machine:
      create user 'proxy'@'$host' identified by 'toto';
      grant all privileges on *.* to 'proxy'@'$(hostname)' identified by 'toto'
        with grant option;

      flush privileges;
    ";
}

init () {
    exec 1>> init.log;

    [ ! -d "${dir}" ] && {
	echo "\`${dir}' directory expected!" >/dev/stderr;
	exit 1;
    }
    
    echo "Configuring..." >/dev/stderr;
    configure "$@";

    echo "Installing database..." >/dev/stderr;
    "${root}/scripts/mysql_install_db" \
	--defaults-file="${cnf}"       \
	--basedir="${root}";

    local bootstrap_rubis_database="$2";
    if test "x$bootstrap_rubis_database" = "xtrue"; then
	echo "Bootstrapping rubis database..." >/dev/stderr;
	real_start --binlog-ignore-db=rubis --replicate-ignore-db=rubis;
	rubis_bootstrap;
	create_proxy_user;
	mysql_cmd -e "reset master";
	stop;
    fi;
    echo "Initialization done." >/dev/stderr;
}

configure () {
    local server_id="$1"; shift
    local bootstrap_rubis_database="$1"; shift
    
    exec 1>> configure.log;

    [ ! -f "${cnf}" ] && {
	echo "File \`${cnf}' not found!" >/dev/stderr;
	exit 1;
    }

    if test "x$bootstrap_rubis_database" = "xtrue"; then
	datadir="/home/local/tmp/data"; # XXX for the experiments only!
    else
	datadir="${root}/data";
    fi

    sed -e '
	 s!@port@!'"${port}"'!g;
	 s!@server_id@!'"${server_id}"'!g;
	 s!@mysql_bin@!'"${mysql_bin}"'!g;
	 s!@mysql_relay@!'"${mysql_relay}"'!g;
	 s!@basedir@!'"${root}"'!g;
	 s!@datadir@!'"${datadir}"'!g;
	 s!@socket@!'"${socket}"'!g;
	 s!@pid_file@!'"${PWD}/${pidfile}"'!g;
       ' -i "${cnf}";
}

create_slave_user () {
    echo "Configuring replica user..." >/dev/stderr;
    mysql_cmd -e "
      grant replication slave on *.* to 'replica'@'%' identified by 'toto';
      grant replication slave on *.* to 'replica'@'localhost' identified by 'toto';
      grant replication slave on *.* to 'replica'@'$(hostname)' identified by 'toto';
      flush privileges;
    ";
}

real_start () {
    (
	cd "${root}";
	exec nohup "${root}/bin/mysqld_safe" --defaults-file="${cnf}" \
	     --basedir="${root}" "$@" </dev/null 2>&1 >/dev/null;
    ) &

    echo "Waiting..." >/dev/stderr;
    while [ ! -f "${pidfile}" ]; do sleep 1; done;
}

start () {
    master="$1"; shift;

    echo "Starting..." >/dev/stderr;
    real_start;
    
    if test "x$master" = "xtrue"; then
	create_slave_user;
    fi;
    # otherwise we shall receive an `enslave' command.

    echo "Started." >/dev/stderr;
}

restart () {
    if test -f "${pidfile}"; then
	stop;
    fi;
    echo "Restarting..." >/dev/stderr;
    real_start;
    echo "Restarted." >/dev/stderr;
}

dump () {
    dump_file="$1"; shift;

    # "${root}"/bin/mysql -h localhost --user=root --port=${port} --socket="${socket}" -e "
    #     #stop slave;
    #     flush tables with read lock;

    #     \\! \\\"${root}\\\"/bin/mysqldump --user=root --port=${port} --socket=\\\"${socket}\\\" --add-drop-table --all-databases --master-data \\| gzip > \\\"${dump_file}\\\"
        
    #     show master status;
    # ";

    # "${root}"/bin/mysqldump -h localhost --user=root --port=${port} --socket="${socket}" --add-drop-table --all-databases \
    # 	| gzip -9 > "${dump_file}";

    # "${root}"/bin/mysql -h localhost --user=root --port=${port} --socket="${socket}" -e "
    #     show master status;
    #     unlock tables;

    #     #create user 'replica'@'%' identified by 'toto';
    #     #grant replication slave on *.* to 'replica'@'%' identified by 'toto' with grant option;

    #     #create user 'replica'@'localhost' identified by 'toto';
    #     #grant replication slave on *.* to 'replica'@'localhost' identified by 'toto' with grant option;

    #     unlock privileges;
    # ";

    "${root}"/bin/mysqldump -h localhost --user=root --port=${port} \
	     --socket="${socket}" \
    	     --opt --all-databases \
    	     --master-data=1 --single-transaction  \
    	| gzip > "${dump_file}";

}

# takeover () {
#     from_user="$1"; shift;
#     from_host="$1"; shift;
#     from_file="$1"; shift;
#     # master_host="$1"; shift;
#     # master_port="$1"; shift;
#     # master_user="$1"; shift;
#     # master_password="$1"; shift;

#     # Disable (interactive) host key checking for those...
#     # sshopts="-o StrictHostKeyChecking=yes";
#     sshopts="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null";

#     ssh "${from_user}@${from_host}" $sshopts -- cat "${from_file}" | \
# 	gunzip --to-stdout                                         | \
# 	mysql_cmd;
# }

masterize () {
    mysql_cmd -e "stop slave; reset master;";
}

enslave () {
    master_host="$1"; shift;
    master_port="$1"; shift;
    master_user="$1"; shift;
    master_password="$1"; shift;

    # in case... (?)
    mysql_cmd -e "
      stop slave;
      change master to
        MASTER_HOST='${master_host}', MASTER_PORT=${master_port},
        MASTER_USER='${master_user}', MASTER_PASSWORD='${master_password}';
      start slave;
    ";
}

enslave_with_dump () {
    from_user="$1"; shift;
    from_host="$1"; shift;
    from_file="$1"; shift;
    master_host="$1"; shift;
    master_port="$1"; shift;
    master_user="$1"; shift;
    master_password="$1"; shift;

    # in case... (?)
    mysql_cmd -e "stop slave;";

    # Disable (interactive) host key checking for those...
    # sshopts="-o StrictHostKeyChecking=yes";
    sshopts="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null";

    ssh "${from_user}@${from_host}" $sshopts -- cat "${from_file}" | \
	gunzip --to-stdout                                         | \
	mysql_cmd;

    mysql_cmd -e "
      change master to
        MASTER_HOST='${master_host}', MASTER_PORT=${master_port},
        MASTER_USER='${master_user}', MASTER_PASSWORD='${master_password}';
      start slave;
    ";
}

stop () {
    "${root}/bin/mysqladmin" --user=root --socket="${socket}" shutdown;

    if test -f "${pidfile}"; then
	pid=$(< "${pidfile}");
	"${root}/bin/mysql_waitpid" ${pid} 10;
    fi;
}

case $cmd in
    init | start | stop | restart | dump |       \
	masterize | enslave | enslave_with_dump)
	exec 1>> "${cmd}.log";
	"${cmd}" "$@";
	;;
    *)
	echo "Unknown command \`${cmd}'!" >&/dev/stderr;
	exit 1;
esac;
