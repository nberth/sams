#!/bin/bash
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# This file is part of sams multi-tier application management examples.
#
# Copyright Grenoble University, (2013)
#
# Author: Nicolas Berthier <nicolas.berthier@inria.fr>
#
# This software is a computer program whose purpose is to autonomously manage
# computing systems using reactive control techniques.
#
# This software is governed by the CeCILL license under French law and abiding
# by the rules of distribution of free software.  You can use, modify and/ or
# redistribute the software under the terms of the CeCILL license as circulated
# by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
#
# As a counterpart to the access to the source code and rights to copy, modify
# and redistribute granted by the license, users are provided only with a
# limited warranty and the software's author, the holder of the economic
# rights, and the successive licensors have only limited liability.
#
# In this respect, the user's attention is drawn to the risks associated with
# loading, using, modifying and/or developing or reproducing the software by
# the user in light of its specific status of free software, that may mean that
# it is complicated to manipulate, and that also therefore means that it is
# reserved for developers and experienced professionals having in-depth
# computer knowledge. Users are therefore encouraged to load and test the
# software's suitability as regards their requirements in conditions enabling
# the security of their systems and/or data to be ensured and, more generally,
# to use and operate it in the same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

dir="apache"
root="${PWD}/${dir}";
cmd="$1"; shift;
pid="${root}/logs/apache.pid";

init () {
    user="$USER";
    groups=( $(groups) );
    group="${groups[0]}";
    host="$(hostname)";
    port="$1"; shift;

    exec 1> init.log;

    [ ! -d "${dir}" ] && {
	echo "\`${dir}' directory expected!" >/dev/stderr;
	exit 1;
    }

    sed -e '
	s @SERVER_ROOT@ '"${root}"' g;
	s @PIDFILE@ logs/apache.pid g;
	s/@PORT@/'"${port}"'/g;
	s/@HOST@/'"${host}"'/g;
	s/@USER@/'"${user}"'/g;
	s/@GROUP@/'"${group}"'/g;
      ' -i "${root}/conf/httpd.conf";

    exit 0;
}

case $cmd in
    init)
	init "$@";
	;;
    # XXX temporary:
    # start)
    # 	exec 1> "${cmd}.log";
    # 	sleep 5;	     # add some extra artificial time for tests purpose!
    # 	export LD_LIBRARY_PATH="${root}/lib:$LD_LIBRARY_PATH";
    # 	exec "${root}"/bin/httpd -f "${root}/conf/httpd.conf" -k "${cmd}";
    # 	# while [ ! -f "${pid}" ]; do sleep 1; done;
    # 	;;
    graceful | restart | start | stop)
	exec 1> "${cmd}.log";
	export LD_LIBRARY_PATH="${root}/lib:$LD_LIBRARY_PATH";
	exec "${root}"/bin/httpd -f "${root}/conf/httpd.conf" -k "${cmd}";
	# while [ ! -f "${pid}" ]; do sleep 1; done;
	;;
    *)
	echo "Unknown command \`${cmd}'!" >&/dev/stderr;
	exit 1;
esac;
