(* 
 * This file is part of sams multi-tier application management examples.
 * 
 * Copyright Grenoble University, (2013)
 * 
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 * 
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *)

include include/utils.ept.pp
include startup4.ept
include repair.ept
include sizing.ept.pp
include tier_model.ept
include tier_model_repl.ept
include tier_properties.ept.pp

(* Generic combination of (one non-replicated tier using services provided by a
  replicated one) * 2. *)

node brbr_tiers_ctrld
  (
   whole_start_req: bool;
   whole_stop_req: bool;

   t1_task_failure: bool;
   t1_task_actually_repairing: bool;
   t1_node_suspected_failures: bool;
   t1_node_failure: bool;
   t1_repair_node_ack: bool;
   t1_repair_node_error: bool;
   t1_add_node_ack: bool;
   t1_add_node_error: bool;

   t2_config_min_nodes: int;
   t2_config_max_nodes: int;
   t2_config_init_nodes: int;
   t2_task_failure: bool;
   t2_task_actually_repairing: bool;
   t2_node_suspected_failures: bool;
   t2_node_failures: int;
   t2_node_failure: bool;
   t2_node_multiple_failures: bool;
   t2_repair_node_ack: bool;
   t2_repair_node_error: bool;
   t2_add_node_ack: bool;
   t2_add_node_error: bool;
   t2_underload: bool;
   t2_overload: bool;

   t3_task_failure: bool;
   t3_task_actually_repairing: bool;
   t3_node_suspected_failures: bool;
   t3_node_failure: bool;
   t3_repair_node_ack: bool;
   t3_repair_node_error: bool;
   t3_add_node_ack: bool;
   t3_add_node_error: bool;

   t4_config_min_nodes: int;
   t4_config_max_nodes: int;
   t4_config_init_nodes: int;
   t4_task_failure: bool;
   t4_task_actually_repairing: bool;
   t4_node_suspected_failures: bool;
   t4_node_failures: int;
   t4_node_failure: bool;
   t4_node_multiple_failures: bool;
   t4_repair_node_ack: bool;
   t4_repair_node_error: bool;
   t4_add_node_ack: bool;
   t4_add_node_error: bool;
   t4_underload: bool;
   t4_overload: bool;
   )
returns
  (
   started: bool;

   t1_running: bool;
   t1_start_cmd: bool;
   t1_stop_cmd: bool;
   t1_repair_node_cmd: bool;
   t1_forget_node_cmd: bool;
   t1_repair_task_cmd: bool;
   t1_forget_task_cmd: bool;
   t1_add_node_cmd: bool;

   t2_running: bool;
   t2_start_cmd: bool;
   t2_stop_cmd: bool;
   t2_repair_node_cmd: bool;
   t2_forget_node_cmd: bool;
   t2_repair_task_cmd: bool;
   t2_forget_task_cmd: bool;
   t2_add_node_cmd: bool;
   t2_rem_node_cmd: bool;
   t2_alive_nodes: int;

   t3_running: bool;
   t3_start_cmd: bool;
   t3_stop_cmd: bool;
   t3_repair_node_cmd: bool;
   t3_forget_node_cmd: bool;
   t3_repair_task_cmd: bool;
   t3_forget_task_cmd: bool;
   t3_add_node_cmd: bool;

   t4_running: bool;
   t4_start_cmd: bool;
   t4_stop_cmd: bool;
   t4_repair_node_cmd: bool;
   t4_forget_node_cmd: bool;
   t4_repair_task_cmd: bool;
   t4_forget_task_cmd: bool;
   t4_add_node_cmd: bool;
   t4_rem_node_cmd: bool;
   t4_alive_nodes: int;

   t1_repair_inhibited: bool;

   t2_repair_inhibited: bool;
   t2_sizing_inhibited: bool;

   t3_repair_inhibited: bool;

   t4_repair_inhibited: bool;
   t4_sizing_inhibited: bool;

   ok: bool;
   )
  contract
var
  ak: bool;
let
  (* Environment assumptions encode some (not necessarily all) mutual exclusion
   properties between input signals. *)
  ak = (
        not (whole_start_req  &  whole_stop_req) &

        not (t1_repair_node_error & t1_repair_node_ack) &
        not (t1_add_node_error    & t1_add_node_ack) &

        not (t2_underload         & t2_overload) &
        not (t2_repair_node_error & t2_repair_node_ack) &
        not (t2_add_node_error    & t2_add_node_ack) &
        inlined implies (t2_node_multiple_failures, t2_node_failure) &

        not (t3_repair_node_error & t3_repair_node_ack) &
        not (t3_add_node_error    & t3_add_node_ack) &

        not (t4_underload         & t4_overload) &
        not (t4_repair_node_error & t4_repair_node_ack) &
        not (t4_add_node_error    & t4_add_node_ack) &
        inlined implies (t4_node_multiple_failures, t4_node_failure) &

        true
        );
tel
  assume ak
  enforce ok
  with
  (

   (* Controlables are approval signals of the managers. Note that their order
    matters for determinization purpose; the controller implicitly tries to
    assign `true' to each of these signals in turn, until an assignment causes
    an invariant violation. Here the resulting behavior is such that repair
    managers have priority over the sizing ones, and the repairing
    (resp. sizing) manager of any tier i takes precedence over any repairing
    (resp. sizing) manager of tiers i+1+j. *)

   t1_repair_ok: bool;
   t2_repair_ok: bool;
   t3_repair_ok: bool;
   t4_repair_ok: bool;

   t2_sizing_ok: bool;
   t4_sizing_ok: bool;
   )
var

  t1_was_repairing: bool;
  t1_repairing: bool;
  t1_start_req: bool;
  t1_stop_req: bool;
  t1_repair_node_req: bool;
  t1_forget_node_req: bool;
  t1_repair_task_req: bool;
  t1_forget_task_req: bool;

  t2_was_adding: bool;
  t2_adding: bool;
  t2_was_repairing: bool;
  t2_repairing: bool;
  t2_start_req: bool;
  t2_stop_req: bool;
  t2_repair_node_req: bool;
  t2_forget_node_req: bool;
  t2_repair_task_req: bool;
  t2_forget_task_req: bool;
  t2_add_node_req: bool;
  t2_rem_node_req: bool;
  t2_min_nodes: bool;
  t2_max_nodes: bool;

  t3_was_repairing: bool;
  t3_repairing: bool;
  t3_start_req: bool;
  t3_stop_req: bool;
  t3_repair_node_req: bool;
  t3_forget_node_req: bool;
  t3_repair_task_req: bool;
  t3_forget_task_req: bool;

  t4_was_adding: bool;
  t4_adding: bool;
  t4_was_repairing: bool;
  t4_repairing: bool;
  t4_start_req: bool;
  t4_stop_req: bool;
  t4_repair_node_req: bool;
  t4_forget_node_req: bool;
  t4_repair_task_req: bool;
  t4_forget_task_req: bool;
  t4_add_node_req: bool;
  t4_rem_node_req: bool;
  t4_min_nodes: bool;
  t4_max_nodes: bool;

  t2_expects_temporary_underload: bool;
  t3_expects_temporary_underload: bool;
  t4_expects_temporary_underload: bool;
  t5_expects_temporary_underload: bool;

  t1_itp_ok: bool;
  t2_itp_ok: bool;
  t3_itp_ok: bool;
  t4_itp_ok: bool;
  t1_props: bool;
  t2_props: bool;
  t3_props: bool;
  t4_props: bool;
  running: bool;
let

  (* + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + *)
  (* Invariant specification *)

  ok = (

        (* When the application is running, then enforce tier-level and
         inter-tier properties. *)
        inlined implies (running,
                         t1_props & t2_props & t3_props & t4_props &
                         t1_itp_ok & t2_itp_ok & t3_itp_ok & t4_itp_ok &
                         true) &

        (* Enable sizing when the application is running only. *)
        inlined implies (t2_sizing_ok, running) &
        inlined implies (t4_sizing_ok, running) &
        true);

  (* + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + *)
  (* Startup sequence specification *)

  (* 6-states automaton describing the various phases of the application
   startup: see `../argos/startup4.argos'. This automaton is able to handle the
   reception of whole stop requests at any time. *)

  (
   t1_start_req,
   t1_stop_req,
   t2_start_req,
   t2_stop_req,
   t3_start_req,
   t3_stop_req,
   t4_start_req,
   t4_stop_req,
   started,
   running,
   (*) = Startup4.startup4*)
   ) = inlined startup4
  (
   whole_start_req,
   whole_stop_req,
   (* hack to avoid using a dedicated `started' input signal for each tier. *)
   t1_add_node_ack,
   t2_add_node_ack, (* ibid. *)
   t3_add_node_ack, (* ibid. *)
   t4_add_node_ack, (* ibid. *)
   );

  (* + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + *)
  (* Inter-tier properties specification. *)

  (* These nodes basically build conjunction of invariants from various input
   signals. See `tier_properties.ept.pp'. *)

  (
   t1_itp_ok,
   t2_expects_temporary_underload,
   ) = inlined inter_tier_properties
  (
   false,
   t1_node_failure or t1_node_suspected_failures or t1_task_failure,
   t1_repairing or t1_task_actually_repairing,
   false,
   );

  (
   t2_itp_ok,
   t3_expects_temporary_underload,
   ) = inlined inter_tier_properties
  (
   t2_expects_temporary_underload,
   t2_node_failure or t2_node_suspected_failures or t2_task_failure,
   t2_repairing or t2_task_actually_repairing,
   t2_rem_node_cmd,
   );

  (
   t3_itp_ok,
   t4_expects_temporary_underload,
   ) = inlined inter_tier_properties
  (
   t3_expects_temporary_underload,
   t3_node_failure or t3_node_suspected_failures or t3_task_failure,
   t3_repairing or t3_task_actually_repairing,
   false,
   );

  (
   t4_itp_ok,
   t5_expects_temporary_underload,
   ) = inlined inter_tier_properties
  (
   t4_expects_temporary_underload,
   t4_node_failure or t4_node_suspected_failures or t4_task_failure,
   t4_repairing or t4_task_actually_repairing,
   t4_rem_node_cmd,
   );

  (* + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + *)
  (* Tier 1 properties. See `tier_properties.ept.pp'. *)

  t1_props = inlined tier_properties
  (
   false,
   false,
   t1_node_suspected_failures,
   t1_node_failure or t1_task_failure or t1_task_actually_repairing,
   false,
   false,
   false,
   false,
   t1_repairing,
   t1_repair_node_cmd,
   );

  (* + + *)

  t1_repair_inhibited = not t1_repair_ok;
  (
   t1_repair_node_req,
   t1_forget_node_req,
   t1_repair_task_req,
   t1_forget_task_req,
   ) = inlined repair
  (
   t1_repair_ok,
   t1_node_failure,
   t1_task_failure,
   t1_was_repairing,
   t1_task_actually_repairing,
   );

  t1_was_repairing = false -> pre t1_repairing;
  (
   t1_running,
   t1_repairing,
   t1_start_cmd,
   t1_stop_cmd,
   t1_repair_node_cmd,
   t1_forget_node_cmd,
   t1_repair_task_cmd,
   t1_forget_task_cmd,
   t1_add_node_cmd,
   ) = inlined tier_model
  (
   t1_start_req,
   t1_stop_req,
   t1_repair_node_req,
   t1_forget_node_req,
   t1_repair_task_req,
   t1_forget_task_req,
   t1_repair_node_ack,
   t1_repair_node_error,
   t1_add_node_ack,
   t1_add_node_error,
   t1_node_failure,
   );

  (* + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + *)
  (* Tier 2 properties. See `tier_properties.ept.pp'. *)

  t2_props = inlined tier_properties
  (
   t2_underload,
   t2_overload,
   t2_node_suspected_failures,
   t2_node_failure or t2_task_failure or t2_task_actually_repairing,
   t2_node_multiple_failures,
   t2_adding,
   t2_add_node_cmd,
   t2_rem_node_cmd,
   t2_repairing,
   t2_repair_node_cmd,
   );

  (* + + *)

  t2_repair_inhibited = not t2_repair_ok;
  (
   t2_repair_node_req,
   t2_forget_node_req,
   t2_repair_task_req,
   t2_forget_task_req,
   ) = inlined repair
  (
   t2_repair_ok,
   t2_node_failure,
   t2_task_failure,
   t2_was_repairing,
   t2_task_actually_repairing,
   );

  t2_sizing_inhibited = not t2_sizing_ok;
  (
   t2_add_node_req,
   t2_rem_node_req,
   ) = inlined sizing
  (
   t2_sizing_ok,
   t2_underload,
   t2_overload,
   t2_was_adding,
   ( true -> pre t2_min_nodes) & not t2_add_node_ack,
   (false -> pre t2_max_nodes) & not t2_node_failure,
   );

  t2_was_adding    =  true -> pre t2_adding;
  t2_was_repairing = false -> pre t2_repairing;
  (
   t2_running,
   t2_adding,
   t2_repairing,
   t2_start_cmd,
   t2_stop_cmd,
   t2_repair_node_cmd,
   t2_forget_node_cmd,
   t2_repair_task_cmd,
   t2_forget_task_cmd,
   t2_add_node_cmd,
   t2_rem_node_cmd,
   t2_min_nodes,
   t2_max_nodes,
   t2_alive_nodes,
   ) = inlined tier_model_repl
  (
   t2_start_req,
   t2_stop_req,
   t2_repair_node_req,
   t2_forget_node_req,
   t2_repair_task_req,
   t2_forget_task_req,
   t2_add_node_req,
   t2_rem_node_req,
   t2_repair_node_ack,
   t2_repair_node_error,
   t2_add_node_ack,
   t2_add_node_error,
   t2_node_failures,
   t2_config_min_nodes,
   t2_config_max_nodes,
   t2_config_init_nodes,
   );

  (* + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + *)
  (* Tier 3 properties. See `tier_properties.ept.pp'. *)

  t3_props = inlined tier_properties
  (
   false,
   false,
   t3_node_suspected_failures,
   t3_node_failure or t3_task_failure or t3_task_actually_repairing,
   false,
   false,
   false,
   false,
   t3_repairing,
   t3_repair_node_cmd,
   );

  (* + + *)

  t3_repair_inhibited = not t3_repair_ok;
  (
   t3_repair_node_req,
   t3_forget_node_req,
   t3_repair_task_req,
   t3_forget_task_req,
   ) = inlined repair
  (
   t3_repair_ok,
   t3_node_failure,
   t3_task_failure,
   t3_was_repairing,
   t3_task_actually_repairing,
   );

  t3_was_repairing = false -> pre t3_repairing;
  (
   t3_running,
   t3_repairing,
   t3_start_cmd,
   t3_stop_cmd,
   t3_repair_node_cmd,
   t3_forget_node_cmd,
   t3_repair_task_cmd,
   t3_forget_task_cmd,
   t3_add_node_cmd,
   ) = inlined tier_model
  (
   t3_start_req,
   t3_stop_req,
   t3_repair_node_req,
   t3_forget_node_req,
   t3_repair_task_req,
   t3_forget_task_req,
   t3_repair_node_ack,
   t3_repair_node_error,
   t3_add_node_ack,
   t3_add_node_error,
   t3_node_failure,
   );

  (* + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + *)
  (* Tier 4 properties. See `tier_properties.ept.pp'. *)

  t4_props = inlined tier_properties
  (
   t4_underload,
   t4_overload,
   t4_node_suspected_failures,
   t4_node_failure or t4_task_failure or t4_task_actually_repairing,
   t4_node_multiple_failures,
   t4_adding,
   t4_add_node_cmd,
   t4_rem_node_cmd,
   t4_repairing,
   t4_repair_node_cmd,
   );

  (* + + *)

  t4_repair_inhibited = not t4_repair_ok;
  (
   t4_repair_node_req,
   t4_forget_node_req,
   t4_repair_task_req,
   t4_forget_task_req,
   ) = inlined repair
  (
   t4_repair_ok,
   t4_node_failure,
   t4_task_failure,
   t4_was_repairing,
   t4_task_actually_repairing,
   );

  t4_sizing_inhibited = not t4_sizing_ok;
  (
   t4_add_node_req,
   t4_rem_node_req,
   ) = inlined sizing
  (
   t4_sizing_ok,
   t4_underload,
   t4_overload,
   t4_was_adding, (* & not t4_add_node_ack *)
   ( true -> pre t4_min_nodes) & not t4_add_node_ack,
   (false -> pre t4_max_nodes) & not t4_node_failure,
   );

  t4_was_adding    =  true -> pre t4_adding;
  t4_was_repairing = false -> pre t4_repairing;
  (
   t4_running,
   t4_adding,
   t4_repairing,
   t4_start_cmd,
   t4_stop_cmd,
   t4_repair_node_cmd,
   t4_forget_node_cmd,
   t4_repair_task_cmd,
   t4_forget_task_cmd,
   t4_add_node_cmd,
   t4_rem_node_cmd,
   t4_min_nodes,
   t4_max_nodes,
   t4_alive_nodes,
   ) = inlined tier_model_repl
  (
   t4_start_req,
   t4_stop_req,
   t4_repair_node_req,
   t4_forget_node_req,
   t4_repair_task_req,
   t4_forget_task_req,
   t4_add_node_req,
   t4_rem_node_req,
   t4_repair_node_ack,
   t4_repair_node_error,
   t4_add_node_ack,
   t4_add_node_error,
   t4_node_failures,
   t4_config_min_nodes,
   t4_config_max_nodes,
   t4_config_init_nodes,
   );

  (* + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + *)

tel
