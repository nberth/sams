(* 
 * This file is part of sams multi-tier application management examples.
 * 
 * Copyright Grenoble University, (2013)
 * 
 * Author: Nicolas Berthier <nicolas.berthier@inria.fr>
 * 
 * This software is a computer program whose purpose is to autonomously manage
 * computing systems using reactive control techniques.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software.  You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *)

(* Note include/utils.ept.pp needs to be included! *)

node tier_properties
  (
   underload: bool;
   overload: bool;
   suspected_failure: bool;
   failure: bool;
   multiple_failures: bool;
   adding: bool;
   add_cmd: bool;
   rem_cmd: bool;
   repairing: bool;
   rep_cmd: bool;
   )
returns
  (
   ok: bool
   )
let
  ok =
    (

     (* Useful? *)
     (*inlined implies (adding & not failure, not add_req) &*)

     (* No remove command while adding (useless in principle). *)
     (*inlined never ((adding or add_cmd) & rem_cmd) &*)

     (* The only situation where adding while repairing is allowed is when there
     are multiple failures. *)
     inlined implies (add_cmd & (repairing or rep_cmd), multiple_failures) &

     (* The only situation where repairing while adding is allowed is when there
      are multiple failures. *)
     inlined implies ((adding or add_cmd) & rep_cmd, multiple_failures) &
     (* inlined implies ((adding or add_cmd) & (repairing or rep_cmd), multiple_failures) & *)

     (* Do not change machine pool when a failure and an underload occur
      "simultaneously" (?). *)
     inlined implies (failure & underload, not (rem_cmd or rep_cmd)) &

     (* These two lead to a failure of synthesis; why? *)
     (*inlined implies (failure & underload, not (add_cmd or rem_cmd or rep_cmd)) &*)
     (*inlined implies (failure & not multiple_failures & underload,
                      not (add_cmd or rep_cmd or rem_cmd)) &*)

     (* Do not add a machine when failures do not lead to immediate
      * overload (Hum...) *)
     (* inlined implies (failure & not overload, not add) & *)

     true
     );
tel

node inter_tier_properties
  (
   temporary_underload: bool;
   failure: bool;
   repairing: bool;
   rem_cmd: bool;
   )
returns
  (
   ok: bool;
   tail_expects_temporary_underload: bool;
   )
let
  tail_expects_temporary_underload = temporary_underload or
    failure or repairing;

  ok =
    (

     (* Forbid removing a machine if the underload is notified as temporary. *)
     inlined implies (temporary_underload, not rem_cmd) &
     (* inlined implies (rem_cmd, not temporary_underload) & *)

     true
     );
tel
