;; -*- mode: emacs-lisp; coding: utf-8 -*-
((nil . ((eval . (set (make-local-variable 'my-project-path)
                      (file-name-directory
                       (let ((d (dir-locals-find-file "./")))
                         (if (stringp d) d (car d))))))
	 (eval . (message "Project directory set to `%s'." my-project-path))
	 (eval . (set 'compile-command (format "make -C \"%s\"" my-project-path)))))
 (org-mode
  .
  ((eval . (when (file-exists-p "etc/org2latex.el") (load-file "etc/org2latex.el")))
   (reftex-texpath-environment-variables . ("./tex"))
   (reftex-bibpath-environment-variables . ("./bib:./tex"))
   (reftex-cite-format . natbib)
   (ispell-parser . tex)
   (eval . (flyspell-mode 1))
   (eval . (auto-fill-mode -1))
   (eval . (reftex-mode))
   (eval . (reftex-parse-all)))))
