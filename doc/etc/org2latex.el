;; LaTeX export customization
(require 'org)
(require 'ox-latex)

(setq my-headline-filter-regexp
      (concat "^\\\\"
	      (regexp-opt '("section" "subsection" "subsubsection"
			    "paragraph" "subparagraph") 'words)
	      "\\*\?{\\(\\\\itshape \\)\?\\(strip.*\\|ignore\\|dummy\\)\.\?}"))

(add-to-list 'org-latex-classes
	     '("scrartcl"
	       "\\documentclass{scrartcl}
                [NO-DEFAULT-PACKAGES]
                [EXTRA]"
	       ("\\section{%s}" . "\\section*{%s}")
	       ("\\subsection{%s}" . "\\subsection*{%s}")
	       ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	       ("\\paragraph{ %s}" . "\\paragraph*{%s}")
	       ("\\subparagraph{%s}" . "\\subparagraph*{%s}")
	       ("\\subsubparagraph{%s}" . "\\subsubparagraph*{%s}")))
(add-to-list 'org-latex-classes
	     '("scrreprt"
	       "\\documentclass{scrreprt}
               [NO-DEFAULT-PACKAGES]
               [EXTRA]"
	       ("\\part{%s}" . "\\part*{%s}")
	       ("\\section{%s}" . "\\section*{%s}")
	       ("\\subsection{%s}" . "\\subsection*{%s}")
	       ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	       ("\\paragraph{%s}" . "\\paragraph*{%s}")
	       ("\\subparagraph{%s}" . "\\subparagraph*{%s}")
	       ("\\subsubparagraph{%s}" . "\\subsubparagraph*{%s}")))
(defun ox-mrkup-filter-bold (text backend info)
  (when (org-export-derived-backend-p backend 'latex)
    (when (string-match my-headline-filter-regexp text)
      (format "%%%s" text))))
(add-to-list
 'org-export-filter-headline-functions
 'ox-mrkup-filter-bold)

(setq org-export-headline-levels 6
      org-export-latex-hyperref-format "\\ref{%s}"
      org-export-latex-href-format "\\url{%s}"
      make-backup-files nil)

;; For tikz externalize:
(setq org-latex-pdf-process 
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
	"pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
	"pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
